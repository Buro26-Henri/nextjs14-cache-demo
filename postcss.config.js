const breakpoints = {
    sm: "576px",
    md: "768px",
    lg: "992px",
    xl: "1200px"
}

module.exports = {
    plugins: [
        [
            'postcss-mixins',
            {
                mixins: {
                    'media-breakpoint-down': (mixin, breakpoint) => {
                        const size = breakpoints[breakpoint] || breakpoint
                        const nodesAsString = mixin.nodes.map(node => node.toString()).join(';')
                        mixin.replaceWith(`@media (max-width: ${size}) { ${nodesAsString}; }`)
                    },
                    'media-breakpoint-up': (mixin, breakpoint) => {
                        const size = breakpoints[breakpoint] || breakpoint
                        const nodesAsString = mixin.nodes.map(node => node.toString()).join(';')
                        mixin.replaceWith(`@media (min-width: ${size}) { ${nodesAsString}; }`)
                    },
                    'media-query': (mixin, query) => {
                        const nodesAsString = mixin.nodes.map(node => node.toString()).join(';')
                        mixin.replaceWith(`@media ${query} { ${nodesAsString}; }`)
                    },
                },
            }
        ],
        "postcss-nested",
        "postcss-flexbugs-fixes",
        [
            "postcss-preset-env",
            {
                autoprefixer: {
                    flexbox: "no-2009"
                },
                stage: 3,
                features: {
                    "custom-properties": false
                }
            }
        ]
    ]
}