# Caching & Server Actions

## Why caching
.....

## Types of caching
- Memoization
- Persistent

## React cache (memoization)
- Request based memoization

## Nextjs cache (persistent)
- Persistent
- tags
- keyparts
- invalidation 

### Server actions
- revalidatePath