export enum CacheType {
    NONE = 'none',
    REACT = 'react',
    NEXT = 'next',
    NEXT_REACT = 'next-react',
}