export * from '@/lib/strapi/generated/types'

export interface StrapiImage extends StrapiEdge<StrapiImageAttributes> {
}

export interface StrapiImageAttributes {
    url: string
    alternativeText: string
}

export interface StrapiEdge<T> {
    id: string,
    attributes: T,
}


