import {DefaultSession} from "next-auth"
import {KlantEntity} from "@/lib/strapi";

declare module "next-auth" {
    /**
     * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
     */
    interface Session {
        user: {
            id: string
            accessToken: string
            klant: {
                data: KlantEntity
            }
            rol?: 'manager' | 'authenticated'
        } & DefaultSession["user"]
        accessToken?: string

        session?: {
            accessToken?: string
            rol?: 'manager' | 'authenticated'
        }
    }
}