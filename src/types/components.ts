import {CSSProperties, ReactNode} from "react";
import {Session} from "next-auth";

export type Children = ReactNode

export type ChildrenNullable = Children | null | undefined

export interface HasChildren {
    children: Children
}

export interface HasOptionalChildren {
    children?: ChildrenNullable
}

export interface HasClassName {
    className?: string
}

export interface HasStyles {
    style?: CSSProperties
}

export interface HasSession {
    session: Session | null | undefined
}

export enum ButtonVariants {
    DEFAULT = 'btn-default',
    LIGHT_BLUE = 'btn-light_blue',
    BLUE = 'btn-blue',
    GREEN = 'btn-green'
}
export enum IconButtonVariants {
    Account,
    AddToCart,
    Cart,
    Check,
    Close,
    HeartBlack,
    Heart,
    Location,
    Order,
    Warning
}
