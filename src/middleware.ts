import {NextRequest, NextResponse} from 'next/server'



export async function middleware(req: NextRequest): Promise<NextResponse | void> {
    req.headers.set('x-pathname', req.nextUrl.pathname)

    return NextResponse.next({
        headers: {
            'x-pathname': req.nextUrl.pathname,
        },
    })
}
