import React from 'react'
import { Container } from 'react-bootstrap'
import styles from './not-found.module.css'

export interface NotFoundProps {

}


const NotFoundPage: React.FC<NotFoundProps> = async ({}) => {

    return (
        <Container className={styles.notFound}>
            <h1>Not found</h1>
            <p>Could not found page</p>
        </Container>
    )
}

export default NotFoundPage
