import { appRouter } from "@/server";
import { fetchRequestHandler } from "@trpc/server/adapters/fetch";
import {createContext} from "@/server/context";

// @see https://trpc.io/docs/api-handler
const handler = (req: Request) =>
    fetchRequestHandler({
        router: appRouter,
        endpoint: '/api/trpc',
        req,

        /**
         * @link https://trpc.io/docs/context
         */
        createContext: opts => createContext(opts),

        /**
         * @link https://trpc.io/docs/error-handling
         */
        onError({ error }) {
            if (error.code === 'INTERNAL_SERVER_ERROR') {
                // send to bug reporting
                //console.error('Something went wrong', error);
            }
        },

        /**
         * Enable query batching
         */
        batching: {
            enabled: true,
        },

        /**
         * @link https://trpc.io/docs/caching#api-response-caching
         */
        // responseMeta() {
        //   // ...
        // },

    })

export { handler as GET, handler as POST }