import { NextResponse } from 'next/server'
import { revalidateTag } from 'next/cache'

const handler = async (req: Request) => {
    const data = await req.json()

    console.log({
        webhook: 'strapi',
        token: process.env.NEXT_API_TOKEN_STRAPI,
        data
    })

    // General revalidations
    if (data?.model) {
        revalidateTag(data.model)
        revalidateTag(`${data.model}_${data.entry.id}`)
    }

    return NextResponse.json({ ok: true })
}

export { handler as GET, handler as POST }