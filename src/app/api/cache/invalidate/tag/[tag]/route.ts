import { NextResponse } from 'next/server'
import { revalidateTag } from 'next/cache'

const handler = async (req: Request, { params }: { params: { tag: string } }) => {
    revalidateTag(params.tag)

    return NextResponse.json({ ok: true })
}

export { handler as GET }