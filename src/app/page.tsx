import type { FC } from 'react'


export interface PageProps {

}


const Home: FC<PageProps> = async () => {

    return (
        <section>
            <h1>React (nextjs) Demo</h1>
        </section>
    )
}

export default Home