import React from 'react'
import FontProvider from '@/providers/FontProvider'
import type { Metadata, Viewport } from 'next'
import TrpcProvider from '@/providers/TrpcProvider'
import NotificationProvider from '@/providers/NotificationProvider'
import SkeletonThemeProvider from '@/providers/SkeletonThemeProvider'
import Header from '@/components/layout/header/Header'
import { Container } from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css'
import '@/styles/global.css'


export interface RootLayoutProps {
    children: React.ReactNode;
}

export const viewport: Viewport = {
    width: 'device-width',
    initialScale: 1.0,
    maximumScale: 1.0,
    userScalable: false
}

export const generateMetadata = async (): Promise<Metadata> => {

    return {
        metadataBase: new URL(process.env.NEXT_PUBLIC_BASE_URL ?? ''),
        title: {
            default: 'Cache demo',
            template: `%s | Cache demo`
        },
        applicationName: 'Cache demo'
    }
}

const RootLayout: React.FC<RootLayoutProps> = async ({ children }) => {

    return (
        <html lang="nl" suppressHydrationWarning>
        <FontProvider>
            <SkeletonThemeProvider>
                <body>
                <NotificationProvider>
                    <TrpcProvider>
                        <Header />
                        <main>
                            <Container>
                                {children}
                            </Container>
                        </main>
                    </TrpcProvider>
                </NotificationProvider>
                </body>
            </SkeletonThemeProvider>
        </FontProvider>
        </html>
    )
}

export default RootLayout
