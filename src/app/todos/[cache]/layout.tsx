import React, { type FC, type ReactNode } from 'react'
import styles from './layout.module.css'
import Link from 'next/link'

export interface PageProps {
    children: ReactNode
}


const TodosListPage: FC<PageProps> = ({ children }) => {


    return (
        <>
            <nav className={styles.nav}>
                <Link href={'/todos/none'}>Todos (none)</Link>
                <Link href={'/todos/react'}>Todos (react)</Link>
                <Link href={'/todos/next'}>Todos (next)</Link>
                <Link href={'/todos/next-react'}>Todos (next + react)</Link>
            </nav>

            {children}
        </>
    )
}

export default TodosListPage