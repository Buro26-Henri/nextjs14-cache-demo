'use server'

import { revalidatePath } from 'next/cache'
import { action } from '@/lib/actions'
import { z } from 'zod'
import { zfd } from 'zod-form-data'

export const newList = action(
    zfd.formData({
        name: zfd.text(z.string())
    }),
    async (data, { caller }) => {
        await caller.list.create(data)

        revalidatePath('/todos')

        return {
            success: true
        }
    }, {}
)