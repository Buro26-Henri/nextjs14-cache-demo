'use client'

import { type FC, useRef } from 'react'
import { newList } from '@/app/todos/[cache]/(list)/@form/action.ts'
import styles from './page.module.css'
import buttonStyles from '@/styles/button.module.css'
import formStyles from '@/styles/form.module.css'
import { useAction } from 'next-safe-action/hooks'
import { isExecuting } from 'next-safe-action/status'

export interface PageProps {

}


const TodosListPage: FC<PageProps> = () => {
    const formRef = useRef<HTMLFormElement>(null)
    const { status, execute, result } = useAction(newList, {
        onSettled: res => {
            formRef.current?.reset()
        }
    })

    return (
        <form action={execute} ref={formRef} className={styles.form}>
            <input type={'text'} name={'name'} className={formStyles.input} />
            <button
                type={'submit'}
                disabled={isExecuting(status)}
                className={buttonStyles.button}
            >
                {status === 'executing' ? 'Loading...' : 'Create'}
            </button>
        </form>
    )
}

export default TodosListPage