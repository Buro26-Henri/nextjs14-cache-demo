import type { FC, ReactNode } from 'react'
import styles from './layout.module.css'

export interface PageProps {
    title: ReactNode
    list: ReactNode
    children: ReactNode
    form: ReactNode
}


const TodosListPage: FC<PageProps> = async ({ list, title, children, form }) => {


    return (
        <section className={styles.container}>
            {children}
            {title}
            {form}
            {list}
        </section>
    )
}

export default TodosListPage