import type { FC } from 'react'
import styles from './page.module.css'
import TodoCard from '@/app/todos/[cache]/(list)/@list/_components/TodoCard.tsx'
import { createCaller } from '@/lib/trpc/trpc-caller.ts'
import type { CacheType } from '@/types/cache.ts'


export interface PageProps {
    params: {
        cache: CacheType
    }
}


const TodosListPage: FC<PageProps> = async ({ params }) => {
    const caller = await createCaller()
    const lists = await caller.list.findMany({
        cache: params.cache,
    })

    return (
        <div className={styles.list}>
            {lists.map(list => (
                <TodoCard list={list} key={list.id} cache={params.cache} />
            ))}
        </div>
    )
}

export default TodosListPage