'use server'

import { action } from '@/lib/actions'
import { z } from 'zod'
import { revalidatePath } from 'next/cache'

export const deleteList = action(
    z.object({
        id: z.string()
    }),
    async ({ id }, { caller }) => {
        await caller.list.delete({ id })

        revalidatePath('/todos')

        return {
            success: true
        }
    }
)