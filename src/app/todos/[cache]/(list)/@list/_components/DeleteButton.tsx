'use client'

import React from 'react'
import { useAction } from 'next-safe-action/hooks'
import { deleteList } from '@/app/todos/[cache]/(list)/@list/_components/actions.ts'
import styles from '@/styles/button.module.css'
import { isExecuting } from 'next-safe-action/status'

export interface DeleteButtonProps {
    listId: string | number
}

const DeleteButton: React.FC<DeleteButtonProps> = ({ listId }) => {
    const { status, execute } = useAction(deleteList)

    return (
        <button
            className={styles.button}
            onClick={() => execute({
                id: String(listId)
            })}
        >
            {isExecuting(status) ? 'Loading...' : 'Delete'}
        </button>
    )
}

export default DeleteButton
