import React from 'react'
import type { List } from '@/database/schema/todos.ts'
import styles from './TodoCard.module.css'
import { format } from 'date-fns'
import DeleteButton from '@/app/todos/[cache]/(list)/@list/_components/DeleteButton.tsx'
import Link from 'next/link'
import type { CacheType } from '@/types/cache.ts'

export interface TodoCardProps {
    list: List
    cache: CacheType
}

const TodoCard: React.FC<TodoCardProps> = ({ list, cache }) => {

    return (
        <article className={styles.card}>
            <Link href={`/todos/${cache}/${list.id}`} className={styles.link}>
                <h2>{list.name}</h2>
            </Link>
            <p>{format(Number(list.createdAt), 'd-MM-y')}</p>

            <div className={styles.actions}>
                <DeleteButton listId={list.id} />
            </div>
        </article>
    )
}

export default TodoCard
