import type { FC } from 'react'
import { createCaller } from '@/lib/trpc/trpc-caller.ts'
import type { CacheType } from '@/types/cache.ts'


export interface PageProps {
    params: {
        cache: CacheType
    }
}


const TodosListPage: FC<PageProps> = async ({ params }) => {
    const caller = await createCaller()
    const [lists] = await Promise.all([
        caller.list.findMany({
            cache: params.cache,
        }),
        caller.list.findMany({
            cache: params.cache,
        }),
        caller.list.findMany({
            cache: params.cache,
        }),
        caller.list.findMany({
            cache: params.cache,
        }),
        caller.list.findMany({
            cache: params.cache,
        }),
        caller.list.findMany({
            cache: params.cache,
        }),
        caller.list.findMany({
            cache: params.cache,
        }),
    ])

    return (
        <h1>My Todo lists ({lists.length})</h1>
    )
}

export default TodosListPage