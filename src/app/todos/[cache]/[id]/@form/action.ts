'use server'

import { revalidatePath } from 'next/cache'
import { z } from 'zod'
import { zfd } from 'zod-form-data'
import { action } from '@/lib/actions'

export const addTodo = action(
    zfd.formData({
        listId: zfd.text(z.string()),
        title: zfd.text(z.string()),
        description: zfd.text(z.string().optional()).optional()
    }),
    async (data, { caller }) => {
        await caller.list.todos.add({
            ...data,
            listId: Number(data.listId)
        })

        revalidatePath(`/todos/${data.listId}`)

        return {
            success: true
        }
    }, {}
)