'use client'

import { type FC, useRef, useState } from 'react'
import { addTodo } from '@/app/todos/[cache]/[id]/@form/action.ts'
import styles from './page.module.css'
import buttonStyles from '@/styles/button.module.css'
import formStyles from '@/styles/form.module.css'
import { useAction } from 'next-safe-action/hooks'
import Modal from '@/components/data-display/Modal/Modal.tsx'
import { isExecuting } from 'next-safe-action/status'

export interface PageProps {
    params: {
        id: string
    }
}


const TodosListPage: FC<PageProps> = ({ params }) => {
    const [showModal, setShowModal] = useState<boolean>(false)
    const formRef = useRef<HTMLFormElement>(null)
    const { status, execute, result } = useAction(addTodo, {
        onSettled: res => {
            formRef.current?.reset()
            setShowModal(false)
        }
    })

    return (
        <>
            <Modal open={showModal} onClose={() => setShowModal(false)}>
                <form
                    ref={formRef}
                    className={styles.form}
                    action={(data: FormData) => {
                        data.set('listId', params.id)

                        execute(data)
                    }}
                >
                    <div className={formStyles.formGroup}>
                        <label className={formStyles.label}>Title</label>
                        <input type={'text'} name={'title'} className={formStyles.input} />
                    </div>

                    <div className={formStyles.formGroup}>
                        <label className={formStyles.label}>Description</label>
                        <textarea name={'description'} className={formStyles.textarea} />
                    </div>

                    <button
                        type={'submit'}
                        disabled={status === 'executing'}
                        className={buttonStyles.button}
                    >
                        {isExecuting(status) ? 'Adding...' : 'Add'}
                    </button>
                </form>
            </Modal>

            <div>
                <button
                    className={buttonStyles.button}
                    onClick={() => setShowModal(true)}
                >
                    Add todo
                </button>
            </div>
        </>
    )
}

export default TodosListPage