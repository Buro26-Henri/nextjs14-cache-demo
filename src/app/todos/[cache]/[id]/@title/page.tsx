import type { FC } from 'react'
import { createCaller } from '@/lib/trpc/trpc-caller.ts'
import type { CacheType } from '@/types/cache.ts'


export interface PageProps {
    params: {
        id: string
        cache: CacheType
    }
}


const TodosListPage: FC<PageProps> = async ({ params }) => {
    const caller = await createCaller()
    const [list] = await Promise.all([
        caller.list.findOne({
            id: params.id,
            cache: params.cache,
        }),
        caller.list.findOne({
            id: params.id,
            cache: params.cache,
        }),
        caller.list.findOne({
            id: params.id,
            cache: params.cache,
        }),
        caller.list.findOne({
            id: params.id,
            cache: params.cache,
        }),
        caller.list.findOne({
            id: params.id,
            cache: params.cache,
        }),
        caller.list.findOne({
            id: params.id,
            cache: params.cache,
        }),
    ])
    let completed = 0
    list?.todos?.map((val, cur) => {
        if (!val.completed) {
            return
        }

        return completed++
    })

    return (
        <h1>{list?.name} ({completed}/{list?.todos?.length})</h1>
    )
}

export default TodosListPage