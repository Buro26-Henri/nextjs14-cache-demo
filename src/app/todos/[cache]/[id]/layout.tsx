import type { FC, ReactNode } from 'react'
import styles from './layout.module.css'

export interface PageProps {
    title: ReactNode
    children: ReactNode
    form: ReactNode
    todos: ReactNode
}


const TodoPage: FC<PageProps> = async ({ title, children, form, todos }) => {


    return (
        <section className={styles.container}>
            {children}
            {title}
            {form}
            {todos}
        </section>
    )
}

export default TodoPage