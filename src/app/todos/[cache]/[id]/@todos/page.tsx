import type { FC } from 'react'
import styles from './page.module.css'
import { createCaller } from '@/lib/trpc/trpc-caller.ts'
import TodoCard from '@/app/todos/[cache]/[id]/@todos/_components/TodoCard.tsx'
import type { CacheType } from '@/types/cache.ts'

export interface TodosListPageProps {
    params: {
        id: string
        cache: CacheType
    }
}

const TodosListPage: FC<TodosListPageProps> = async ({ params }) => {
    const caller = await createCaller()
    const list = await caller.list.findOne({
        id: params.id,
        cache: params.cache,
    })
    const openTodos = list?.todos?.filter(todo => !todo.completed)
    const completedTodos = list?.todos?.filter(todo => todo.completed)

    return (
        <section className={styles.list}>
            {!!openTodos?.length && (
                <>
                    <h3>Open</h3>
                    {openTodos?.map(todo => (
                        <TodoCard todo={todo} key={todo.id} />
                    ))}
                </>
            )}

            {!!completedTodos?.length && (
                <>
                    <h4>Completed</h4>
                    {completedTodos?.map(todo => (
                        <TodoCard todo={todo} key={todo.id} />
                    ))}
                </>
            )}
        </section>
    )
}

export default TodosListPage
