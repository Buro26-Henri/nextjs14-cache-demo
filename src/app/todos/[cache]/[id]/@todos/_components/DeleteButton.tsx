'use client'

import React from 'react'
import { useAction } from 'next-safe-action/hooks'
import { deleteTodo } from '@/app/todos/[cache]/[id]/@todos/_components/actions.ts'
import styles from '@/styles/button.module.css'
import { isExecuting } from 'next-safe-action/status'

export interface DeleteButtonProps {
    id: string
}

const DeleteButton: React.FC<DeleteButtonProps> = ({ id }) => {
    const { execute, status } = useAction(deleteTodo)

    return (
        <button
            className={styles.button}
            onClick={() => execute({ id })}
        >
            {isExecuting(status) ? 'Loading...' : 'Delete'}
        </button>
    )
}

export default DeleteButton
