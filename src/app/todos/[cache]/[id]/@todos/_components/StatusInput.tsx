'use client'

import React from 'react'
import { useAction } from 'next-safe-action/hooks'
import { changeTodoStatus } from '@/app/todos/[cache]/[id]/@todos/_components/actions.ts'
import styles from './StatusInput.module.css'
import { isExecuting } from 'next-safe-action/status'

export interface StatusInputProps {
    id: string
    completed: boolean
}

const StatusInput: React.FC<StatusInputProps> = ({ id, completed }) => {
    const { status, execute } = useAction(changeTodoStatus, {})

    return (
        <div>
            <input
                type={'checkbox'}
                checked={completed}
                disabled={isExecuting(status)}
                className={styles.checkbox}
                onChange={e => {
                    execute({
                        id,
                        completed: e.currentTarget.checked
                    })
                }}
            />
        </div>
    )
}

export default StatusInput
