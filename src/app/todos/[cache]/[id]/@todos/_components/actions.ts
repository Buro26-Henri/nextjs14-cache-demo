'use server'

import { action } from '@/lib/actions'
import { z } from 'zod'
import { revalidatePath } from 'next/cache'

export const changeTodoStatus = action(
    z.object({
        id: z.string(),
        completed: z.boolean()
    }),
    async ({ id, completed }, { caller }) => {
        const response = await caller.list.todos.update({
            id: Number(id),
            completed
        }).then(res => res[0])

        revalidatePath(`/todos/${response.listId}`)
    }
)

export const deleteTodo = action(
    z.object({
        id: z.string()
    }),
    async ({ id }, { caller }) => {
        const response = await caller.list.todos.delete({
            id
        }).then(res => res[0])

        revalidatePath(`/todos/${response.listId}`)
    }
)