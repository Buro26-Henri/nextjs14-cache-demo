import React from 'react'
import type { Todo } from '@/database/schema/todos.ts'
import styles from './TodoCard.module.css'
import StatusInput from '@/app/todos/[cache]/[id]/@todos/_components/StatusInput.tsx'
import DeleteButton from '@/app/todos/[cache]/[id]/@todos/_components/DeleteButton.tsx'

export interface TodoCardProps {
    todo: Todo
}

const TodoCard: React.FC<TodoCardProps> = ({ todo }) => {

    return (
        <article className={styles.card}>
            <div className={styles.statusContainer}>
                <StatusInput id={String(todo.id)} completed={!!todo.completed} />
            </div>

            <div className={styles.content}>
                <h2>{todo.title}</h2>
                <p>{todo.description}</p>
                <div>
                    <DeleteButton id={String(todo.id)} />
                </div>
            </div>
        </article>
    )
}

export default TodoCard
