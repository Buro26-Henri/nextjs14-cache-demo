import React, { type FC, type ReactNode } from 'react'
import styles from './layout.module.css'
import Link from 'next/link'

export interface PageProps {
    children: ReactNode
}


const TodosListPage: FC<PageProps> = ({ children }) => {


    return (
        <>
            <nav className={styles.nav}>
                <Link href={'/fundamentals/vanilla-vs-react'}>Vanilla vs React</Link>
                <Link href={'/fundamentals/tree'}>Tree</Link>
            </nav>

            {children}
        </>
    )
}

export default TodosListPage