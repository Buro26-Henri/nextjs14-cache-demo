
let count = 0

export default function Sidebar() {
    count++

    return (
        <nav>
            <h2>Sidebar</h2>
            <p>Render count: {count}</p>
        </nav>
    )
}