let count = 0

export default function Header() {
    count++

    return (
        <header>
            <h1>React Fundamentals</h1>
            <p>{`This header has been rendered ${count} times`}</p>
        </header>
    )
}