

let count = 0

export default function InteractiveChild() {
    count++

    return (
        <p>Interactive child {count} renders</p>
    )
}