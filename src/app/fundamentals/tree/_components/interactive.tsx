import { type Dispatch, type SetStateAction, useState } from 'react'
import InteractiveChild from '@/app/fundamentals/tree/_components/interactive-child.tsx'

type InteractiveProps = {
    count: number
    setCount: Dispatch<SetStateAction<number>>
}

let renderCount = 0

export default function Interactive({ count, setCount }: InteractiveProps) {
    //const [count, setCount] = useState(0)
    renderCount++

    return (
        <>
            <button onClick={() => setCount(count + 1)}>Click me</button>
            <p>You clicked {count} times</p>
            <p>Interactive rendered {renderCount} times</p>

            <InteractiveChild/>
        </>
    )
}