'use client'

import { Col, Row } from 'react-bootstrap'
import { useState } from 'react'
import Sidebar from '@/app/fundamentals/tree/_components/sidebar.tsx'
import Header from '@/app/fundamentals/tree/_components/header.tsx'
import Interactive from '@/app/fundamentals/tree/_components/interactive.tsx'
import Image from 'next/image'
import renderTreeImage from '@public/render_tree.webp'


export default function Page() {
    const [count, setCount] = useState(0)

    return (
        <>
            <aside>
                <p>I am above all</p>
            </aside>

            <Row>
                <Col md={3}>
                    <Sidebar />
                </Col>
                <Col md={9}>
                    <Header />
                    <Interactive
                        count={count}
                        setCount={setCount}
                    />

                    <Image src={renderTreeImage} alt={'Render Tree'} />
                </Col>
            </Row>
        </>
    )
}

