'use client'

import Script from 'next/script'
import { Col, Row } from 'react-bootstrap'
import { useState } from 'react'

function Vanilla() {

    return (
        <>
            <h1>Vanilla JS</h1>

            <div id={'app'} />

            <Script id={'demo'}>{`
              const app = document.getElementById('app');
              
              const button = document.createElement('button');
              button.textContent = 'Click me';
             
              let count = 0;
                       
              const message = document.createElement('p');
              message.textContent = \`You clicked \${count} times\`;
            
              button.addEventListener('click', () => {
                count++;
                message.textContent = \`You clicked \${count} times\`;
              });
            
              app.appendChild(button);
              app.appendChild(message);
            `}</Script>
        </>
    )
}

function React() {
    const [count, setCount] = useState(0)

    return (
        <>
            <h1>React</h1>

            <button onClick={() => setCount(count + 1)}>
                Click me
            </button>
            <p>You clicked {count} times</p>
        </>
    )
}


export default function Page() {

    return (
        <Row>
            <Col>
                <Vanilla />
            </Col>

            <Col>
                <React />
            </Col>
        </Row>
    )
}

