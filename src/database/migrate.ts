import { migrate } from 'drizzle-orm/better-sqlite3/migrator'
import { db } from '@/database/index.ts'

await migrate(db, { migrationsFolder: './src/database/migrations' })