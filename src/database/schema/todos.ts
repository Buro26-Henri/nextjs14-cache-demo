import { integer, sqliteTable, text } from 'drizzle-orm/sqlite-core'
import { sql, relations } from 'drizzle-orm'

export const list = sqliteTable('lists', {
    id: integer('id', { mode: 'number' }).primaryKey({ autoIncrement: true }),
    name: text('name').notNull(),
    createdAt: integer('created_at').default(sql`(CURRENT_TIMESTAMP)`)
})


export const todo = sqliteTable('todos', {
    id: integer('id', { mode: 'number' }).primaryKey({ autoIncrement: true }),
    listId: integer('list_id')
        .references(() => list.id, { onDelete: 'cascade' })
        .notNull(),
    title: text('title').notNull(),
    description: text('description'),
    completed: integer('completed', { mode: 'boolean' }).default(false),
    createdAt: integer('created_at').default(sql`(CURRENT_TIMESTAMP)`)
})


export const listRelations = relations(list, ({ many }) => ({
    todos: many(todo)
}))

export const postsRelations = relations(todo, ({ one }) => ({
    author: one(list, {
        fields: [todo.listId],
        references: [list.id]
    })
}))


export type List = typeof list.$inferSelect
export type InsertList = typeof list.$inferInsert

export type Todo = typeof todo.$inferSelect
export type InsertTodo = typeof todo.$inferInsert



