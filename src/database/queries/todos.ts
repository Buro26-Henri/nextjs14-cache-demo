import { utils } from '@/lib'
import { db } from '@/database'
import { eq } from 'drizzle-orm'
import { list } from '@/database/schema/todos.ts'
import { cache } from 'react'

export const findManyLists = async () => {
    await utils.misc.delay(3000)
    console.log(`list:findMany ${(new Date()).getTime()}`)

    return db.query.list.findMany()
}

export const findOneList = async (id: number) => {
    await utils.misc.delay(3000)
    console.log(`list:findOne ${(new Date()).getTime()}`)

    return db.query.list.findFirst({
        where: eq(list.id, id),
        with: {
            todos: true
        }
    })
}

export const findOneListCached = cache(findOneList)
export const findManyListsCached = cache(findManyLists)