import {format as _format, isValid} from 'date-fns'
import nl from 'date-fns/locale/nl'
import en from 'date-fns/locale/en-US'

const locales = {
    nl,
    en,
}


export const formatDate = (date: string | Date, format: string = 'dd-MM-yyyy', locale: 'nl' | 'en'): string | null => {
    if (!date) return null

    const parsedDate = new Date(date)
    if (!isValid(parsedDate)) return null

    return _format(parsedDate, format, { // @ts-ignore
        locale: locales[locale] || nl,
    })
}