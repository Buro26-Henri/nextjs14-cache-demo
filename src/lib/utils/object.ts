import {camelCaseToDashes} from "./string";


export const objectKeysCamelCaseToDashes = <T, R>(object: T): R => {
    let obj: any = {}
    for (const [key, value] of Object.entries(object || {})) {
        obj[camelCaseToDashes(key)] = value
    }

    return obj
}