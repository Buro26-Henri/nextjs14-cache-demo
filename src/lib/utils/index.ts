import {generateRandomPassword} from './auth'
import {delay} from "./misc";
import {validateEmail} from "./validate";
import {getFromLocalStorage, hasInLocalStorage, localStorageIsDefined, removeFromLocalStorage, setLocalStorage} from "./localStorage";
import {objectKeysCamelCaseToDashes} from "./object";
import * as promise from "./promise";
import * as graphql from "./graphql";
import * as number from "./number";
import * as form from './form'
import * as date from './date'

export {generateRandomPassword}

export const utils = {
    auth: {
        generateRandomPassword,
    },
    localStorage: {
        get: getFromLocalStorage,
        set: setLocalStorage,
        has: hasInLocalStorage,
        remove: removeFromLocalStorage,
        isDefined: localStorageIsDefined,
    },
    misc: {
        delay,
    },
    object: {
        objectKeysCamelCaseToDashes,
    },
    string: {
        objectKeysCamelCaseToDashes,
    },
    validate: {
        email: validateEmail,
    },
    promise,
    graphql,
    number,
    form,
    date,
}

export default utils