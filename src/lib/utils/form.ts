import {isValid, parse} from "date-fns"

export const formatTimeValue = (value: string | null | undefined | Date) => {
    if (!value) return null

    if (value instanceof Date) return value

    const result = parse(value, 'HH:mm:ss.SSS', new Date())

    if (isValid(result)) return result

    return new Date(value)
}

