import {format} from 'date-fns'


export const formatDate = (date: string | null | undefined): string | null => {
    if (!date) {
        return null
    }

    return format(new Date(date), 'yyyy-MM-dd')
}

export type FormatDatesFromInputOptions = {
    removeEmptyDatesFromInput?: boolean
}

export const formatDatesFromInput = <T>(input: T, dateFields: (keyof T)[], options: FormatDatesFromInputOptions = {}): T => {
    const result = {...input}

    for (const field of dateFields) {
        if (options.removeEmptyDatesFromInput && !result[field]) {
            delete result[field]
            continue
        }

        if (result[field]) { // @ts-ignore
            result[field] = formatDate(result[field])
        }
    }

    return result
}


export const formatTime = (time: string | null | undefined): string | null => {
    if (!time) {
        return null
    }

    const [hours, minutes, seconds, milliseconds] = time.split(/[:.]/)

    const date = new Date()
    date.setHours(Number(hours))
    date.setMinutes(Number(minutes))
    date.setSeconds(Number(seconds))
    date.setMilliseconds(Number(milliseconds || 0))

    let formattedTime = format(date, 'HH:mm:ss')

    if (milliseconds) {
        formattedTime += `.${milliseconds.padEnd(3, '0')}`
    }

    return formattedTime
}

export type FormatTimesFromInputOptions = {
    removeEmptyTimesFromInput?: boolean
}

export const formatTimesFromInput = <T>(
    input: T,
    timeFields: (keyof T)[],
    options: FormatTimesFromInputOptions = {}
): T => {
    const result = { ...input }

    for (const field of timeFields) {
        if (options.removeEmptyTimesFromInput && !result[field]) {
            delete result[field]
            continue
        }

        if (result[field]) { // @ts-ignore
            result[field] = formatTime(result[field])
        }
    }

    return result
}