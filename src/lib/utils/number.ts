export const roundFloatInRange = (value: string | number, minValue: number, maxValue: number, step: number, decimals: number = 2): string => {
    // Convert the string value to a number
    const floatValue = typeof value !== "number" ? parseFloat(value) : value

    // Round the float value to the nearest step
    let roundedValue = Math.round(floatValue / step) * step

    // Clamp the rounded value within the given range
    roundedValue = Math.max(minValue, Math.min(maxValue, roundedValue))

    // If the rounded value is less than the min value, set it to the min value
    if (floatValue < minValue) {
        roundedValue = minValue
    }

    // If the rounded value exceeds the max value, set it to the max value
    if (floatValue > maxValue) {
        roundedValue = maxValue
    }

    // Format the rounded value to two decimal places
    const formattedValue = roundedValue.toFixed(decimals)

    return formattedValue.toString()
}
