

export const isFulfilled = <T,>(p:PromiseSettledResult<T>): p is PromiseFulfilledResult<T> => p.status === 'fulfilled'
export const isRejected = <T,>(p:PromiseSettledResult<T>): p is PromiseRejectedResult => p.status === 'rejected'

export const allSettled = async <T extends Promise<any>[]>(promises: readonly [...T]): Promise<[{
    [P in keyof T]: T[P] extends Promise<infer U> ? U | null : never
}, PromiseRejectedResult[]]> => {
    const results = await Promise.allSettled(promises)

    return [
        results.map(result => {
            if (isFulfilled(result)) {
                return result.value
            }

            return null
        }) as {
            [P in keyof T]: T[P] extends Promise<infer U> ? U | null : never
        },
        results.filter(isRejected)
    ]
}
