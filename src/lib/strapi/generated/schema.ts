import { z } from 'zod';

import * as Types from '@/lib/strapi/generated/types';


type Properties<T> = Required<{
  [K in keyof T]: z.ZodType<T[K], any, T[K]>;
}>


export function DateTimeSchema(): z.ZodSchema<string> {
    return z.string()
}

export function I18NLocaleCodeSchema(): z.ZodSchema<string> {
    return z.string()
}

export function JSONSchema(): z.ZodSchema<string> {
    return z.any()
}

export function LongSchema(): z.ZodSchema<number> {
    return z.coerce.number()
}

export function UploadSchema(): z.ZodSchema<any> {
    return z.any()
}


export function BestellingTypeSchema(): z.ZodSchema<Types.BestellingType> {
    return z.nativeEnum(Types.BestellingType)
}

export function ENUM_ADRES_LANDSchema(): z.ZodSchema<Types.Enum_Adres_Land> {
    return z.nativeEnum(Types.Enum_Adres_Land)
}

export function ENUM_ADRES_PROVINCIESchema(): z.ZodSchema<Types.Enum_Adres_Provincie> {
    return z.nativeEnum(Types.Enum_Adres_Provincie)
}

export function ENUM_ATTRIBUUT_WEERGAVESchema(): z.ZodSchema<Types.Enum_Attribuut_Weergave> {
    return z.nativeEnum(Types.Enum_Attribuut_Weergave)
}

export function ENUM_BANNER_KLEURSchema(): z.ZodSchema<Types.Enum_Banner_Kleur> {
    return z.nativeEnum(Types.Enum_Banner_Kleur)
}

export function ENUM_BESTELLING_BESTELLING_STATUSSchema(): z.ZodSchema<Types.Enum_Bestelling_Bestelling_Status> {
    return z.nativeEnum(Types.Enum_Bestelling_Bestelling_Status)
}

export function ENUM_BESTELLING_BESTELLING_TYPESchema(): z.ZodSchema<Types.Enum_Bestelling_Bestelling_Type> {
    return z.nativeEnum(Types.Enum_Bestelling_Bestelling_Type)
}

export function ENUM_COMPONENTSHAREDMETASOCIAL_SOCIALNETWORKSchema(): z.ZodSchema<Types.Enum_Componentsharedmetasocial_Socialnetwork> {
    return z.nativeEnum(Types.Enum_Componentsharedmetasocial_Socialnetwork)
}

export function ENUM_EMAIL_TYPESchema(): z.ZodSchema<Types.Enum_Email_Type> {
    return z.nativeEnum(Types.Enum_Email_Type)
}

export function ENUM_PRODUCT_PRODUCT_TYPESchema(): z.ZodSchema<Types.Enum_Product_Product_Type> {
    return z.nativeEnum(Types.Enum_Product_Product_Type)
}

export function ENUM_PRODUCT_ZICHTBAARSchema(): z.ZodSchema<Types.Enum_Product_Zichtbaar> {
    return z.nativeEnum(Types.Enum_Product_Zichtbaar)
}

export function NavigationRenderTypeSchema(): z.ZodSchema<Types.NavigationRenderType> {
    return z.nativeEnum(Types.NavigationRenderType)
}

export function PublicationStateSchema(): z.ZodSchema<Types.PublicationState> {
    return z.nativeEnum(Types.PublicationState)
}

export function TriggerEndpointsTypeSchema(): z.ZodSchema<Types.TriggerEndpointsType> {
    return z.nativeEnum(Types.TriggerEndpointsType)
}


export function AanmeldingInputObjectSchema(): z.ZodObject<Properties<Types.AanmeldingInputObject>> {
    return z.object({
        achternaam: z.string(),
        bedrijfsnaam: z.string(),
        btw_nummer: z.string().nullish().optional(),
        email: z.string(),
        huisnummer: z.string(),
        kvk_nummer: z.string().nullish().optional(),
        plaatsnaam: z.string(),
        postcode: z.string(),
        straatnaam: z.string(),
        telefoon: z.string(),
        voornaam: z.string()
    }) as z.ZodObject<Properties<Types.AanmeldingInputObject>>
}



export function AdresFiltersInputSchema(): z.ZodObject<Properties<Types.AdresFiltersInput>> {
    return z.object({
        achternaam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        aflever_bestellingen: z.lazy(() => BestellingFiltersInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => AdresFiltersInputSchema()).nullish().optional()).nullish().optional(),
        bedrijf: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        btw_nummer: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        budget: z.lazy(() => LongFilterInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        factuur_bestellingen: z.lazy(() => BestellingFiltersInputSchema()).nullish().optional(),
        huisnummer: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        klant: z.lazy(() => KlantFiltersInputSchema()).nullish().optional(),
        land: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        magento_adres_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        magento_customer_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => AdresFiltersInputSchema()).nullish().optional(),
        opmerking: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        optienaam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => AdresFiltersInputSchema()).nullish().optional()).nullish().optional(),
        plaats: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        postcode: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        projectcode: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        provincie: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        straatnaam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        telefoon: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        tussenvoegsel: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        voornaam: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.AdresFiltersInput>>
}



export function AdresInputSchema(): z.ZodObject<Properties<Types.AdresInput>> {
    return z.object({
        achternaam: z.string().nullish().optional(),
        aflever_bestellingen: z.array(z.string().nullish().optional()).nullish().optional(),
        bedrijf: z.string().nullish().optional(),
        btw_nummer: z.string().nullish().optional(),
        budget: z.lazy(() => LongSchema()).nullish().optional(),
        factuur_bestellingen: z.array(z.string().nullish().optional()).nullish().optional(),
        huisnummer: z.string().nullish().optional(),
        klant: z.string().nullish().optional(),
        land: z.lazy(() => ENUM_ADRES_LANDSchema()).nullish().optional(),
        magento_adres_id: z.coerce.number().nullish().optional(),
        magento_customer_id: z.coerce.number().nullish().optional(),
        opmerking: z.string().nullish().optional(),
        optienaam: z.string().nullish().optional(),
        plaats: z.string().nullish().optional(),
        postcode: z.string().nullish().optional(),
        projectcode: z.string().nullish().optional(),
        provincie: z.lazy(() => ENUM_ADRES_PROVINCIESchema()).nullish().optional(),
        straatnaam: z.string().nullish().optional(),
        telefoon: z.string().nullish().optional(),
        tussenvoegsel: z.string().nullish().optional(),
        voornaam: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.AdresInput>>
}



export function AdresInputObjectSchema(): z.ZodObject<Properties<Types.AdresInputObject>> {
    return z.object({
        achternaam: z.string().nullish().optional(),
        bedrijf: z.string(),
        btw_nummer: z.string().nullish().optional(),
        budget: z.coerce.number().nullish().optional(),
        huisnummer: z.string(),
        land: z.lazy(() => ENUM_ADRES_LANDSchema()).nullish().optional(),
        opmerking: z.string().nullish().optional(),
        plaats: z.string(),
        postcode: z.string(),
        projectcode: z.string().nullish().optional(),
        provincie: z.lazy(() => ENUM_ADRES_PROVINCIESchema()).nullish().optional(),
        straatnaam: z.string(),
        telefoon: z.string(),
        tussenvoegsel: z.string().nullish().optional(),
        voornaam: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.AdresInputObject>>
}



export function AttribuutFiltersInputSchema(): z.ZodObject<Properties<Types.AttribuutFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => AttribuutFiltersInputSchema()).nullish().optional()).nullish().optional(),
        attribuut_waardes: z.lazy(() => AttribuutWaardeFiltersInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        magento_arttributeid: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        naam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => AttribuutFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => AttribuutFiltersInputSchema()).nullish().optional()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        weergave: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        weergavenaam: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.AttribuutFiltersInput>>
}



export function AttribuutInputSchema(): z.ZodObject<Properties<Types.AttribuutInput>> {
    return z.object({
        attribuut_waardes: z.array(z.string().nullish().optional()).nullish().optional(),
        magento_arttributeid: z.coerce.number().nullish().optional(),
        naam: z.string().nullish().optional(),
        weergave: z.lazy(() => ENUM_ATTRIBUUT_WEERGAVESchema()).nullish().optional(),
        weergavenaam: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.AttribuutInput>>
}



export function AttribuutWaardeFiltersInputSchema(): z.ZodObject<Properties<Types.AttribuutWaardeFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => AttribuutWaardeFiltersInputSchema()).nullish().optional()).nullish().optional(),
        attribuut: z.lazy(() => AttribuutFiltersInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        magento_attributeid: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        magento_product_ids: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        naam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => AttribuutWaardeFiltersInputSchema()).nullish().optional(),
        optienaam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => AttribuutWaardeFiltersInputSchema()).nullish().optional()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        weergavenaam: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.AttribuutWaardeFiltersInput>>
}



export function AttribuutWaardeInputSchema(): z.ZodObject<Properties<Types.AttribuutWaardeInput>> {
    return z.object({
        attribuut: z.string().nullish().optional(),
        magento_attributeid: z.coerce.number().nullish().optional(),
        magento_product_ids: z.string().nullish().optional(),
        naam: z.string().nullish().optional(),
        optienaam: z.string().nullish().optional(),
        weergavenaam: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.AttribuutWaardeInput>>
}



export function BannerFiltersInputSchema(): z.ZodObject<Properties<Types.BannerFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => BannerFiltersInputSchema()).nullish().optional()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        inhoud: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        kleur: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => BannerFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => BannerFiltersInputSchema()).nullish().optional()).nullish().optional(),
        product_prijs: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        product_prijs_aanbieding: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        publishedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        titel: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        url: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.BannerFiltersInput>>
}



export function BestellingFiltersInputSchema(): z.ZodObject<Properties<Types.BestellingFiltersInput>> {
    return z.object({
        afleveradres: z.lazy(() => AdresFiltersInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => BestellingFiltersInputSchema()).nullish().optional()).nullish().optional(),
        bestelling_status: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        bestelling_type: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        bestelnummer: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        datum_geplaatst: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        emails: z.lazy(() => ComponentMiscFacturatieFiltersInputSchema()).nullish().optional(),
        factuuradres: z.lazy(() => AdresFiltersInputSchema()).nullish().optional(),
        gebruiker: z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        klant: z.lazy(() => KlantFiltersInputSchema()).nullish().optional(),
        not: z.lazy(() => BestellingFiltersInputSchema()).nullish().optional(),
        opmerking: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => BestellingFiltersInputSchema()).nullish().optional()).nullish().optional(),
        producten: z.lazy(() => ComponentBestellingProductenFiltersInputSchema()).nullish().optional(),
        snapshot: z.lazy(() => JSONFilterInputSchema()).nullish().optional(),
        subtotaal: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        totaal: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        verzendkosten: z.lazy(() => FloatFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.BestellingFiltersInput>>
}



export function BestellingInputObjectSchema(): z.ZodObject<Properties<Types.BestellingInputObject>> {
    return z.object({
        afleveradres: z.coerce.number().nullish().optional(),
        bestelling_type: z.lazy(() => ENUM_BESTELLING_BESTELLING_TYPESchema()).nullish().optional(),
        emails: z.array(z.string().nullish().optional()).nullish().optional(),
        opmerking: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.BestellingInputObject>>
}



export function BooleanFilterInputSchema(): z.ZodObject<Properties<Types.BooleanFilterInput>> {
    return z.object({
        and: z.array(z.coerce.boolean().nullish().optional()).nullish().optional(),
        between: z.array(z.coerce.boolean().nullish().optional()).nullish().optional(),
        contains: z.coerce.boolean().nullish().optional(),
        containsi: z.coerce.boolean().nullish().optional(),
        endsWith: z.coerce.boolean().nullish().optional(),
        eq: z.coerce.boolean().nullish().optional(),
        eqi: z.coerce.boolean().nullish().optional(),
        gt: z.coerce.boolean().nullish().optional(),
        gte: z.coerce.boolean().nullish().optional(),
        in: z.array(z.coerce.boolean().nullish().optional()).nullish().optional(),
        lt: z.coerce.boolean().nullish().optional(),
        lte: z.coerce.boolean().nullish().optional(),
        ne: z.coerce.boolean().nullish().optional(),
        nei: z.coerce.boolean().nullish().optional(),
        not: z.lazy(() => BooleanFilterInputSchema()).nullish().optional(),
        notContains: z.coerce.boolean().nullish().optional(),
        notContainsi: z.coerce.boolean().nullish().optional(),
        notIn: z.array(z.coerce.boolean().nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.coerce.boolean().nullish().optional()).nullish().optional(),
        startsWith: z.coerce.boolean().nullish().optional()
    }) as z.ZodObject<Properties<Types.BooleanFilterInput>>
}



export function CategorieFiltersInputSchema(): z.ZodObject<Properties<Types.CategorieFiltersInput>> {
    return z.object({
        alias: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => CategorieFiltersInputSchema()).nullish().optional()).nullish().optional(),
        beschrijving: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        hoofdcategorie: z.lazy(() => CategorieFiltersInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        klant: z.lazy(() => KlantFiltersInputSchema()).nullish().optional(),
        magento_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        magento_parent_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        naam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => CategorieFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => CategorieFiltersInputSchema()).nullish().optional()).nullish().optional(),
        pad: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        producten: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        publishedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        seo: z.lazy(() => ComponentSharedSeoFiltersInputSchema()).nullish().optional(),
        subcategorie: z.lazy(() => CategorieFiltersInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.CategorieFiltersInput>>
}



export function ComponentBestellingProductenFiltersInputSchema(): z.ZodObject<Properties<Types.ComponentBestellingProductenFiltersInput>> {
    return z.object({
        aantal: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => ComponentBestellingProductenFiltersInputSchema()).nullish().optional()).nullish().optional(),
        not: z.lazy(() => ComponentBestellingProductenFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => ComponentBestellingProductenFiltersInputSchema()).nullish().optional()).nullish().optional(),
        product: z.lazy(() => ProductFiltersInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.ComponentBestellingProductenFiltersInput>>
}



export function ComponentMiscFacturatieFiltersInputSchema(): z.ZodObject<Properties<Types.ComponentMiscFacturatieFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => ComponentMiscFacturatieFiltersInputSchema()).nullish().optional()).nullish().optional(),
        email: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => ComponentMiscFacturatieFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => ComponentMiscFacturatieFiltersInputSchema()).nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.ComponentMiscFacturatieFiltersInput>>
}



export function ComponentMiscFacturatieInputSchema(): z.ZodObject<Properties<Types.ComponentMiscFacturatieInput>> {
    return z.object({
        email: z.string().nullish().optional(),
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.ComponentMiscFacturatieInput>>
}



export function ComponentProductAttributenFiltersInputSchema(): z.ZodObject<Properties<Types.ComponentProductAttributenFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => ComponentProductAttributenFiltersInputSchema()).nullish().optional()).nullish().optional(),
        attribuut_waarde: z.lazy(() => AttribuutWaardeFiltersInputSchema()).nullish().optional(),
        not: z.lazy(() => ComponentProductAttributenFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => ComponentProductAttributenFiltersInputSchema()).nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.ComponentProductAttributenFiltersInput>>
}



export function ComponentSharedMetaSocialFiltersInputSchema(): z.ZodObject<Properties<Types.ComponentSharedMetaSocialFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => ComponentSharedMetaSocialFiltersInputSchema()).nullish().optional()).nullish().optional(),
        description: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => ComponentSharedMetaSocialFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => ComponentSharedMetaSocialFiltersInputSchema()).nullish().optional()).nullish().optional(),
        socialNetwork: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        title: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.ComponentSharedMetaSocialFiltersInput>>
}



export function ComponentSharedSeoFiltersInputSchema(): z.ZodObject<Properties<Types.ComponentSharedSeoFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => ComponentSharedSeoFiltersInputSchema()).nullish().optional()).nullish().optional(),
        canonicalURL: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        keywords: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        metaDescription: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        metaRobots: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        metaSocial: z.lazy(() => ComponentSharedMetaSocialFiltersInputSchema()).nullish().optional(),
        metaTitle: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        metaViewport: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => ComponentSharedSeoFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => ComponentSharedSeoFiltersInputSchema()).nullish().optional()).nullish().optional(),
        structuredData: z.lazy(() => JSONFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.ComponentSharedSeoFiltersInput>>
}



export function DateTimeFilterInputSchema(): z.ZodObject<Properties<Types.DateTimeFilterInput>> {
    return z.object({
        and: z.array(z.lazy(() => DateTimeSchema()).nullish().optional()).nullish().optional(),
        between: z.array(z.lazy(() => DateTimeSchema()).nullish().optional()).nullish().optional(),
        contains: z.lazy(() => DateTimeSchema()).nullish().optional(),
        containsi: z.lazy(() => DateTimeSchema()).nullish().optional(),
        endsWith: z.lazy(() => DateTimeSchema()).nullish().optional(),
        eq: z.lazy(() => DateTimeSchema()).nullish().optional(),
        eqi: z.lazy(() => DateTimeSchema()).nullish().optional(),
        gt: z.lazy(() => DateTimeSchema()).nullish().optional(),
        gte: z.lazy(() => DateTimeSchema()).nullish().optional(),
        in: z.array(z.lazy(() => DateTimeSchema()).nullish().optional()).nullish().optional(),
        lt: z.lazy(() => DateTimeSchema()).nullish().optional(),
        lte: z.lazy(() => DateTimeSchema()).nullish().optional(),
        ne: z.lazy(() => DateTimeSchema()).nullish().optional(),
        nei: z.lazy(() => DateTimeSchema()).nullish().optional(),
        not: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        notContains: z.lazy(() => DateTimeSchema()).nullish().optional(),
        notContainsi: z.lazy(() => DateTimeSchema()).nullish().optional(),
        notIn: z.array(z.lazy(() => DateTimeSchema()).nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.lazy(() => DateTimeSchema()).nullish().optional()).nullish().optional(),
        startsWith: z.lazy(() => DateTimeSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.DateTimeFilterInput>>
}



export function DocumentFiltersInputSchema(): z.ZodObject<Properties<Types.DocumentFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => DocumentFiltersInputSchema()).nullish().optional()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        magento_docid: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        naam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => DocumentFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => DocumentFiltersInputSchema()).nullish().optional()).nullish().optional(),
        products: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        publiek: z.lazy(() => BooleanFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.DocumentFiltersInput>>
}



export function DubliceerFavorietenlijstInputObjectSchema(): z.ZodObject<Properties<Types.DubliceerFavorietenlijstInputObject>> {
    return z.object({
        naam: z.string()
    }) as z.ZodObject<Properties<Types.DubliceerFavorietenlijstInputObject>>
}



export function FavorietenlijstFiltersInputSchema(): z.ZodObject<Properties<Types.FavorietenlijstFiltersInput>> {
    return z.object({
        alias: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => FavorietenlijstFiltersInputSchema()).nullish().optional()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        gebruiker: z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        magento_customer_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        magento_product_ids: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        naam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => FavorietenlijstFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => FavorietenlijstFiltersInputSchema()).nullish().optional()).nullish().optional(),
        producten: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        uuid: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.FavorietenlijstFiltersInput>>
}



export function FavorietenlijstInputObjectSchema(): z.ZodObject<Properties<Types.FavorietenlijstInputObject>> {
    return z.object({
        naam: z.string(),
        producten: z.array(z.coerce.number()).nullish().optional()
    }) as z.ZodObject<Properties<Types.FavorietenlijstInputObject>>
}



export function FileInfoInputSchema(): z.ZodObject<Properties<Types.FileInfoInput>> {
    return z.object({
        alternativeText: z.string().nullish().optional(),
        caption: z.string().nullish().optional(),
        name: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.FileInfoInput>>
}



export function FloatFilterInputSchema(): z.ZodObject<Properties<Types.FloatFilterInput>> {
    return z.object({
        and: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        between: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        contains: z.coerce.number().nullish().optional(),
        containsi: z.coerce.number().nullish().optional(),
        endsWith: z.coerce.number().nullish().optional(),
        eq: z.coerce.number().nullish().optional(),
        eqi: z.coerce.number().nullish().optional(),
        gt: z.coerce.number().nullish().optional(),
        gte: z.coerce.number().nullish().optional(),
        in: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        lt: z.coerce.number().nullish().optional(),
        lte: z.coerce.number().nullish().optional(),
        ne: z.coerce.number().nullish().optional(),
        nei: z.coerce.number().nullish().optional(),
        not: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        notContains: z.coerce.number().nullish().optional(),
        notContainsi: z.coerce.number().nullish().optional(),
        notIn: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        startsWith: z.coerce.number().nullish().optional()
    }) as z.ZodObject<Properties<Types.FloatFilterInput>>
}



export function I18NLocaleFiltersInputSchema(): z.ZodObject<Properties<Types.I18NLocaleFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => I18NLocaleFiltersInputSchema()).nullish().optional()).nullish().optional(),
        code: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        name: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => I18NLocaleFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => I18NLocaleFiltersInputSchema()).nullish().optional()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.I18NLocaleFiltersInput>>
}



export function IDFilterInputSchema(): z.ZodObject<Properties<Types.IdFilterInput>> {
    return z.object({
        and: z.array(z.string().nullish().optional()).nullish().optional(),
        between: z.array(z.string().nullish().optional()).nullish().optional(),
        contains: z.string().nullish().optional(),
        containsi: z.string().nullish().optional(),
        endsWith: z.string().nullish().optional(),
        eq: z.string().nullish().optional(),
        eqi: z.string().nullish().optional(),
        gt: z.string().nullish().optional(),
        gte: z.string().nullish().optional(),
        in: z.array(z.string().nullish().optional()).nullish().optional(),
        lt: z.string().nullish().optional(),
        lte: z.string().nullish().optional(),
        ne: z.string().nullish().optional(),
        nei: z.string().nullish().optional(),
        not: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        notContains: z.string().nullish().optional(),
        notContainsi: z.string().nullish().optional(),
        notIn: z.array(z.string().nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.string().nullish().optional()).nullish().optional(),
        startsWith: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.IdFilterInput>>
}



export function IntFilterInputSchema(): z.ZodObject<Properties<Types.IntFilterInput>> {
    return z.object({
        and: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        between: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        contains: z.coerce.number().nullish().optional(),
        containsi: z.coerce.number().nullish().optional(),
        endsWith: z.coerce.number().nullish().optional(),
        eq: z.coerce.number().nullish().optional(),
        eqi: z.coerce.number().nullish().optional(),
        gt: z.coerce.number().nullish().optional(),
        gte: z.coerce.number().nullish().optional(),
        in: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        lt: z.coerce.number().nullish().optional(),
        lte: z.coerce.number().nullish().optional(),
        ne: z.coerce.number().nullish().optional(),
        nei: z.coerce.number().nullish().optional(),
        not: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        notContains: z.coerce.number().nullish().optional(),
        notContainsi: z.coerce.number().nullish().optional(),
        notIn: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.coerce.number().nullish().optional()).nullish().optional(),
        startsWith: z.coerce.number().nullish().optional()
    }) as z.ZodObject<Properties<Types.IntFilterInput>>
}



export function JSONFilterInputSchema(): z.ZodObject<Properties<Types.JsonFilterInput>> {
    return z.object({
        and: z.array(z.lazy(() => JSONSchema()).nullish().optional()).nullish().optional(),
        between: z.array(z.lazy(() => JSONSchema()).nullish().optional()).nullish().optional(),
        contains: z.lazy(() => JSONSchema()).nullish().optional(),
        containsi: z.lazy(() => JSONSchema()).nullish().optional(),
        endsWith: z.lazy(() => JSONSchema()).nullish().optional(),
        eq: z.lazy(() => JSONSchema()).nullish().optional(),
        eqi: z.lazy(() => JSONSchema()).nullish().optional(),
        gt: z.lazy(() => JSONSchema()).nullish().optional(),
        gte: z.lazy(() => JSONSchema()).nullish().optional(),
        in: z.array(z.lazy(() => JSONSchema()).nullish().optional()).nullish().optional(),
        lt: z.lazy(() => JSONSchema()).nullish().optional(),
        lte: z.lazy(() => JSONSchema()).nullish().optional(),
        ne: z.lazy(() => JSONSchema()).nullish().optional(),
        nei: z.lazy(() => JSONSchema()).nullish().optional(),
        not: z.lazy(() => JSONFilterInputSchema()).nullish().optional(),
        notContains: z.lazy(() => JSONSchema()).nullish().optional(),
        notContainsi: z.lazy(() => JSONSchema()).nullish().optional(),
        notIn: z.array(z.lazy(() => JSONSchema()).nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.lazy(() => JSONSchema()).nullish().optional()).nullish().optional(),
        startsWith: z.lazy(() => JSONSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.JsonFilterInput>>
}



export function KlantFiltersInputSchema(): z.ZodObject<Properties<Types.KlantFiltersInput>> {
    return z.object({
        adres: z.lazy(() => AdresFiltersInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => KlantFiltersInputSchema()).nullish().optional()).nullish().optional(),
        bestellingen: z.lazy(() => BestellingFiltersInputSchema()).nullish().optional(),
        categorieen: z.lazy(() => CategorieFiltersInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        factuurEmail: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        factuuradres: z.lazy(() => AdresFiltersInputSchema()).nullish().optional(),
        gebruiker: z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        magento_customer_ids: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        naam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => KlantFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => KlantFiltersInputSchema()).nullish().optional()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.KlantFiltersInput>>
}



export function LongFilterInputSchema(): z.ZodObject<Properties<Types.LongFilterInput>> {
    return z.object({
        and: z.array(z.lazy(() => LongSchema()).nullish().optional()).nullish().optional(),
        between: z.array(z.lazy(() => LongSchema()).nullish().optional()).nullish().optional(),
        contains: z.lazy(() => LongSchema()).nullish().optional(),
        containsi: z.lazy(() => LongSchema()).nullish().optional(),
        endsWith: z.lazy(() => LongSchema()).nullish().optional(),
        eq: z.lazy(() => LongSchema()).nullish().optional(),
        eqi: z.lazy(() => LongSchema()).nullish().optional(),
        gt: z.lazy(() => LongSchema()).nullish().optional(),
        gte: z.lazy(() => LongSchema()).nullish().optional(),
        in: z.array(z.lazy(() => LongSchema()).nullish().optional()).nullish().optional(),
        lt: z.lazy(() => LongSchema()).nullish().optional(),
        lte: z.lazy(() => LongSchema()).nullish().optional(),
        ne: z.lazy(() => LongSchema()).nullish().optional(),
        nei: z.lazy(() => LongSchema()).nullish().optional(),
        not: z.lazy(() => LongFilterInputSchema()).nullish().optional(),
        notContains: z.lazy(() => LongSchema()).nullish().optional(),
        notContainsi: z.lazy(() => LongSchema()).nullish().optional(),
        notIn: z.array(z.lazy(() => LongSchema()).nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.lazy(() => LongSchema()).nullish().optional()).nullish().optional(),
        startsWith: z.lazy(() => LongSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.LongFilterInput>>
}



export function NieuwFacturatieEmailInputObjectSchema(): z.ZodObject<Properties<Types.NieuwFacturatieEmailInputObject>> {
    return z.object({
        email: z.string()
    }) as z.ZodObject<Properties<Types.NieuwFacturatieEmailInputObject>>
}



export function PaginaFiltersInputSchema(): z.ZodObject<Properties<Types.PaginaFiltersInput>> {
    return z.object({
        alias: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => PaginaFiltersInputSchema()).nullish().optional()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => PaginaFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => PaginaFiltersInputSchema()).nullish().optional()).nullish().optional(),
        publishedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        seo: z.lazy(() => ComponentSharedSeoFiltersInputSchema()).nullish().optional(),
        titel: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        toonTitel: z.lazy(() => BooleanFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.PaginaFiltersInput>>
}



export function PaginationArgSchema(): z.ZodObject<Properties<Types.PaginationArg>> {
    return z.object({
        limit: z.coerce.number().nullish().optional(),
        page: z.coerce.number().nullish().optional(),
        pageSize: z.coerce.number().nullish().optional(),
        start: z.coerce.number().nullish().optional()
    }) as z.ZodObject<Properties<Types.PaginationArg>>
}



export function PlaatsInFavorietenlijstInputObjectSchema(): z.ZodObject<Properties<Types.PlaatsInFavorietenlijstInputObject>> {
    return z.object({
        product: z.coerce.number()
    }) as z.ZodObject<Properties<Types.PlaatsInFavorietenlijstInputObject>>
}



export function PlaatsProductInWinkelwagenInputObjectSchema(): z.ZodObject<Properties<Types.PlaatsProductInWinkelwagenInputObject>> {
    return z.object({
        aantal: z.coerce.number().nullish().optional(),
        product: z.coerce.number()
    }) as z.ZodObject<Properties<Types.PlaatsProductInWinkelwagenInputObject>>
}



export function ProductFiltersInputSchema(): z.ZodObject<Properties<Types.ProductFiltersInput>> {
    return z.object({
        Magento_product_catids: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        alias: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => ProductFiltersInputSchema()).nullish().optional()).nullish().optional(),
        attribuut: z.lazy(() => ComponentProductAttributenFiltersInputSchema()).nullish().optional(),
        beschrijving: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        categorie: z.lazy(() => CategorieFiltersInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        documentens: z.lazy(() => DocumentFiltersInputSchema()).nullish().optional(),
        favorietenlijsten: z.lazy(() => FavorietenlijstFiltersInputSchema()).nullish().optional(),
        gerelateerd: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        hoofdpad: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        magento_parent_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        magento_product_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        magento_product_links: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        naam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => ProductFiltersInputSchema()).nullish().optional()).nullish().optional(),
        prijs: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        prijs_aanbieding: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        product_type: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        producten: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        productnummer: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        publishedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        seo: z.lazy(() => ComponentSharedSeoFiltersInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        zichtbaar: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.ProductFiltersInput>>
}



export function ProductstatistiekInputObjectSchema(): z.ZodObject<Properties<Types.ProductstatistiekInputObject>> {
    return z.object({
        afleverAdres: z.string().nullish().optional(),
        soort: z.lazy(() => BestellingTypeSchema()).nullish().optional(),
        tot: z.string().nullish().optional(),
        vanaf: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.ProductstatistiekInputObject>>
}



export function StringFilterInputSchema(): z.ZodObject<Properties<Types.StringFilterInput>> {
    return z.object({
        and: z.array(z.string().nullish().optional()).nullish().optional(),
        between: z.array(z.string().nullish().optional()).nullish().optional(),
        contains: z.string().nullish().optional(),
        containsi: z.string().nullish().optional(),
        endsWith: z.string().nullish().optional(),
        eq: z.string().nullish().optional(),
        eqi: z.string().nullish().optional(),
        gt: z.string().nullish().optional(),
        gte: z.string().nullish().optional(),
        in: z.array(z.string().nullish().optional()).nullish().optional(),
        lt: z.string().nullish().optional(),
        lte: z.string().nullish().optional(),
        ne: z.string().nullish().optional(),
        nei: z.string().nullish().optional(),
        not: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        notContains: z.string().nullish().optional(),
        notContainsi: z.string().nullish().optional(),
        notIn: z.array(z.string().nullish().optional()).nullish().optional(),
        notNull: z.coerce.boolean().nullish().optional(),
        null: z.coerce.boolean().nullish().optional(),
        or: z.array(z.string().nullish().optional()).nullish().optional(),
        startsWith: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.StringFilterInput>>
}



export function UpdateProductInWinkelwagenInputObjectSchema(): z.ZodObject<Properties<Types.UpdateProductInWinkelwagenInputObject>> {
    return z.object({
        aantal: z.coerce.number().nullish().optional()
    }) as z.ZodObject<Properties<Types.UpdateProductInWinkelwagenInputObject>>
}



export function UploadFileFiltersInputSchema(): z.ZodObject<Properties<Types.UploadFileFiltersInput>> {
    return z.object({
        alternativeText: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => UploadFileFiltersInputSchema()).nullish().optional()).nullish().optional(),
        caption: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        ext: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        folder: z.lazy(() => UploadFolderFiltersInputSchema()).nullish().optional(),
        folderPath: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        formats: z.lazy(() => JSONFilterInputSchema()).nullish().optional(),
        hash: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        height: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        mime: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        name: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => UploadFileFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => UploadFileFiltersInputSchema()).nullish().optional()).nullish().optional(),
        previewUrl: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        provider: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        provider_metadata: z.lazy(() => JSONFilterInputSchema()).nullish().optional(),
        size: z.lazy(() => FloatFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        url: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        width: z.lazy(() => IntFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.UploadFileFiltersInput>>
}



export function UploadFileInputSchema(): z.ZodObject<Properties<Types.UploadFileInput>> {
    return z.object({
        alternativeText: z.string().nullish().optional(),
        caption: z.string().nullish().optional(),
        ext: z.string().nullish().optional(),
        folder: z.string().nullish().optional(),
        folderPath: z.string().nullish().optional(),
        formats: z.lazy(() => JSONSchema()).nullish().optional(),
        hash: z.string().nullish().optional(),
        height: z.coerce.number().nullish().optional(),
        mime: z.string().nullish().optional(),
        name: z.string().nullish().optional(),
        previewUrl: z.string().nullish().optional(),
        provider: z.string().nullish().optional(),
        provider_metadata: z.lazy(() => JSONSchema()).nullish().optional(),
        size: z.coerce.number().nullish().optional(),
        url: z.string().nullish().optional(),
        width: z.coerce.number().nullish().optional()
    }) as z.ZodObject<Properties<Types.UploadFileInput>>
}



export function UploadFolderFiltersInputSchema(): z.ZodObject<Properties<Types.UploadFolderFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => UploadFolderFiltersInputSchema()).nullish().optional()).nullish().optional(),
        children: z.lazy(() => UploadFolderFiltersInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        files: z.lazy(() => UploadFileFiltersInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        name: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => UploadFolderFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => UploadFolderFiltersInputSchema()).nullish().optional()).nullish().optional(),
        parent: z.lazy(() => UploadFolderFiltersInputSchema()).nullish().optional(),
        path: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        pathId: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.UploadFolderFiltersInput>>
}



export function UploadFolderInputSchema(): z.ZodObject<Properties<Types.UploadFolderInput>> {
    return z.object({
        children: z.array(z.string().nullish().optional()).nullish().optional(),
        files: z.array(z.string().nullish().optional()).nullish().optional(),
        name: z.string().nullish().optional(),
        parent: z.string().nullish().optional(),
        path: z.string().nullish().optional(),
        pathId: z.coerce.number().nullish().optional()
    }) as z.ZodObject<Properties<Types.UploadFolderInput>>
}



export function UsersPermissionsLoginInputSchema(): z.ZodObject<Properties<Types.UsersPermissionsLoginInput>> {
    return z.object({
        identifier: z.string(),
        password: z.string(),
        provider: z.string()
    }) as z.ZodObject<Properties<Types.UsersPermissionsLoginInput>>
}



export function UsersPermissionsPermissionFiltersInputSchema(): z.ZodObject<Properties<Types.UsersPermissionsPermissionFiltersInput>> {
    return z.object({
        action: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => UsersPermissionsPermissionFiltersInputSchema()).nullish().optional()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => UsersPermissionsPermissionFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => UsersPermissionsPermissionFiltersInputSchema()).nullish().optional()).nullish().optional(),
        role: z.lazy(() => UsersPermissionsRoleFiltersInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.UsersPermissionsPermissionFiltersInput>>
}



export function UsersPermissionsRegisterInputSchema(): z.ZodObject<Properties<Types.UsersPermissionsRegisterInput>> {
    return z.object({
        email: z.string(),
        password: z.string(),
        username: z.string()
    }) as z.ZodObject<Properties<Types.UsersPermissionsRegisterInput>>
}



export function UsersPermissionsRoleFiltersInputSchema(): z.ZodObject<Properties<Types.UsersPermissionsRoleFiltersInput>> {
    return z.object({
        and: z.array(z.lazy(() => UsersPermissionsRoleFiltersInputSchema()).nullish().optional()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        description: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        name: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => UsersPermissionsRoleFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => UsersPermissionsRoleFiltersInputSchema()).nullish().optional()).nullish().optional(),
        permissions: z.lazy(() => UsersPermissionsPermissionFiltersInputSchema()).nullish().optional(),
        type: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        users: z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.UsersPermissionsRoleFiltersInput>>
}



export function UsersPermissionsRoleInputSchema(): z.ZodObject<Properties<Types.UsersPermissionsRoleInput>> {
    return z.object({
        description: z.string().nullish().optional(),
        name: z.string().nullish().optional(),
        permissions: z.array(z.string().nullish().optional()).nullish().optional(),
        type: z.string().nullish().optional(),
        users: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.UsersPermissionsRoleInput>>
}



export function UsersPermissionsUserFiltersInputSchema(): z.ZodObject<Properties<Types.UsersPermissionsUserFiltersInput>> {
    return z.object({
        achternaam: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        and: z.array(z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional()).nullish().optional(),
        bestellingen: z.lazy(() => BestellingFiltersInputSchema()).nullish().optional(),
        blocked: z.lazy(() => BooleanFilterInputSchema()).nullish().optional(),
        confirmationToken: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        confirmed: z.lazy(() => BooleanFilterInputSchema()).nullish().optional(),
        createdAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        email: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        facturatie: z.lazy(() => ComponentMiscFacturatieFiltersInputSchema()).nullish().optional(),
        favorietenlijsten: z.lazy(() => FavorietenlijstFiltersInputSchema()).nullish().optional(),
        id: z.lazy(() => IDFilterInputSchema()).nullish().optional(),
        klant: z.lazy(() => KlantFiltersInputSchema()).nullish().optional(),
        magento_customer_id: z.lazy(() => IntFilterInputSchema()).nullish().optional(),
        not: z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional(),
        or: z.array(z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional()).nullish().optional(),
        password: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        provider: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        resetPasswordToken: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        role: z.lazy(() => UsersPermissionsRoleFiltersInputSchema()).nullish().optional(),
        updatedAt: z.lazy(() => DateTimeFilterInputSchema()).nullish().optional(),
        username: z.lazy(() => StringFilterInputSchema()).nullish().optional(),
        voornaam: z.lazy(() => StringFilterInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.UsersPermissionsUserFiltersInput>>
}



export function UsersPermissionsUserInputSchema(): z.ZodObject<Properties<Types.UsersPermissionsUserInput>> {
    return z.object({
        achternaam: z.string().nullish().optional(),
        bestellingen: z.array(z.string().nullish().optional()).nullish().optional(),
        blocked: z.coerce.boolean().nullish().optional(),
        confirmationToken: z.string().nullish().optional(),
        confirmed: z.coerce.boolean().nullish().optional(),
        email: z.string().nullish().optional(),
        facturatie: z.array(z.lazy(() => ComponentMiscFacturatieInputSchema()).nullish().optional()).nullish().optional(),
        favorietenlijsten: z.array(z.string().nullish().optional()).nullish().optional(),
        klant: z.string().nullish().optional(),
        magento_customer_id: z.coerce.number().nullish().optional(),
        password: z.string().nullish().optional(),
        provider: z.string().nullish().optional(),
        resetPasswordToken: z.string().nullish().optional(),
        role: z.string().nullish().optional(),
        username: z.string().nullish().optional(),
        voornaam: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.UsersPermissionsUserInput>>
}


export function AdresFindManySchemaGenerated(): z.ZodObject<Properties<Types.AdresFindManyQueryVariables>> {
    return z.object({
        filters: z.lazy(() => AdresFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.AdresFindManyQueryVariables>>
}



export function AdresFindSchemaGenerated(): z.ZodObject<Properties<Types.AdresFindQueryVariables>> {
    return z.object({
        filters: z.lazy(() => AdresFiltersInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.AdresFindQueryVariables>>
}



export function FindNavigationSchemaGenerated(): z.ZodObject<Properties<Types.FindNavigationQueryVariables>> {
    return z.object({
        navigationId: z.string()
    }) as z.ZodObject<Properties<Types.FindNavigationQueryVariables>>
}



export function FindNavigationItemByPathSchemaGenerated(): z.ZodObject<Properties<Types.FindNavigationItemByPathQueryVariables>> {
    return z.object({
        navigationId: z.string(),
        path: z.string()
    }) as z.ZodObject<Properties<Types.FindNavigationItemByPathQueryVariables>>
}



export function PaginaSchemaGenerated(): z.ZodObject<Properties<Types.PaginaQueryVariables>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.PaginaQueryVariables>>
}



export function PageByAliasSchemaGenerated(): z.ZodObject<Properties<Types.PageByAliasQueryVariables>> {
    return z.object({
        alias: z.string()
    }) as z.ZodObject<Properties<Types.PageByAliasQueryVariables>>
}


export function ChangePasswordMutationInputSchema(): z.ZodObject<Properties<Types.MutationChangePasswordArgs>> {
    return z.object({
        currentPassword: z.string(),
        password: z.string(),
        passwordConfirmation: z.string()
    }) as z.ZodObject<Properties<Types.MutationChangePasswordArgs>>
}



export function CreateAanmeldingMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateAanmeldingArgs>> {
    return z.object({
        input: z.lazy(() => AanmeldingInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateAanmeldingArgs>>
}



export function CreateAdresMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateAdresArgs>> {
    return z.object({
        input: z.lazy(() => AdresInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateAdresArgs>>
}



export function CreateAttribuutMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateAttribuutArgs>> {
    return z.object({
        data: z.lazy(() => AttribuutInputSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateAttribuutArgs>>
}



export function CreateAttribuutWaardeMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateAttribuutWaardeArgs>> {
    return z.object({
        data: z.lazy(() => AttribuutWaardeInputSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateAttribuutWaardeArgs>>
}



export function CreateFavorietenlijstMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateFavorietenlijstArgs>> {
    return z.object({
        input: z.lazy(() => FavorietenlijstInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateFavorietenlijstArgs>>
}



export function CreateUploadFileMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateUploadFileArgs>> {
    return z.object({
        data: z.lazy(() => UploadFileInputSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateUploadFileArgs>>
}



export function CreateUploadFolderMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateUploadFolderArgs>> {
    return z.object({
        data: z.lazy(() => UploadFolderInputSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateUploadFolderArgs>>
}



export function CreateUsersPermissionsRoleMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateUsersPermissionsRoleArgs>> {
    return z.object({
        data: z.lazy(() => UsersPermissionsRoleInputSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateUsersPermissionsRoleArgs>>
}



export function CreateUsersPermissionsUserMutationInputSchema(): z.ZodObject<Properties<Types.MutationCreateUsersPermissionsUserArgs>> {
    return z.object({
        data: z.lazy(() => UsersPermissionsUserInputSchema())
    }) as z.ZodObject<Properties<Types.MutationCreateUsersPermissionsUserArgs>>
}



export function DeleteAdresMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteAdresArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteAdresArgs>>
}



export function DeleteAttribuutMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteAttribuutArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteAttribuutArgs>>
}



export function DeleteAttribuutWaardeMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteAttribuutWaardeArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteAttribuutWaardeArgs>>
}



export function DeleteFavorietenlijstMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteFavorietenlijstArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteFavorietenlijstArgs>>
}



export function DeleteUploadFileMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteUploadFileArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteUploadFileArgs>>
}



export function DeleteUploadFolderMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteUploadFolderArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteUploadFolderArgs>>
}



export function DeleteUsersPermissionsRoleMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteUsersPermissionsRoleArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteUsersPermissionsRoleArgs>>
}



export function DeleteUsersPermissionsUserMutationInputSchema(): z.ZodObject<Properties<Types.MutationDeleteUsersPermissionsUserArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationDeleteUsersPermissionsUserArgs>>
}



export function DubliceerFavorietenlijstMutationInputSchema(): z.ZodObject<Properties<Types.MutationDubliceerFavorietenlijstArgs>> {
    return z.object({
        id: z.string(),
        input: z.lazy(() => DubliceerFavorietenlijstInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationDubliceerFavorietenlijstArgs>>
}



export function EmailConfirmationMutationInputSchema(): z.ZodObject<Properties<Types.MutationEmailConfirmationArgs>> {
    return z.object({
        confirmation: z.string()
    }) as z.ZodObject<Properties<Types.MutationEmailConfirmationArgs>>
}



export function ForgotPasswordMutationInputSchema(): z.ZodObject<Properties<Types.MutationForgotPasswordArgs>> {
    return z.object({
        email: z.string()
    }) as z.ZodObject<Properties<Types.MutationForgotPasswordArgs>>
}



export function HerbestelWinkelwagenMutationInputSchema(): z.ZodObject<Properties<Types.MutationHerbestelWinkelwagenArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationHerbestelWinkelwagenArgs>>
}



export function LoginMutationInputSchema(): z.ZodObject<Properties<Types.MutationLoginArgs>> {
    return z.object({
        input: z.lazy(() => UsersPermissionsLoginInputSchema())
    }) as z.ZodObject<Properties<Types.MutationLoginArgs>>
}



export function MultipleUploadMutationInputSchema(): z.ZodObject<Properties<Types.MutationMultipleUploadArgs>> {
    return z.object({
        field: z.string().nullish().optional(),
        files: z.array(z.lazy(() => UploadSchema()).nullish().optional()),
        ref: z.string().nullish().optional(),
        refId: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.MutationMultipleUploadArgs>>
}



export function NieuwFacturatieEmailMutationInputSchema(): z.ZodObject<Properties<Types.MutationNieuwFacturatieEmailArgs>> {
    return z.object({
        input: z.lazy(() => NieuwFacturatieEmailInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationNieuwFacturatieEmailArgs>>
}



export function PlaatsBestellingMutationInputSchema(): z.ZodObject<{}> {
    return z.object({
        
    }) as z.ZodObject<{}>
}



export function PlaatsFavorietenlijstInWinkelwagenMutationInputSchema(): z.ZodObject<Properties<Types.MutationPlaatsFavorietenlijstInWinkelwagenArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationPlaatsFavorietenlijstInWinkelwagenArgs>>
}



export function PlaatsInFavorietenlijstMutationInputSchema(): z.ZodObject<Properties<Types.MutationPlaatsInFavorietenlijstArgs>> {
    return z.object({
        id: z.coerce.number(),
        input: z.lazy(() => PlaatsInFavorietenlijstInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationPlaatsInFavorietenlijstArgs>>
}



export function PlaatsProductInWinkelwagenMutationInputSchema(): z.ZodObject<Properties<Types.MutationPlaatsProductInWinkelwagenArgs>> {
    return z.object({
        input: z.lazy(() => PlaatsProductInWinkelwagenInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationPlaatsProductInWinkelwagenArgs>>
}



export function RegisterMutationInputSchema(): z.ZodObject<Properties<Types.MutationRegisterArgs>> {
    return z.object({
        input: z.lazy(() => UsersPermissionsRegisterInputSchema())
    }) as z.ZodObject<Properties<Types.MutationRegisterArgs>>
}



export function RemoveFileMutationInputSchema(): z.ZodObject<Properties<Types.MutationRemoveFileArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationRemoveFileArgs>>
}



export function ResetPasswordMutationInputSchema(): z.ZodObject<Properties<Types.MutationResetPasswordArgs>> {
    return z.object({
        code: z.string(),
        password: z.string(),
        passwordConfirmation: z.string()
    }) as z.ZodObject<Properties<Types.MutationResetPasswordArgs>>
}



export function TestOrderbevestigingMutationInputSchema(): z.ZodObject<Properties<Types.MutationTestOrderbevestigingArgs>> {
    return z.object({
        email: z.string().nullish().optional(),
        id: z.coerce.number()
    }) as z.ZodObject<Properties<Types.MutationTestOrderbevestigingArgs>>
}



export function UpdateAdresMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateAdresArgs>> {
    return z.object({
        data: z.lazy(() => AdresInputSchema()),
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateAdresArgs>>
}



export function UpdateAttribuutMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateAttribuutArgs>> {
    return z.object({
        data: z.lazy(() => AttribuutInputSchema()),
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateAttribuutArgs>>
}



export function UpdateAttribuutWaardeMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateAttribuutWaardeArgs>> {
    return z.object({
        data: z.lazy(() => AttribuutWaardeInputSchema()),
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateAttribuutWaardeArgs>>
}



export function UpdateFavorietenlijstMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateFavorietenlijstArgs>> {
    return z.object({
        id: z.string(),
        input: z.lazy(() => FavorietenlijstInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationUpdateFavorietenlijstArgs>>
}



export function UpdateFileInfoMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateFileInfoArgs>> {
    return z.object({
        id: z.string(),
        info: z.lazy(() => FileInfoInputSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.MutationUpdateFileInfoArgs>>
}



export function UpdateProductInWinkelwagenMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateProductInWinkelwagenArgs>> {
    return z.object({
        input: z.lazy(() => UpdateProductInWinkelwagenInputObjectSchema()),
        productId: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateProductInWinkelwagenArgs>>
}



export function UpdateUploadFileMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateUploadFileArgs>> {
    return z.object({
        data: z.lazy(() => UploadFileInputSchema()),
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateUploadFileArgs>>
}



export function UpdateUploadFolderMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateUploadFolderArgs>> {
    return z.object({
        data: z.lazy(() => UploadFolderInputSchema()),
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateUploadFolderArgs>>
}



export function UpdateUsersPermissionsRoleMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateUsersPermissionsRoleArgs>> {
    return z.object({
        data: z.lazy(() => UsersPermissionsRoleInputSchema()),
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateUsersPermissionsRoleArgs>>
}



export function UpdateUsersPermissionsUserMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateUsersPermissionsUserArgs>> {
    return z.object({
        data: z.lazy(() => UsersPermissionsUserInputSchema()),
        id: z.string()
    }) as z.ZodObject<Properties<Types.MutationUpdateUsersPermissionsUserArgs>>
}



export function UpdateWinkelwagenMutationInputSchema(): z.ZodObject<Properties<Types.MutationUpdateWinkelwagenArgs>> {
    return z.object({
        input: z.lazy(() => BestellingInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationUpdateWinkelwagenArgs>>
}



export function UploadMutationInputSchema(): z.ZodObject<Properties<Types.MutationUploadArgs>> {
    return z.object({
        field: z.string().nullish().optional(),
        file: z.lazy(() => UploadSchema()),
        info: z.lazy(() => FileInfoInputSchema()).nullish().optional(),
        ref: z.string().nullish().optional(),
        refId: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.MutationUploadArgs>>
}



export function VerwijderFacturatieEmailMutationInputSchema(): z.ZodObject<Properties<Types.MutationVerwijderFacturatieEmailArgs>> {
    return z.object({
        input: z.lazy(() => NieuwFacturatieEmailInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationVerwijderFacturatieEmailArgs>>
}



export function VerwijderProductUitWinkelwagenMutationInputSchema(): z.ZodObject<Properties<Types.MutationVerwijderProductUitWinkelwagenArgs>> {
    return z.object({
        productId: z.string()
    }) as z.ZodObject<Properties<Types.MutationVerwijderProductUitWinkelwagenArgs>>
}



export function VerwijderUitFavorietenlijstMutationInputSchema(): z.ZodObject<Properties<Types.MutationVerwijderUitFavorietenlijstArgs>> {
    return z.object({
        id: z.coerce.number(),
        input: z.lazy(() => PlaatsInFavorietenlijstInputObjectSchema())
    }) as z.ZodObject<Properties<Types.MutationVerwijderUitFavorietenlijstArgs>>
}



export function AdresQueryInputSchema(): z.ZodObject<Properties<Types.QueryAdresArgs>> {
    return z.object({
        id: z.string()
    }) as z.ZodObject<Properties<Types.QueryAdresArgs>>
}



export function AdressenQueryInputSchema(): z.ZodObject<Properties<Types.QueryAdressenArgs>> {
    return z.object({
        filters: z.lazy(() => AdresFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryAdressenArgs>>
}



export function AlgemeenQueryInputSchema(): z.ZodObject<{}> {
    return z.object({
        
    }) as z.ZodObject<{}>
}



export function AttributenQueryInputSchema(): z.ZodObject<Properties<Types.QueryAttributenArgs>> {
    return z.object({
        filters: z.lazy(() => AttribuutFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryAttributenArgs>>
}



export function AttribuutQueryInputSchema(): z.ZodObject<Properties<Types.QueryAttribuutArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryAttribuutArgs>>
}



export function AttribuutWaardeQueryInputSchema(): z.ZodObject<Properties<Types.QueryAttribuutWaardeArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryAttribuutWaardeArgs>>
}



export function AttribuutWaardesQueryInputSchema(): z.ZodObject<Properties<Types.QueryAttribuutWaardesArgs>> {
    return z.object({
        filters: z.lazy(() => AttribuutWaardeFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryAttribuutWaardesArgs>>
}



export function BannerQueryInputSchema(): z.ZodObject<Properties<Types.QueryBannerArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryBannerArgs>>
}



export function BannersQueryInputSchema(): z.ZodObject<Properties<Types.QueryBannersArgs>> {
    return z.object({
        filters: z.lazy(() => BannerFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        publicationState: z.lazy(() => PublicationStateSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryBannersArgs>>
}



export function CategorieQueryInputSchema(): z.ZodObject<Properties<Types.QueryCategorieArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryCategorieArgs>>
}



export function CategorieenQueryInputSchema(): z.ZodObject<Properties<Types.QueryCategorieenArgs>> {
    return z.object({
        filters: z.lazy(() => CategorieFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        publicationState: z.lazy(() => PublicationStateSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryCategorieenArgs>>
}



export function DocumentenQueryInputSchema(): z.ZodObject<Properties<Types.QueryDocumentenArgs>> {
    return z.object({
        filters: z.lazy(() => DocumentFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryDocumentenArgs>>
}



export function FavorietenlijstQueryInputSchema(): z.ZodObject<Properties<Types.QueryFavorietenlijstArgs>> {
    return z.object({
        id: z.coerce.number()
    }) as z.ZodObject<Properties<Types.QueryFavorietenlijstArgs>>
}



export function FavorietenlijstByAliasQueryInputSchema(): z.ZodObject<Properties<Types.QueryFavorietenlijstByAliasArgs>> {
    return z.object({
        alias: z.string()
    }) as z.ZodObject<Properties<Types.QueryFavorietenlijstByAliasArgs>>
}



export function FavorietenlijstenQueryInputSchema(): z.ZodObject<Properties<Types.QueryFavorietenlijstenArgs>> {
    return z.object({
        filters: z.lazy(() => FavorietenlijstFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryFavorietenlijstenArgs>>
}



export function FindBestellingQueryInputSchema(): z.ZodObject<Properties<Types.QueryFindBestellingArgs>> {
    return z.object({
        bestelnummer: z.coerce.number()
    }) as z.ZodObject<Properties<Types.QueryFindBestellingArgs>>
}



export function FindBestellingenQueryInputSchema(): z.ZodObject<Properties<Types.QueryFindBestellingenArgs>> {
    return z.object({
        filters: z.lazy(() => BestellingFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryFindBestellingenArgs>>
}



export function I18NLocaleQueryInputSchema(): z.ZodObject<Properties<Types.QueryI18NLocaleArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryI18NLocaleArgs>>
}



export function I18NLocalesQueryInputSchema(): z.ZodObject<Properties<Types.QueryI18NLocalesArgs>> {
    return z.object({
        filters: z.lazy(() => I18NLocaleFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryI18NLocalesArgs>>
}



export function KlantenQueryInputSchema(): z.ZodObject<Properties<Types.QueryKlantenArgs>> {
    return z.object({
        filters: z.lazy(() => KlantFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryKlantenArgs>>
}



export function MeQueryInputSchema(): z.ZodObject<{}> {
    return z.object({
        
    }) as z.ZodObject<{}>
}



export function PaginaQueryInputSchema(): z.ZodObject<Properties<Types.QueryPaginaArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryPaginaArgs>>
}



export function PaginasQueryInputSchema(): z.ZodObject<Properties<Types.QueryPaginasArgs>> {
    return z.object({
        filters: z.lazy(() => PaginaFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        publicationState: z.lazy(() => PublicationStateSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryPaginasArgs>>
}



export function ProductQueryInputSchema(): z.ZodObject<Properties<Types.QueryProductArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryProductArgs>>
}



export function ProductByAliasQueryInputSchema(): z.ZodObject<Properties<Types.QueryProductByAliasArgs>> {
    return z.object({
        alias: z.string()
    }) as z.ZodObject<Properties<Types.QueryProductByAliasArgs>>
}



export function ProductenQueryInputSchema(): z.ZodObject<Properties<Types.QueryProductenArgs>> {
    return z.object({
        filters: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        publicationState: z.lazy(() => PublicationStateSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryProductenArgs>>
}



export function ProductenAdminQueryInputSchema(): z.ZodObject<Properties<Types.QueryProductenAdminArgs>> {
    return z.object({
        filters: z.lazy(() => ProductFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        publicationState: z.lazy(() => PublicationStateSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryProductenAdminArgs>>
}



export function ProductenPerCategorieQueryInputSchema(): z.ZodObject<Properties<Types.QueryProductenPerCategorieArgs>> {
    return z.object({
        categorie: z.string()
    }) as z.ZodObject<Properties<Types.QueryProductenPerCategorieArgs>>
}



export function ProductstatistiekQueryInputSchema(): z.ZodObject<Properties<Types.QueryProductstatistiekArgs>> {
    return z.object({
        input: z.lazy(() => ProductstatistiekInputObjectSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryProductstatistiekArgs>>
}



export function RenderNavigationQueryInputSchema(): z.ZodObject<Properties<Types.QueryRenderNavigationArgs>> {
    return z.object({
        locale: z.lazy(() => I18NLocaleCodeSchema()).nullish().optional(),
        menuOnly: z.coerce.boolean().nullish().optional(),
        navigationIdOrSlug: z.string(),
        path: z.string().nullish().optional(),
        type: z.lazy(() => NavigationRenderTypeSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryRenderNavigationArgs>>
}



export function RenderNavigationChildQueryInputSchema(): z.ZodObject<Properties<Types.QueryRenderNavigationChildArgs>> {
    return z.object({
        childUiKey: z.string(),
        id: z.string(),
        menuOnly: z.coerce.boolean().nullish().optional(),
        type: z.lazy(() => NavigationRenderTypeSchema()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryRenderNavigationChildArgs>>
}



export function TriggerEndpointsQueryInputSchema(): z.ZodObject<Properties<Types.QueryTriggerEndpointsArgs>> {
    return z.object({
        buro26: z.coerce.number(),
        type: z.lazy(() => TriggerEndpointsTypeSchema())
    }) as z.ZodObject<Properties<Types.QueryTriggerEndpointsArgs>>
}



export function UploadFileQueryInputSchema(): z.ZodObject<Properties<Types.QueryUploadFileArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUploadFileArgs>>
}



export function UploadFilesQueryInputSchema(): z.ZodObject<Properties<Types.QueryUploadFilesArgs>> {
    return z.object({
        filters: z.lazy(() => UploadFileFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUploadFilesArgs>>
}



export function UploadFolderQueryInputSchema(): z.ZodObject<Properties<Types.QueryUploadFolderArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUploadFolderArgs>>
}



export function UploadFoldersQueryInputSchema(): z.ZodObject<Properties<Types.QueryUploadFoldersArgs>> {
    return z.object({
        filters: z.lazy(() => UploadFolderFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUploadFoldersArgs>>
}



export function UsersPermissionsRoleQueryInputSchema(): z.ZodObject<Properties<Types.QueryUsersPermissionsRoleArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUsersPermissionsRoleArgs>>
}



export function UsersPermissionsRolesQueryInputSchema(): z.ZodObject<Properties<Types.QueryUsersPermissionsRolesArgs>> {
    return z.object({
        filters: z.lazy(() => UsersPermissionsRoleFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUsersPermissionsRolesArgs>>
}



export function UsersPermissionsUserQueryInputSchema(): z.ZodObject<Properties<Types.QueryUsersPermissionsUserArgs>> {
    return z.object({
        id: z.string().nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUsersPermissionsUserArgs>>
}



export function UsersPermissionsUsersQueryInputSchema(): z.ZodObject<Properties<Types.QueryUsersPermissionsUsersArgs>> {
    return z.object({
        filters: z.lazy(() => UsersPermissionsUserFiltersInputSchema()).nullish().optional(),
        pagination: z.lazy(() => PaginationArgSchema()).nullish().optional(),
        sort: z.array(z.string().nullish().optional()).nullish().optional()
    }) as z.ZodObject<Properties<Types.QueryUsersPermissionsUsersArgs>>
}



export function WinkelwagenQueryInputSchema(): z.ZodObject<{}> {
    return z.object({
        
    }) as z.ZodObject<{}>
}



export function ZoekProductenQueryInputSchema(): z.ZodObject<Properties<Types.QueryZoekProductenArgs>> {
    return z.object({
        zoekterm: z.string()
    }) as z.ZodObject<Properties<Types.QueryZoekProductenArgs>>
}