import 'server-only'
import { createClient } from '@/lib/strapi/generated/client-factory'


export const client = createClient({
    url: process.env.NEXT_PUBLIC_API_GRAPHQL_URL!,
    logging: true
})