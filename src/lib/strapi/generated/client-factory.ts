import {QueryInputArg, QueryResponse, query, mutate, createGraphQlClient, ClientOptions, MutationInputArg, MutationResponse, QueryInputArgDocumentOptional, GeneratedQueryResponse, GeneratedMutationResponse} from 'buro26-graphql-codegen-client'
import {Mutation, Query} from "@/lib/strapi/generated/types";
import {
   QueryAdresArgs,
   QueryAdressenArgs,
   QueryAttributenArgs,
   QueryAttribuutArgs,
   QueryAttribuutWaardeArgs,
   QueryAttribuutWaardesArgs,
   QueryBannerArgs,
   QueryBannersArgs,
   QueryCategorieArgs,
   QueryCategorieenArgs,
   QueryDocumentenArgs,
   QueryFavorietenlijstArgs,
   QueryFavorietenlijstByAliasArgs,
   QueryFavorietenlijstenArgs,
   QueryFindBestellingArgs,
   QueryFindBestellingenArgs,
   QueryI18NLocaleArgs,
   QueryI18NLocalesArgs,
   QueryKlantenArgs,
   QueryPaginaArgs,
   QueryPaginasArgs,
   QueryProductArgs,
   QueryProductByAliasArgs,
   QueryProductenArgs,
   QueryProductenAdminArgs,
   QueryProductenPerCategorieArgs,
   QueryProductstatistiekArgs,
   QueryRenderNavigationArgs,
   QueryRenderNavigationChildArgs,
   QueryTriggerEndpointsArgs,
   QueryUploadFileArgs,
   QueryUploadFilesArgs,
   QueryUploadFolderArgs,
   QueryUploadFoldersArgs,
   QueryUsersPermissionsRoleArgs,
   QueryUsersPermissionsRolesArgs,
   QueryUsersPermissionsUserArgs,
   QueryUsersPermissionsUsersArgs,
   QueryZoekProductenArgs,
   AdresFindManyQueryVariables,
   AdresFindManyQuery,
   AdresFindManyDocument,
   AdresFindQueryVariables,
   AdresFindQuery,
   AdresFindDocument,
   FindNavigationQueryVariables,
   FindNavigationQuery,
   FindNavigationDocument,
   FindNavigationItemByPathQueryVariables,
   FindNavigationItemByPathQuery,
   FindNavigationItemByPathDocument,
   PaginaQueryVariables,
   PaginaQuery,
   PaginaDocument,
   PageByAliasQueryVariables,
   PageByAliasQuery,
   PageByAliasDocument,
   MutationChangePasswordArgs,
   MutationCreateAanmeldingArgs,
   MutationCreateAdresArgs,
   MutationCreateAttribuutArgs,
   MutationCreateAttribuutWaardeArgs,
   MutationCreateFavorietenlijstArgs,
   MutationCreateUploadFileArgs,
   MutationCreateUploadFolderArgs,
   MutationCreateUsersPermissionsRoleArgs,
   MutationCreateUsersPermissionsUserArgs,
   MutationDeleteAdresArgs,
   MutationDeleteAttribuutArgs,
   MutationDeleteAttribuutWaardeArgs,
   MutationDeleteFavorietenlijstArgs,
   MutationDeleteUploadFileArgs,
   MutationDeleteUploadFolderArgs,
   MutationDeleteUsersPermissionsRoleArgs,
   MutationDeleteUsersPermissionsUserArgs,
   MutationDubliceerFavorietenlijstArgs,
   MutationEmailConfirmationArgs,
   MutationForgotPasswordArgs,
   MutationHerbestelWinkelwagenArgs,
   MutationLoginArgs,
   MutationMultipleUploadArgs,
   MutationNieuwFacturatieEmailArgs,
   MutationPlaatsFavorietenlijstInWinkelwagenArgs,
   MutationPlaatsInFavorietenlijstArgs,
   MutationPlaatsProductInWinkelwagenArgs,
   MutationRegisterArgs,
   MutationRemoveFileArgs,
   MutationResetPasswordArgs,
   MutationTestOrderbevestigingArgs,
   MutationUpdateAdresArgs,
   MutationUpdateAttribuutArgs,
   MutationUpdateAttribuutWaardeArgs,
   MutationUpdateFavorietenlijstArgs,
   MutationUpdateFileInfoArgs,
   MutationUpdateProductInWinkelwagenArgs,
   MutationUpdateUploadFileArgs,
   MutationUpdateUploadFolderArgs,
   MutationUpdateUsersPermissionsRoleArgs,
   MutationUpdateUsersPermissionsUserArgs,
   MutationUpdateWinkelwagenArgs,
   MutationUploadArgs,
   MutationVerwijderFacturatieEmailArgs,
   MutationVerwijderProductUitWinkelwagenArgs,
   MutationVerwijderUitFavorietenlijstArgs
} from '@/lib/strapi/generated/types'
import {
   AdresQueryInputSchema,
   AdressenQueryInputSchema,
   AlgemeenQueryInputSchema,
   AttributenQueryInputSchema,
   AttribuutQueryInputSchema,
   AttribuutWaardeQueryInputSchema,
   AttribuutWaardesQueryInputSchema,
   BannerQueryInputSchema,
   BannersQueryInputSchema,
   CategorieQueryInputSchema,
   CategorieenQueryInputSchema,
   DocumentenQueryInputSchema,
   FavorietenlijstQueryInputSchema,
   FavorietenlijstByAliasQueryInputSchema,
   FavorietenlijstenQueryInputSchema,
   FindBestellingQueryInputSchema,
   FindBestellingenQueryInputSchema,
   I18NLocaleQueryInputSchema,
   I18NLocalesQueryInputSchema,
   KlantenQueryInputSchema,
   MeQueryInputSchema,
   PaginaQueryInputSchema,
   PaginasQueryInputSchema,
   ProductQueryInputSchema,
   ProductByAliasQueryInputSchema,
   ProductenQueryInputSchema,
   ProductenAdminQueryInputSchema,
   ProductenPerCategorieQueryInputSchema,
   ProductstatistiekQueryInputSchema,
   RenderNavigationQueryInputSchema,
   RenderNavigationChildQueryInputSchema,
   TriggerEndpointsQueryInputSchema,
   UploadFileQueryInputSchema,
   UploadFilesQueryInputSchema,
   UploadFolderQueryInputSchema,
   UploadFoldersQueryInputSchema,
   UsersPermissionsRoleQueryInputSchema,
   UsersPermissionsRolesQueryInputSchema,
   UsersPermissionsUserQueryInputSchema,
   UsersPermissionsUsersQueryInputSchema,
   WinkelwagenQueryInputSchema,
   ZoekProductenQueryInputSchema,
   AdresFindManySchemaGenerated,
   AdresFindSchemaGenerated,
   FindNavigationSchemaGenerated,
   FindNavigationItemByPathSchemaGenerated,
   PaginaSchemaGenerated,
   PageByAliasSchemaGenerated,
   ChangePasswordMutationInputSchema,
   CreateAanmeldingMutationInputSchema,
   CreateAdresMutationInputSchema,
   CreateAttribuutMutationInputSchema,
   CreateAttribuutWaardeMutationInputSchema,
   CreateFavorietenlijstMutationInputSchema,
   CreateUploadFileMutationInputSchema,
   CreateUploadFolderMutationInputSchema,
   CreateUsersPermissionsRoleMutationInputSchema,
   CreateUsersPermissionsUserMutationInputSchema,
   DeleteAdresMutationInputSchema,
   DeleteAttribuutMutationInputSchema,
   DeleteAttribuutWaardeMutationInputSchema,
   DeleteFavorietenlijstMutationInputSchema,
   DeleteUploadFileMutationInputSchema,
   DeleteUploadFolderMutationInputSchema,
   DeleteUsersPermissionsRoleMutationInputSchema,
   DeleteUsersPermissionsUserMutationInputSchema,
   DubliceerFavorietenlijstMutationInputSchema,
   EmailConfirmationMutationInputSchema,
   ForgotPasswordMutationInputSchema,
   HerbestelWinkelwagenMutationInputSchema,
   LoginMutationInputSchema,
   MultipleUploadMutationInputSchema,
   NieuwFacturatieEmailMutationInputSchema,
   PlaatsBestellingMutationInputSchema,
   PlaatsFavorietenlijstInWinkelwagenMutationInputSchema,
   PlaatsInFavorietenlijstMutationInputSchema,
   PlaatsProductInWinkelwagenMutationInputSchema,
   RegisterMutationInputSchema,
   RemoveFileMutationInputSchema,
   ResetPasswordMutationInputSchema,
   TestOrderbevestigingMutationInputSchema,
   UpdateAdresMutationInputSchema,
   UpdateAttribuutMutationInputSchema,
   UpdateAttribuutWaardeMutationInputSchema,
   UpdateFavorietenlijstMutationInputSchema,
   UpdateFileInfoMutationInputSchema,
   UpdateProductInWinkelwagenMutationInputSchema,
   UpdateUploadFileMutationInputSchema,
   UpdateUploadFolderMutationInputSchema,
   UpdateUsersPermissionsRoleMutationInputSchema,
   UpdateUsersPermissionsUserMutationInputSchema,
   UpdateWinkelwagenMutationInputSchema,
   UploadMutationInputSchema,
   VerwijderFacturatieEmailMutationInputSchema,
   VerwijderProductUitWinkelwagenMutationInputSchema,
   VerwijderUitFavorietenlijstMutationInputSchema
} from '@/lib/strapi/generated/schema'



export const createClient = (options: ClientOptions) => {
    const graphQlClient = createGraphQlClient(options)
    
    return {
        query: {
            adres: {
                schema: AdresQueryInputSchema,
                fetch: (options: QueryInputArg<Query['adres'], QueryAdresArgs>): Promise<QueryResponse<Query['adres']>> => query(graphQlClient, options),
            },
            adressen: {
                schema: AdressenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['adressen'], QueryAdressenArgs>): Promise<QueryResponse<Query['adressen']>> => query(graphQlClient, options),
            },
            algemeen: {
                schema: AlgemeenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['algemeen']>): Promise<QueryResponse<Query['algemeen']>> => query(graphQlClient, options),
            },
            attributen: {
                schema: AttributenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['attributen'], QueryAttributenArgs>): Promise<QueryResponse<Query['attributen']>> => query(graphQlClient, options),
            },
            attribuut: {
                schema: AttribuutQueryInputSchema,
                fetch: (options: QueryInputArg<Query['attribuut'], QueryAttribuutArgs>): Promise<QueryResponse<Query['attribuut']>> => query(graphQlClient, options),
            },
            attribuutWaarde: {
                schema: AttribuutWaardeQueryInputSchema,
                fetch: (options: QueryInputArg<Query['attribuutWaarde'], QueryAttribuutWaardeArgs>): Promise<QueryResponse<Query['attribuutWaarde']>> => query(graphQlClient, options),
            },
            attribuutWaardes: {
                schema: AttribuutWaardesQueryInputSchema,
                fetch: (options: QueryInputArg<Query['attribuutWaardes'], QueryAttribuutWaardesArgs>): Promise<QueryResponse<Query['attribuutWaardes']>> => query(graphQlClient, options),
            },
            banner: {
                schema: BannerQueryInputSchema,
                fetch: (options: QueryInputArg<Query['banner'], QueryBannerArgs>): Promise<QueryResponse<Query['banner']>> => query(graphQlClient, options),
            },
            banners: {
                schema: BannersQueryInputSchema,
                fetch: (options: QueryInputArg<Query['banners'], QueryBannersArgs>): Promise<QueryResponse<Query['banners']>> => query(graphQlClient, options),
            },
            categorie: {
                schema: CategorieQueryInputSchema,
                fetch: (options: QueryInputArg<Query['categorie'], QueryCategorieArgs>): Promise<QueryResponse<Query['categorie']>> => query(graphQlClient, options),
            },
            categorieen: {
                schema: CategorieenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['categorieen'], QueryCategorieenArgs>): Promise<QueryResponse<Query['categorieen']>> => query(graphQlClient, options),
            },
            documenten: {
                schema: DocumentenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['documenten'], QueryDocumentenArgs>): Promise<QueryResponse<Query['documenten']>> => query(graphQlClient, options),
            },
            favorietenlijst: {
                schema: FavorietenlijstQueryInputSchema,
                fetch: (options: QueryInputArg<Query['favorietenlijst'], QueryFavorietenlijstArgs>): Promise<QueryResponse<Query['favorietenlijst']>> => query(graphQlClient, options),
            },
            favorietenlijstByAlias: {
                schema: FavorietenlijstByAliasQueryInputSchema,
                fetch: (options: QueryInputArg<Query['favorietenlijstByAlias'], QueryFavorietenlijstByAliasArgs>): Promise<QueryResponse<Query['favorietenlijstByAlias']>> => query(graphQlClient, options),
            },
            favorietenlijsten: {
                schema: FavorietenlijstenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['favorietenlijsten'], QueryFavorietenlijstenArgs>): Promise<QueryResponse<Query['favorietenlijsten']>> => query(graphQlClient, options),
            },
            findBestelling: {
                schema: FindBestellingQueryInputSchema,
                fetch: (options: QueryInputArg<Query['findBestelling'], QueryFindBestellingArgs>): Promise<QueryResponse<Query['findBestelling']>> => query(graphQlClient, options),
            },
            findBestellingen: {
                schema: FindBestellingenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['findBestellingen'], QueryFindBestellingenArgs>): Promise<QueryResponse<Query['findBestellingen']>> => query(graphQlClient, options),
            },
            i18NLocale: {
                schema: I18NLocaleQueryInputSchema,
                fetch: (options: QueryInputArg<Query['i18NLocale'], QueryI18NLocaleArgs>): Promise<QueryResponse<Query['i18NLocale']>> => query(graphQlClient, options),
            },
            i18NLocales: {
                schema: I18NLocalesQueryInputSchema,
                fetch: (options: QueryInputArg<Query['i18NLocales'], QueryI18NLocalesArgs>): Promise<QueryResponse<Query['i18NLocales']>> => query(graphQlClient, options),
            },
            klanten: {
                schema: KlantenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['klanten'], QueryKlantenArgs>): Promise<QueryResponse<Query['klanten']>> => query(graphQlClient, options),
            },
            me: {
                schema: MeQueryInputSchema,
                fetch: (options: QueryInputArg<Query['me']>): Promise<QueryResponse<Query['me']>> => query(graphQlClient, options),
            },
            pagina: {
                schema: PaginaQueryInputSchema,
                fetch: (options: QueryInputArg<Query['pagina'], QueryPaginaArgs>): Promise<QueryResponse<Query['pagina']>> => query(graphQlClient, options),
            },
            paginas: {
                schema: PaginasQueryInputSchema,
                fetch: (options: QueryInputArg<Query['paginas'], QueryPaginasArgs>): Promise<QueryResponse<Query['paginas']>> => query(graphQlClient, options),
            },
            product: {
                schema: ProductQueryInputSchema,
                fetch: (options: QueryInputArg<Query['product'], QueryProductArgs>): Promise<QueryResponse<Query['product']>> => query(graphQlClient, options),
            },
            productByAlias: {
                schema: ProductByAliasQueryInputSchema,
                fetch: (options: QueryInputArg<Query['productByAlias'], QueryProductByAliasArgs>): Promise<QueryResponse<Query['productByAlias']>> => query(graphQlClient, options),
            },
            producten: {
                schema: ProductenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['producten'], QueryProductenArgs>): Promise<QueryResponse<Query['producten']>> => query(graphQlClient, options),
            },
            productenAdmin: {
                schema: ProductenAdminQueryInputSchema,
                fetch: (options: QueryInputArg<Query['productenAdmin'], QueryProductenAdminArgs>): Promise<QueryResponse<Query['productenAdmin']>> => query(graphQlClient, options),
            },
            productenPerCategorie: {
                schema: ProductenPerCategorieQueryInputSchema,
                fetch: (options: QueryInputArg<Query['productenPerCategorie'], QueryProductenPerCategorieArgs>): Promise<QueryResponse<Query['productenPerCategorie']>> => query(graphQlClient, options),
            },
            productstatistiek: {
                schema: ProductstatistiekQueryInputSchema,
                fetch: (options: QueryInputArg<Query['productstatistiek'], QueryProductstatistiekArgs>): Promise<QueryResponse<Query['productstatistiek']>> => query(graphQlClient, options),
            },
            renderNavigation: {
                schema: RenderNavigationQueryInputSchema,
                fetch: (options: QueryInputArg<Query['renderNavigation'], QueryRenderNavigationArgs>): Promise<QueryResponse<Query['renderNavigation']>> => query(graphQlClient, options),
            },
            renderNavigationChild: {
                schema: RenderNavigationChildQueryInputSchema,
                fetch: (options: QueryInputArg<Query['renderNavigationChild'], QueryRenderNavigationChildArgs>): Promise<QueryResponse<Query['renderNavigationChild']>> => query(graphQlClient, options),
            },
            triggerEndpoints: {
                schema: TriggerEndpointsQueryInputSchema,
                fetch: (options: QueryInputArg<Query['triggerEndpoints'], QueryTriggerEndpointsArgs>): Promise<QueryResponse<Query['triggerEndpoints']>> => query(graphQlClient, options),
            },
            uploadFile: {
                schema: UploadFileQueryInputSchema,
                fetch: (options: QueryInputArg<Query['uploadFile'], QueryUploadFileArgs>): Promise<QueryResponse<Query['uploadFile']>> => query(graphQlClient, options),
            },
            uploadFiles: {
                schema: UploadFilesQueryInputSchema,
                fetch: (options: QueryInputArg<Query['uploadFiles'], QueryUploadFilesArgs>): Promise<QueryResponse<Query['uploadFiles']>> => query(graphQlClient, options),
            },
            uploadFolder: {
                schema: UploadFolderQueryInputSchema,
                fetch: (options: QueryInputArg<Query['uploadFolder'], QueryUploadFolderArgs>): Promise<QueryResponse<Query['uploadFolder']>> => query(graphQlClient, options),
            },
            uploadFolders: {
                schema: UploadFoldersQueryInputSchema,
                fetch: (options: QueryInputArg<Query['uploadFolders'], QueryUploadFoldersArgs>): Promise<QueryResponse<Query['uploadFolders']>> => query(graphQlClient, options),
            },
            usersPermissionsRole: {
                schema: UsersPermissionsRoleQueryInputSchema,
                fetch: (options: QueryInputArg<Query['usersPermissionsRole'], QueryUsersPermissionsRoleArgs>): Promise<QueryResponse<Query['usersPermissionsRole']>> => query(graphQlClient, options),
            },
            usersPermissionsRoles: {
                schema: UsersPermissionsRolesQueryInputSchema,
                fetch: (options: QueryInputArg<Query['usersPermissionsRoles'], QueryUsersPermissionsRolesArgs>): Promise<QueryResponse<Query['usersPermissionsRoles']>> => query(graphQlClient, options),
            },
            usersPermissionsUser: {
                schema: UsersPermissionsUserQueryInputSchema,
                fetch: (options: QueryInputArg<Query['usersPermissionsUser'], QueryUsersPermissionsUserArgs>): Promise<QueryResponse<Query['usersPermissionsUser']>> => query(graphQlClient, options),
            },
            usersPermissionsUsers: {
                schema: UsersPermissionsUsersQueryInputSchema,
                fetch: (options: QueryInputArg<Query['usersPermissionsUsers'], QueryUsersPermissionsUsersArgs>): Promise<QueryResponse<Query['usersPermissionsUsers']>> => query(graphQlClient, options),
            },
            winkelwagen: {
                schema: WinkelwagenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['winkelwagen']>): Promise<QueryResponse<Query['winkelwagen']>> => query(graphQlClient, options),
            },
            zoekProducten: {
                schema: ZoekProductenQueryInputSchema,
                fetch: (options: QueryInputArg<Query['zoekProducten'], QueryZoekProductenArgs>): Promise<QueryResponse<Query['zoekProducten']>> => query(graphQlClient, options),
            },
            generated: {
                adresFindMany: {
                    schema: AdresFindManySchemaGenerated, // @ts-ignore
                    fetch: (options: QueryInputArgDocumentOptional<Query['adresFindMany'], AdresFindManyQueryVariables>): Promise<GeneratedQueryResponse<AdresFindManyQuery>> => query<any>(
                        graphQlClient,
                        {
                            ...options,
                            query: options?.query || AdresFindManyDocument,
                            
                        }
                    ),
                },
                adresFind: {
                    schema: AdresFindSchemaGenerated, // @ts-ignore
                    fetch: (options: QueryInputArgDocumentOptional<Query['adresFind'], AdresFindQueryVariables>): Promise<GeneratedQueryResponse<AdresFindQuery>> => query<any>(
                        graphQlClient,
                        {
                            ...options,
                            query: options?.query || AdresFindDocument,
                            
                        }
                    ),
                },
                findNavigation: {
                    schema: FindNavigationSchemaGenerated, // @ts-ignore
                    fetch: (options: QueryInputArgDocumentOptional<Query['findNavigation'], FindNavigationQueryVariables>): Promise<GeneratedQueryResponse<FindNavigationQuery>> => query<any>(
                        graphQlClient,
                        {
                            ...options,
                            query: options?.query || FindNavigationDocument,
                            
                        }
                    ),
                },
                findNavigationItemByPath: {
                    schema: FindNavigationItemByPathSchemaGenerated, // @ts-ignore
                    fetch: (options: QueryInputArgDocumentOptional<Query['findNavigationItemByPath'], FindNavigationItemByPathQueryVariables>): Promise<GeneratedQueryResponse<FindNavigationItemByPathQuery>> => query<any>(
                        graphQlClient,
                        {
                            ...options,
                            query: options?.query || FindNavigationItemByPathDocument,
                            
                        }
                    ),
                },
                pagina: {
                    schema: PaginaSchemaGenerated, // @ts-ignore
                    fetch: (options: QueryInputArgDocumentOptional<Query['pagina'], PaginaQueryVariables>): Promise<GeneratedQueryResponse<PaginaQuery>> => query<any>(
                        graphQlClient,
                        {
                            ...options,
                            query: options?.query || PaginaDocument,
                            
                        }
                    ),
                },
                pageByAlias: {
                    schema: PageByAliasSchemaGenerated, // @ts-ignore
                    fetch: (options: QueryInputArgDocumentOptional<Query['pageByAlias'], PageByAliasQueryVariables>): Promise<GeneratedQueryResponse<PageByAliasQuery>> => query<any>(
                        graphQlClient,
                        {
                            ...options,
                            query: options?.query || PageByAliasDocument,
                            
                        }
                    ),
                },
            },
        },
        mutate: {
            changePassword: {
                schema: ChangePasswordMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['changePassword'], MutationChangePasswordArgs>): Promise<MutationResponse<Mutation['changePassword']>> => mutate(graphQlClient, options),
            },
            createAanmelding: {
                schema: CreateAanmeldingMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createAanmelding'], MutationCreateAanmeldingArgs>): Promise<MutationResponse<Mutation['createAanmelding']>> => mutate(graphQlClient, options),
            },
            createAdres: {
                schema: CreateAdresMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createAdres'], MutationCreateAdresArgs>): Promise<MutationResponse<Mutation['createAdres']>> => mutate(graphQlClient, options),
            },
            createAttribuut: {
                schema: CreateAttribuutMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createAttribuut'], MutationCreateAttribuutArgs>): Promise<MutationResponse<Mutation['createAttribuut']>> => mutate(graphQlClient, options),
            },
            createAttribuutWaarde: {
                schema: CreateAttribuutWaardeMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createAttribuutWaarde'], MutationCreateAttribuutWaardeArgs>): Promise<MutationResponse<Mutation['createAttribuutWaarde']>> => mutate(graphQlClient, options),
            },
            createFavorietenlijst: {
                schema: CreateFavorietenlijstMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createFavorietenlijst'], MutationCreateFavorietenlijstArgs>): Promise<MutationResponse<Mutation['createFavorietenlijst']>> => mutate(graphQlClient, options),
            },
            createUploadFile: {
                schema: CreateUploadFileMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createUploadFile'], MutationCreateUploadFileArgs>): Promise<MutationResponse<Mutation['createUploadFile']>> => mutate(graphQlClient, options),
            },
            createUploadFolder: {
                schema: CreateUploadFolderMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createUploadFolder'], MutationCreateUploadFolderArgs>): Promise<MutationResponse<Mutation['createUploadFolder']>> => mutate(graphQlClient, options),
            },
            createUsersPermissionsRole: {
                schema: CreateUsersPermissionsRoleMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createUsersPermissionsRole'], MutationCreateUsersPermissionsRoleArgs>): Promise<MutationResponse<Mutation['createUsersPermissionsRole']>> => mutate(graphQlClient, options),
            },
            createUsersPermissionsUser: {
                schema: CreateUsersPermissionsUserMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['createUsersPermissionsUser'], MutationCreateUsersPermissionsUserArgs>): Promise<MutationResponse<Mutation['createUsersPermissionsUser']>> => mutate(graphQlClient, options),
            },
            deleteAdres: {
                schema: DeleteAdresMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteAdres'], MutationDeleteAdresArgs>): Promise<MutationResponse<Mutation['deleteAdres']>> => mutate(graphQlClient, options),
            },
            deleteAttribuut: {
                schema: DeleteAttribuutMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteAttribuut'], MutationDeleteAttribuutArgs>): Promise<MutationResponse<Mutation['deleteAttribuut']>> => mutate(graphQlClient, options),
            },
            deleteAttribuutWaarde: {
                schema: DeleteAttribuutWaardeMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteAttribuutWaarde'], MutationDeleteAttribuutWaardeArgs>): Promise<MutationResponse<Mutation['deleteAttribuutWaarde']>> => mutate(graphQlClient, options),
            },
            deleteFavorietenlijst: {
                schema: DeleteFavorietenlijstMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteFavorietenlijst'], MutationDeleteFavorietenlijstArgs>): Promise<MutationResponse<Mutation['deleteFavorietenlijst']>> => mutate(graphQlClient, options),
            },
            deleteUploadFile: {
                schema: DeleteUploadFileMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteUploadFile'], MutationDeleteUploadFileArgs>): Promise<MutationResponse<Mutation['deleteUploadFile']>> => mutate(graphQlClient, options),
            },
            deleteUploadFolder: {
                schema: DeleteUploadFolderMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteUploadFolder'], MutationDeleteUploadFolderArgs>): Promise<MutationResponse<Mutation['deleteUploadFolder']>> => mutate(graphQlClient, options),
            },
            deleteUsersPermissionsRole: {
                schema: DeleteUsersPermissionsRoleMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteUsersPermissionsRole'], MutationDeleteUsersPermissionsRoleArgs>): Promise<MutationResponse<Mutation['deleteUsersPermissionsRole']>> => mutate(graphQlClient, options),
            },
            deleteUsersPermissionsUser: {
                schema: DeleteUsersPermissionsUserMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['deleteUsersPermissionsUser'], MutationDeleteUsersPermissionsUserArgs>): Promise<MutationResponse<Mutation['deleteUsersPermissionsUser']>> => mutate(graphQlClient, options),
            },
            dubliceerFavorietenlijst: {
                schema: DubliceerFavorietenlijstMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['dubliceerFavorietenlijst'], MutationDubliceerFavorietenlijstArgs>): Promise<MutationResponse<Mutation['dubliceerFavorietenlijst']>> => mutate(graphQlClient, options),
            },
            emailConfirmation: {
                schema: EmailConfirmationMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['emailConfirmation'], MutationEmailConfirmationArgs>): Promise<MutationResponse<Mutation['emailConfirmation']>> => mutate(graphQlClient, options),
            },
            forgotPassword: {
                schema: ForgotPasswordMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['forgotPassword'], MutationForgotPasswordArgs>): Promise<MutationResponse<Mutation['forgotPassword']>> => mutate(graphQlClient, options),
            },
            herbestelWinkelwagen: {
                schema: HerbestelWinkelwagenMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['herbestelWinkelwagen'], MutationHerbestelWinkelwagenArgs>): Promise<MutationResponse<Mutation['herbestelWinkelwagen']>> => mutate(graphQlClient, options),
            },
            login: {
                schema: LoginMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['login'], MutationLoginArgs>): Promise<MutationResponse<Mutation['login']>> => mutate(graphQlClient, options),
            },
            multipleUpload: {
                schema: MultipleUploadMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['multipleUpload'], MutationMultipleUploadArgs>): Promise<MutationResponse<Mutation['multipleUpload']>> => mutate(graphQlClient, options),
            },
            nieuwFacturatieEmail: {
                schema: NieuwFacturatieEmailMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['nieuwFacturatieEmail'], MutationNieuwFacturatieEmailArgs>): Promise<MutationResponse<Mutation['nieuwFacturatieEmail']>> => mutate(graphQlClient, options),
            },
            plaatsBestelling: {
                schema: PlaatsBestellingMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['plaatsBestelling']>): Promise<MutationResponse<Mutation['plaatsBestelling']>> => mutate(graphQlClient, options),
            },
            plaatsFavorietenlijstInWinkelwagen: {
                schema: PlaatsFavorietenlijstInWinkelwagenMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['plaatsFavorietenlijstInWinkelwagen'], MutationPlaatsFavorietenlijstInWinkelwagenArgs>): Promise<MutationResponse<Mutation['plaatsFavorietenlijstInWinkelwagen']>> => mutate(graphQlClient, options),
            },
            plaatsInFavorietenlijst: {
                schema: PlaatsInFavorietenlijstMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['plaatsInFavorietenlijst'], MutationPlaatsInFavorietenlijstArgs>): Promise<MutationResponse<Mutation['plaatsInFavorietenlijst']>> => mutate(graphQlClient, options),
            },
            plaatsProductInWinkelwagen: {
                schema: PlaatsProductInWinkelwagenMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['plaatsProductInWinkelwagen'], MutationPlaatsProductInWinkelwagenArgs>): Promise<MutationResponse<Mutation['plaatsProductInWinkelwagen']>> => mutate(graphQlClient, options),
            },
            register: {
                schema: RegisterMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['register'], MutationRegisterArgs>): Promise<MutationResponse<Mutation['register']>> => mutate(graphQlClient, options),
            },
            removeFile: {
                schema: RemoveFileMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['removeFile'], MutationRemoveFileArgs>): Promise<MutationResponse<Mutation['removeFile']>> => mutate(graphQlClient, options),
            },
            resetPassword: {
                schema: ResetPasswordMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['resetPassword'], MutationResetPasswordArgs>): Promise<MutationResponse<Mutation['resetPassword']>> => mutate(graphQlClient, options),
            },
            testOrderbevestiging: {
                schema: TestOrderbevestigingMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['testOrderbevestiging'], MutationTestOrderbevestigingArgs>): Promise<MutationResponse<Mutation['testOrderbevestiging']>> => mutate(graphQlClient, options),
            },
            updateAdres: {
                schema: UpdateAdresMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateAdres'], MutationUpdateAdresArgs>): Promise<MutationResponse<Mutation['updateAdres']>> => mutate(graphQlClient, options),
            },
            updateAttribuut: {
                schema: UpdateAttribuutMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateAttribuut'], MutationUpdateAttribuutArgs>): Promise<MutationResponse<Mutation['updateAttribuut']>> => mutate(graphQlClient, options),
            },
            updateAttribuutWaarde: {
                schema: UpdateAttribuutWaardeMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateAttribuutWaarde'], MutationUpdateAttribuutWaardeArgs>): Promise<MutationResponse<Mutation['updateAttribuutWaarde']>> => mutate(graphQlClient, options),
            },
            updateFavorietenlijst: {
                schema: UpdateFavorietenlijstMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateFavorietenlijst'], MutationUpdateFavorietenlijstArgs>): Promise<MutationResponse<Mutation['updateFavorietenlijst']>> => mutate(graphQlClient, options),
            },
            updateFileInfo: {
                schema: UpdateFileInfoMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateFileInfo'], MutationUpdateFileInfoArgs>): Promise<MutationResponse<Mutation['updateFileInfo']>> => mutate(graphQlClient, options),
            },
            updateProductInWinkelwagen: {
                schema: UpdateProductInWinkelwagenMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateProductInWinkelwagen'], MutationUpdateProductInWinkelwagenArgs>): Promise<MutationResponse<Mutation['updateProductInWinkelwagen']>> => mutate(graphQlClient, options),
            },
            updateUploadFile: {
                schema: UpdateUploadFileMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateUploadFile'], MutationUpdateUploadFileArgs>): Promise<MutationResponse<Mutation['updateUploadFile']>> => mutate(graphQlClient, options),
            },
            updateUploadFolder: {
                schema: UpdateUploadFolderMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateUploadFolder'], MutationUpdateUploadFolderArgs>): Promise<MutationResponse<Mutation['updateUploadFolder']>> => mutate(graphQlClient, options),
            },
            updateUsersPermissionsRole: {
                schema: UpdateUsersPermissionsRoleMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateUsersPermissionsRole'], MutationUpdateUsersPermissionsRoleArgs>): Promise<MutationResponse<Mutation['updateUsersPermissionsRole']>> => mutate(graphQlClient, options),
            },
            updateUsersPermissionsUser: {
                schema: UpdateUsersPermissionsUserMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateUsersPermissionsUser'], MutationUpdateUsersPermissionsUserArgs>): Promise<MutationResponse<Mutation['updateUsersPermissionsUser']>> => mutate(graphQlClient, options),
            },
            updateWinkelwagen: {
                schema: UpdateWinkelwagenMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['updateWinkelwagen'], MutationUpdateWinkelwagenArgs>): Promise<MutationResponse<Mutation['updateWinkelwagen']>> => mutate(graphQlClient, options),
            },
            upload: {
                schema: UploadMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['upload'], MutationUploadArgs>): Promise<MutationResponse<Mutation['upload']>> => mutate(graphQlClient, options),
            },
            verwijderFacturatieEmail: {
                schema: VerwijderFacturatieEmailMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['verwijderFacturatieEmail'], MutationVerwijderFacturatieEmailArgs>): Promise<MutationResponse<Mutation['verwijderFacturatieEmail']>> => mutate(graphQlClient, options),
            },
            verwijderProductUitWinkelwagen: {
                schema: VerwijderProductUitWinkelwagenMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['verwijderProductUitWinkelwagen'], MutationVerwijderProductUitWinkelwagenArgs>): Promise<MutationResponse<Mutation['verwijderProductUitWinkelwagen']>> => mutate(graphQlClient, options),
            },
            verwijderUitFavorietenlijst: {
                schema: VerwijderUitFavorietenlijstMutationInputSchema,
                fetch: (options: MutationInputArg<Mutation['verwijderUitFavorietenlijst'], MutationVerwijderUitFavorietenlijstArgs>): Promise<MutationResponse<Mutation['verwijderUitFavorietenlijst']>> => mutate(graphQlClient, options),
            },
            generated: {
            
            },
        },
    }
}
    
        