import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  DateTime: { input: any; output: any; }
  I18NLocaleCode: { input: any; output: any; }
  JSON: { input: any; output: any; }
  Long: { input: any; output: any; }
  Upload: { input: any; output: any; }
};

export type Aanmelding = {
  __typename?: 'Aanmelding';
  achternaam: Scalars['String']['output'];
  bedrijfsnaam: Scalars['String']['output'];
  btw_nummer?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  email: Scalars['String']['output'];
  huisnummer: Scalars['String']['output'];
  kvk_nummer?: Maybe<Scalars['String']['output']>;
  plaatsnaam: Scalars['String']['output'];
  postcode: Scalars['String']['output'];
  straatnaam: Scalars['String']['output'];
  telefoon: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  voornaam: Scalars['String']['output'];
};

export type AanmeldingEntity = {
  __typename?: 'AanmeldingEntity';
  attributes?: Maybe<Aanmelding>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type AanmeldingEntityResponse = {
  __typename?: 'AanmeldingEntityResponse';
  data?: Maybe<AanmeldingEntity>;
};

export type AanmeldingInputObject = {
  achternaam: Scalars['String']['input'];
  bedrijfsnaam: Scalars['String']['input'];
  btw_nummer?: InputMaybe<Scalars['String']['input']>;
  email: Scalars['String']['input'];
  huisnummer: Scalars['String']['input'];
  kvk_nummer?: InputMaybe<Scalars['String']['input']>;
  plaatsnaam: Scalars['String']['input'];
  postcode: Scalars['String']['input'];
  straatnaam: Scalars['String']['input'];
  telefoon: Scalars['String']['input'];
  voornaam: Scalars['String']['input'];
};

export type Adres = {
  __typename?: 'Adres';
  achternaam?: Maybe<Scalars['String']['output']>;
  aflever_bestellingen?: Maybe<BestellingRelationResponseCollection>;
  bedrijf: Scalars['String']['output'];
  btw_nummer?: Maybe<Scalars['String']['output']>;
  budget?: Maybe<Scalars['Long']['output']>;
  budgetBeschikbaar: Scalars['Float']['output'];
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  factuur_bestellingen?: Maybe<BestellingRelationResponseCollection>;
  huisnummer: Scalars['String']['output'];
  klant?: Maybe<KlantEntityResponse>;
  land?: Maybe<Enum_Adres_Land>;
  magento_adres_id?: Maybe<Scalars['Int']['output']>;
  magento_customer_id?: Maybe<Scalars['Int']['output']>;
  opmerking?: Maybe<Scalars['String']['output']>;
  optienaam?: Maybe<Scalars['String']['output']>;
  plaats: Scalars['String']['output'];
  postcode: Scalars['String']['output'];
  projectcode?: Maybe<Scalars['String']['output']>;
  provincie?: Maybe<Enum_Adres_Provincie>;
  straatnaam: Scalars['String']['output'];
  telefoon?: Maybe<Scalars['String']['output']>;
  tussenvoegsel?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  voornaam?: Maybe<Scalars['String']['output']>;
};


export type AdresAflever_BestellingenArgs = {
  filters?: InputMaybe<BestellingFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type AdresFactuur_BestellingenArgs = {
  filters?: InputMaybe<BestellingFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type AdresEntity = {
  __typename?: 'AdresEntity';
  attributes?: Maybe<Adres>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type AdresEntityResponse = {
  __typename?: 'AdresEntityResponse';
  data?: Maybe<AdresEntity>;
};

export type AdresEntityResponseCollection = {
  __typename?: 'AdresEntityResponseCollection';
  data: Array<AdresEntity>;
  meta: ResponseCollectionMeta;
};

export type AdresFiltersInput = {
  achternaam?: InputMaybe<StringFilterInput>;
  aflever_bestellingen?: InputMaybe<BestellingFiltersInput>;
  and?: InputMaybe<Array<InputMaybe<AdresFiltersInput>>>;
  bedrijf?: InputMaybe<StringFilterInput>;
  btw_nummer?: InputMaybe<StringFilterInput>;
  budget?: InputMaybe<LongFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  factuur_bestellingen?: InputMaybe<BestellingFiltersInput>;
  huisnummer?: InputMaybe<StringFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  klant?: InputMaybe<KlantFiltersInput>;
  land?: InputMaybe<StringFilterInput>;
  magento_adres_id?: InputMaybe<IntFilterInput>;
  magento_customer_id?: InputMaybe<IntFilterInput>;
  not?: InputMaybe<AdresFiltersInput>;
  opmerking?: InputMaybe<StringFilterInput>;
  optienaam?: InputMaybe<StringFilterInput>;
  or?: InputMaybe<Array<InputMaybe<AdresFiltersInput>>>;
  plaats?: InputMaybe<StringFilterInput>;
  postcode?: InputMaybe<StringFilterInput>;
  projectcode?: InputMaybe<StringFilterInput>;
  provincie?: InputMaybe<StringFilterInput>;
  straatnaam?: InputMaybe<StringFilterInput>;
  telefoon?: InputMaybe<StringFilterInput>;
  tussenvoegsel?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  voornaam?: InputMaybe<StringFilterInput>;
};

export type AdresInput = {
  achternaam?: InputMaybe<Scalars['String']['input']>;
  aflever_bestellingen?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  bedrijf?: InputMaybe<Scalars['String']['input']>;
  btw_nummer?: InputMaybe<Scalars['String']['input']>;
  budget?: InputMaybe<Scalars['Long']['input']>;
  factuur_bestellingen?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  huisnummer?: InputMaybe<Scalars['String']['input']>;
  klant?: InputMaybe<Scalars['ID']['input']>;
  land?: InputMaybe<Enum_Adres_Land>;
  magento_adres_id?: InputMaybe<Scalars['Int']['input']>;
  magento_customer_id?: InputMaybe<Scalars['Int']['input']>;
  opmerking?: InputMaybe<Scalars['String']['input']>;
  optienaam?: InputMaybe<Scalars['String']['input']>;
  plaats?: InputMaybe<Scalars['String']['input']>;
  postcode?: InputMaybe<Scalars['String']['input']>;
  projectcode?: InputMaybe<Scalars['String']['input']>;
  provincie?: InputMaybe<Enum_Adres_Provincie>;
  straatnaam?: InputMaybe<Scalars['String']['input']>;
  telefoon?: InputMaybe<Scalars['String']['input']>;
  tussenvoegsel?: InputMaybe<Scalars['String']['input']>;
  voornaam?: InputMaybe<Scalars['String']['input']>;
};

export type AdresInputObject = {
  achternaam?: InputMaybe<Scalars['String']['input']>;
  bedrijf: Scalars['String']['input'];
  btw_nummer?: InputMaybe<Scalars['String']['input']>;
  budget?: InputMaybe<Scalars['Int']['input']>;
  huisnummer: Scalars['String']['input'];
  land?: InputMaybe<Enum_Adres_Land>;
  opmerking?: InputMaybe<Scalars['String']['input']>;
  plaats: Scalars['String']['input'];
  postcode: Scalars['String']['input'];
  projectcode?: InputMaybe<Scalars['String']['input']>;
  provincie?: InputMaybe<Enum_Adres_Provincie>;
  straatnaam: Scalars['String']['input'];
  telefoon: Scalars['String']['input'];
  tussenvoegsel?: InputMaybe<Scalars['String']['input']>;
  voornaam?: InputMaybe<Scalars['String']['input']>;
};

export type AdresRelationResponseCollection = {
  __typename?: 'AdresRelationResponseCollection';
  data: Array<AdresEntity>;
};

export type Algemeen = {
  __typename?: 'Algemeen';
  adresEnOpeningstijden: Scalars['String']['output'];
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  email: Scalars['String']['output'];
  footer: ComponentMiscFooter;
  privacyEnDisclaimer?: Maybe<PaginaEntityResponse>;
  telefoonnummer: Scalars['String']['output'];
  telefoonnummerLink: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type AlgemeenEntity = {
  __typename?: 'AlgemeenEntity';
  attributes?: Maybe<Algemeen>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type AlgemeenEntityResponse = {
  __typename?: 'AlgemeenEntityResponse';
  data?: Maybe<AlgemeenEntity>;
};

export type Attributen = {
  __typename?: 'Attributen';
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  naam?: Maybe<Scalars['String']['output']>;
  waarden?: Maybe<Array<Maybe<AttribuutData>>>;
  weergave?: Maybe<Enum_Attribuut_Weergave>;
};

export type Attribuut = {
  __typename?: 'Attribuut';
  attribuut_waardes?: Maybe<AttribuutWaardeRelationResponseCollection>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  magento_arttributeid?: Maybe<Scalars['Int']['output']>;
  naam: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  weergave?: Maybe<Enum_Attribuut_Weergave>;
  weergavenaam?: Maybe<Scalars['String']['output']>;
};


export type AttribuutAttribuut_WaardesArgs = {
  filters?: InputMaybe<AttribuutWaardeFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type AttribuutData = {
  __typename?: 'AttribuutData';
  actief?: Maybe<Scalars['Boolean']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  waarde?: Maybe<Scalars['String']['output']>;
};

export type AttribuutEntity = {
  __typename?: 'AttribuutEntity';
  attributes?: Maybe<Attribuut>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type AttribuutEntityResponse = {
  __typename?: 'AttribuutEntityResponse';
  data?: Maybe<AttribuutEntity>;
};

export type AttribuutEntityResponseCollection = {
  __typename?: 'AttribuutEntityResponseCollection';
  data: Array<AttribuutEntity>;
  meta: ResponseCollectionMeta;
};

export type AttribuutFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<AttribuutFiltersInput>>>;
  attribuut_waardes?: InputMaybe<AttribuutWaardeFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  magento_arttributeid?: InputMaybe<IntFilterInput>;
  naam?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<AttribuutFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<AttribuutFiltersInput>>>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  weergave?: InputMaybe<StringFilterInput>;
  weergavenaam?: InputMaybe<StringFilterInput>;
};

export type AttribuutInput = {
  attribuut_waardes?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  magento_arttributeid?: InputMaybe<Scalars['Int']['input']>;
  naam?: InputMaybe<Scalars['String']['input']>;
  weergave?: InputMaybe<Enum_Attribuut_Weergave>;
  weergavenaam?: InputMaybe<Scalars['String']['input']>;
};

export type AttribuutMapping = {
  __typename?: 'AttribuutMapping';
  attributen: Array<Maybe<AttribuutMappingMap>>;
  product: AttribuutMappingProduct;
};

export type AttribuutMappingMap = {
  __typename?: 'AttribuutMappingMap';
  attribuutId: Scalars['ID']['output'];
  attribuutWaardeId: Scalars['ID']['output'];
};

export type AttribuutMappingProduct = {
  __typename?: 'AttribuutMappingProduct';
  id: Scalars['ID']['output'];
  url: Scalars['ID']['output'];
};

export type AttribuutWaarde = {
  __typename?: 'AttribuutWaarde';
  attribuut?: Maybe<AttribuutEntityResponse>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  magento_attributeid?: Maybe<Scalars['Int']['output']>;
  magento_product_ids?: Maybe<Scalars['String']['output']>;
  naam?: Maybe<Scalars['String']['output']>;
  optienaam?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  weergavenaam?: Maybe<Scalars['String']['output']>;
};

export type AttribuutWaardeEntity = {
  __typename?: 'AttribuutWaardeEntity';
  attributes?: Maybe<AttribuutWaarde>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type AttribuutWaardeEntityResponse = {
  __typename?: 'AttribuutWaardeEntityResponse';
  data?: Maybe<AttribuutWaardeEntity>;
};

export type AttribuutWaardeEntityResponseCollection = {
  __typename?: 'AttribuutWaardeEntityResponseCollection';
  data: Array<AttribuutWaardeEntity>;
  meta: ResponseCollectionMeta;
};

export type AttribuutWaardeFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<AttribuutWaardeFiltersInput>>>;
  attribuut?: InputMaybe<AttribuutFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  magento_attributeid?: InputMaybe<IntFilterInput>;
  magento_product_ids?: InputMaybe<StringFilterInput>;
  naam?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<AttribuutWaardeFiltersInput>;
  optienaam?: InputMaybe<StringFilterInput>;
  or?: InputMaybe<Array<InputMaybe<AttribuutWaardeFiltersInput>>>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  weergavenaam?: InputMaybe<StringFilterInput>;
};

export type AttribuutWaardeInput = {
  attribuut?: InputMaybe<Scalars['ID']['input']>;
  magento_attributeid?: InputMaybe<Scalars['Int']['input']>;
  magento_product_ids?: InputMaybe<Scalars['String']['input']>;
  naam?: InputMaybe<Scalars['String']['input']>;
  optienaam?: InputMaybe<Scalars['String']['input']>;
  weergavenaam?: InputMaybe<Scalars['String']['input']>;
};

export type AttribuutWaardeRelationResponseCollection = {
  __typename?: 'AttribuutWaardeRelationResponseCollection';
  data: Array<AttribuutWaardeEntity>;
};

export type Banner = {
  __typename?: 'Banner';
  afbeelding: UploadFileEntityResponse;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  inhoud?: Maybe<Scalars['String']['output']>;
  kleur?: Maybe<Enum_Banner_Kleur>;
  product_prijs?: Maybe<Scalars['Float']['output']>;
  product_prijs_aanbieding?: Maybe<Scalars['Float']['output']>;
  publishedAt?: Maybe<Scalars['DateTime']['output']>;
  titel: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  url?: Maybe<Scalars['String']['output']>;
};

export type BannerEntity = {
  __typename?: 'BannerEntity';
  attributes?: Maybe<Banner>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type BannerEntityResponse = {
  __typename?: 'BannerEntityResponse';
  data?: Maybe<BannerEntity>;
};

export type BannerEntityResponseCollection = {
  __typename?: 'BannerEntityResponseCollection';
  data: Array<BannerEntity>;
  meta: ResponseCollectionMeta;
};

export type BannerFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<BannerFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  inhoud?: InputMaybe<StringFilterInput>;
  kleur?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<BannerFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<BannerFiltersInput>>>;
  product_prijs?: InputMaybe<FloatFilterInput>;
  product_prijs_aanbieding?: InputMaybe<FloatFilterInput>;
  publishedAt?: InputMaybe<DateTimeFilterInput>;
  titel?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  url?: InputMaybe<StringFilterInput>;
};

export type BannerRelationResponseCollection = {
  __typename?: 'BannerRelationResponseCollection';
  data: Array<BannerEntity>;
};

export type Bestelling = {
  __typename?: 'Bestelling';
  afleveradres?: Maybe<AdresEntityResponse>;
  bestelling_status?: Maybe<Enum_Bestelling_Bestelling_Status>;
  bestelling_type?: Maybe<Enum_Bestelling_Bestelling_Type>;
  bestelnummer?: Maybe<Scalars['Int']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  datum_geplaatst?: Maybe<Scalars['DateTime']['output']>;
  emails?: Maybe<Array<Maybe<ComponentMiscFacturatie>>>;
  factuuradres?: Maybe<AdresEntityResponse>;
  gebruiker?: Maybe<UsersPermissionsUserEntityResponse>;
  klant?: Maybe<KlantEntityResponse>;
  opmerking?: Maybe<Scalars['String']['output']>;
  producten?: Maybe<Array<Maybe<ComponentBestellingProducten>>>;
  resterendBudgetGekozenAdres: Scalars['Float']['output'];
  snapshot?: Maybe<Scalars['JSON']['output']>;
  subtotaal?: Maybe<Scalars['Float']['output']>;
  totaal?: Maybe<Scalars['Float']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  verzendkosten?: Maybe<Scalars['Float']['output']>;
};


export type BestellingEmailsArgs = {
  filters?: InputMaybe<ComponentMiscFacturatieFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type BestellingProductenArgs = {
  filters?: InputMaybe<ComponentBestellingProductenFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type BestellingEntity = {
  __typename?: 'BestellingEntity';
  attributes?: Maybe<Bestelling>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type BestellingEntityResponse = {
  __typename?: 'BestellingEntityResponse';
  data?: Maybe<BestellingEntity>;
};

export type BestellingEntityResponseCollection = {
  __typename?: 'BestellingEntityResponseCollection';
  data: Array<BestellingEntity>;
  meta: ResponseCollectionMeta;
};

export type BestellingFiltersInput = {
  afleveradres?: InputMaybe<AdresFiltersInput>;
  and?: InputMaybe<Array<InputMaybe<BestellingFiltersInput>>>;
  bestelling_status?: InputMaybe<StringFilterInput>;
  bestelling_type?: InputMaybe<StringFilterInput>;
  bestelnummer?: InputMaybe<IntFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  datum_geplaatst?: InputMaybe<DateTimeFilterInput>;
  emails?: InputMaybe<ComponentMiscFacturatieFiltersInput>;
  factuuradres?: InputMaybe<AdresFiltersInput>;
  gebruiker?: InputMaybe<UsersPermissionsUserFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  klant?: InputMaybe<KlantFiltersInput>;
  not?: InputMaybe<BestellingFiltersInput>;
  opmerking?: InputMaybe<StringFilterInput>;
  or?: InputMaybe<Array<InputMaybe<BestellingFiltersInput>>>;
  producten?: InputMaybe<ComponentBestellingProductenFiltersInput>;
  snapshot?: InputMaybe<JsonFilterInput>;
  subtotaal?: InputMaybe<FloatFilterInput>;
  totaal?: InputMaybe<FloatFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  verzendkosten?: InputMaybe<FloatFilterInput>;
};

export type BestellingInputObject = {
  afleveradres?: InputMaybe<Scalars['Int']['input']>;
  bestelling_type?: InputMaybe<Enum_Bestelling_Bestelling_Type>;
  emails?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  opmerking?: InputMaybe<Scalars['String']['input']>;
};

export type BestellingRelationResponseCollection = {
  __typename?: 'BestellingRelationResponseCollection';
  data: Array<BestellingEntity>;
};

export enum BestellingType {
  Artiverkoopartikelen = 'Artiverkoopartikelen',
  Gebruiksartikelen = 'Gebruiksartikelen'
}

export type BooleanFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['Boolean']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['Boolean']['input']>>>;
  contains?: InputMaybe<Scalars['Boolean']['input']>;
  containsi?: InputMaybe<Scalars['Boolean']['input']>;
  endsWith?: InputMaybe<Scalars['Boolean']['input']>;
  eq?: InputMaybe<Scalars['Boolean']['input']>;
  eqi?: InputMaybe<Scalars['Boolean']['input']>;
  gt?: InputMaybe<Scalars['Boolean']['input']>;
  gte?: InputMaybe<Scalars['Boolean']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Boolean']['input']>>>;
  lt?: InputMaybe<Scalars['Boolean']['input']>;
  lte?: InputMaybe<Scalars['Boolean']['input']>;
  ne?: InputMaybe<Scalars['Boolean']['input']>;
  nei?: InputMaybe<Scalars['Boolean']['input']>;
  not?: InputMaybe<BooleanFilterInput>;
  notContains?: InputMaybe<Scalars['Boolean']['input']>;
  notContainsi?: InputMaybe<Scalars['Boolean']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Boolean']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['Boolean']['input']>>>;
  startsWith?: InputMaybe<Scalars['Boolean']['input']>;
};

export type Categorie = {
  __typename?: 'Categorie';
  afbeelding?: Maybe<UploadFileRelationResponseCollection>;
  alias: Scalars['String']['output'];
  beschrijving?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  hoofdcategorie?: Maybe<CategorieEntityResponse>;
  klant?: Maybe<KlantEntityResponse>;
  magento_id?: Maybe<Scalars['Int']['output']>;
  magento_parent_id?: Maybe<Scalars['Int']['output']>;
  naam: Scalars['String']['output'];
  pad?: Maybe<Scalars['String']['output']>;
  producten?: Maybe<ProductRelationResponseCollection>;
  publishedAt?: Maybe<Scalars['DateTime']['output']>;
  seo?: Maybe<ComponentSharedSeo>;
  subcategorie?: Maybe<CategorieRelationResponseCollection>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type CategorieAfbeeldingArgs = {
  filters?: InputMaybe<UploadFileFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type CategorieProductenArgs = {
  filters?: InputMaybe<ProductFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type CategorieSubcategorieArgs = {
  filters?: InputMaybe<CategorieFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type CategorieEntity = {
  __typename?: 'CategorieEntity';
  attributes?: Maybe<Categorie>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type CategorieEntityResponse = {
  __typename?: 'CategorieEntityResponse';
  data?: Maybe<CategorieEntity>;
};

export type CategorieEntityResponseCollection = {
  __typename?: 'CategorieEntityResponseCollection';
  data: Array<CategorieEntity>;
  meta: ResponseCollectionMeta;
};

export type CategorieFiltersInput = {
  alias?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<CategorieFiltersInput>>>;
  beschrijving?: InputMaybe<StringFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  hoofdcategorie?: InputMaybe<CategorieFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  klant?: InputMaybe<KlantFiltersInput>;
  magento_id?: InputMaybe<IntFilterInput>;
  magento_parent_id?: InputMaybe<IntFilterInput>;
  naam?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<CategorieFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<CategorieFiltersInput>>>;
  pad?: InputMaybe<StringFilterInput>;
  producten?: InputMaybe<ProductFiltersInput>;
  publishedAt?: InputMaybe<DateTimeFilterInput>;
  seo?: InputMaybe<ComponentSharedSeoFiltersInput>;
  subcategorie?: InputMaybe<CategorieFiltersInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type CategorieRelationResponseCollection = {
  __typename?: 'CategorieRelationResponseCollection';
  data: Array<CategorieEntity>;
};

export type ComponentBestellingProducten = {
  __typename?: 'ComponentBestellingProducten';
  aantal: Scalars['Int']['output'];
  id: Scalars['ID']['output'];
  product?: Maybe<ProductEntityResponse>;
};

export type ComponentBestellingProductenFiltersInput = {
  aantal?: InputMaybe<IntFilterInput>;
  and?: InputMaybe<Array<InputMaybe<ComponentBestellingProductenFiltersInput>>>;
  not?: InputMaybe<ComponentBestellingProductenFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentBestellingProductenFiltersInput>>>;
  product?: InputMaybe<ProductFiltersInput>;
};

export type ComponentInhoudAfbeelding = {
  __typename?: 'ComponentInhoudAfbeelding';
  afbeelding: UploadFileEntityResponse;
  id: Scalars['ID']['output'];
};

export type ComponentInhoudBanners = {
  __typename?: 'ComponentInhoudBanners';
  banners?: Maybe<BannerRelationResponseCollection>;
  id: Scalars['ID']['output'];
};


export type ComponentInhoudBannersBannersArgs = {
  filters?: InputMaybe<BannerFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type ComponentInhoudButton = {
  __typename?: 'ComponentInhoudButton';
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  url?: Maybe<Scalars['String']['output']>;
};

export type ComponentInhoudCategorieNavigatie = {
  __typename?: 'ComponentInhoudCategorieNavigatie';
  categorie?: Maybe<CategorieEntityResponse>;
  cats?: Maybe<CategorieRelationResponseCollection>;
  id: Scalars['ID']['output'];
};


export type ComponentInhoudCategorieNavigatieCatsArgs = {
  filters?: InputMaybe<CategorieFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type ComponentInhoudContent = {
  __typename?: 'ComponentInhoudContent';
  afbeelding: UploadFileEntityResponse;
  button?: Maybe<ComponentMiscButton>;
  id: Scalars['ID']['output'];
  inhoud: Scalars['String']['output'];
};

export type ComponentInhoudHero = {
  __typename?: 'ComponentInhoudHero';
  afbeelding: UploadFileEntityResponse;
  button?: Maybe<ComponentMiscButton>;
  id: Scalars['ID']['output'];
  tekst: Scalars['String']['output'];
  titel: Scalars['String']['output'];
};

export type ComponentInhoudInhoud = {
  __typename?: 'ComponentInhoudInhoud';
  id: Scalars['ID']['output'];
  tekst: Scalars['String']['output'];
};

export type ComponentInhoudUitgelichteProducten = {
  __typename?: 'ComponentInhoudUitgelichteProducten';
  buttonTekst: Scalars['String']['output'];
  categorie?: Maybe<CategorieEntityResponse>;
  id: Scalars['ID']['output'];
  titel: Scalars['String']['output'];
};

export type ComponentInhoudVideo = {
  __typename?: 'ComponentInhoudVideo';
  alt_text?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  youtube_url?: Maybe<Scalars['String']['output']>;
};

export type ComponentMiscButton = {
  __typename?: 'ComponentMiscButton';
  id: Scalars['ID']['output'];
  label: Scalars['String']['output'];
  url: Scalars['String']['output'];
};

export type ComponentMiscFacturatie = {
  __typename?: 'ComponentMiscFacturatie';
  email: Scalars['String']['output'];
  id: Scalars['ID']['output'];
};

export type ComponentMiscFacturatieFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentMiscFacturatieFiltersInput>>>;
  email?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<ComponentMiscFacturatieFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentMiscFacturatieFiltersInput>>>;
};

export type ComponentMiscFacturatieInput = {
  email?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type ComponentMiscFooter = {
  __typename?: 'ComponentMiscFooter';
  callToAction: Scalars['String']['output'];
  id: Scalars['ID']['output'];
};

export type ComponentProductAttributen = {
  __typename?: 'ComponentProductAttributen';
  attribuut_waarde?: Maybe<AttribuutWaardeEntityResponse>;
  id: Scalars['ID']['output'];
};

export type ComponentProductAttributenFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentProductAttributenFiltersInput>>>;
  attribuut_waarde?: InputMaybe<AttribuutWaardeFiltersInput>;
  not?: InputMaybe<ComponentProductAttributenFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentProductAttributenFiltersInput>>>;
};

export type ComponentSharedMetaSocial = {
  __typename?: 'ComponentSharedMetaSocial';
  description: Scalars['String']['output'];
  id: Scalars['ID']['output'];
  image?: Maybe<UploadFileEntityResponse>;
  socialNetwork: Enum_Componentsharedmetasocial_Socialnetwork;
  title: Scalars['String']['output'];
};

export type ComponentSharedMetaSocialFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentSharedMetaSocialFiltersInput>>>;
  description?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<ComponentSharedMetaSocialFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentSharedMetaSocialFiltersInput>>>;
  socialNetwork?: InputMaybe<StringFilterInput>;
  title?: InputMaybe<StringFilterInput>;
};

export type ComponentSharedSeo = {
  __typename?: 'ComponentSharedSeo';
  canonicalURL?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  keywords?: Maybe<Scalars['String']['output']>;
  metaDescription?: Maybe<Scalars['String']['output']>;
  metaImage?: Maybe<UploadFileEntityResponse>;
  metaRobots?: Maybe<Scalars['String']['output']>;
  metaSocial?: Maybe<Array<Maybe<ComponentSharedMetaSocial>>>;
  metaTitle?: Maybe<Scalars['String']['output']>;
  metaViewport?: Maybe<Scalars['String']['output']>;
  structuredData?: Maybe<Scalars['JSON']['output']>;
};


export type ComponentSharedSeoMetaSocialArgs = {
  filters?: InputMaybe<ComponentSharedMetaSocialFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type ComponentSharedSeoFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<ComponentSharedSeoFiltersInput>>>;
  canonicalURL?: InputMaybe<StringFilterInput>;
  keywords?: InputMaybe<StringFilterInput>;
  metaDescription?: InputMaybe<StringFilterInput>;
  metaRobots?: InputMaybe<StringFilterInput>;
  metaSocial?: InputMaybe<ComponentSharedMetaSocialFiltersInput>;
  metaTitle?: InputMaybe<StringFilterInput>;
  metaViewport?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<ComponentSharedSeoFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ComponentSharedSeoFiltersInput>>>;
  structuredData?: InputMaybe<JsonFilterInput>;
};

export type ComponentSharedTekstVeld = {
  __typename?: 'ComponentSharedTekstVeld';
  id: Scalars['ID']['output'];
  tekst?: Maybe<Scalars['JSON']['output']>;
};

export type DateTimeFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  contains?: InputMaybe<Scalars['DateTime']['input']>;
  containsi?: InputMaybe<Scalars['DateTime']['input']>;
  endsWith?: InputMaybe<Scalars['DateTime']['input']>;
  eq?: InputMaybe<Scalars['DateTime']['input']>;
  eqi?: InputMaybe<Scalars['DateTime']['input']>;
  gt?: InputMaybe<Scalars['DateTime']['input']>;
  gte?: InputMaybe<Scalars['DateTime']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  lt?: InputMaybe<Scalars['DateTime']['input']>;
  lte?: InputMaybe<Scalars['DateTime']['input']>;
  ne?: InputMaybe<Scalars['DateTime']['input']>;
  nei?: InputMaybe<Scalars['DateTime']['input']>;
  not?: InputMaybe<DateTimeFilterInput>;
  notContains?: InputMaybe<Scalars['DateTime']['input']>;
  notContainsi?: InputMaybe<Scalars['DateTime']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  startsWith?: InputMaybe<Scalars['DateTime']['input']>;
};

export type Document = {
  __typename?: 'Document';
  bestand?: Maybe<UploadFileEntityResponse>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  magento_docid?: Maybe<Scalars['Int']['output']>;
  naam: Scalars['String']['output'];
  products?: Maybe<ProductRelationResponseCollection>;
  publiek?: Maybe<Scalars['Boolean']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type DocumentProductsArgs = {
  filters?: InputMaybe<ProductFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type DocumentEntity = {
  __typename?: 'DocumentEntity';
  attributes?: Maybe<Document>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type DocumentEntityResponseCollection = {
  __typename?: 'DocumentEntityResponseCollection';
  data: Array<DocumentEntity>;
  meta: ResponseCollectionMeta;
};

export type DocumentFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<DocumentFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  magento_docid?: InputMaybe<IntFilterInput>;
  naam?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<DocumentFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<DocumentFiltersInput>>>;
  products?: InputMaybe<ProductFiltersInput>;
  publiek?: InputMaybe<BooleanFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type DocumentRelationResponseCollection = {
  __typename?: 'DocumentRelationResponseCollection';
  data: Array<DocumentEntity>;
};

export type DubliceerFavorietenlijstInputObject = {
  naam: Scalars['String']['input'];
};

export enum Enum_Adres_Land {
  Belgie = 'belgie',
  Duitsland = 'duitsland',
  Nederland = 'nederland'
}

export enum Enum_Adres_Provincie {
  Drenthe = 'drenthe',
  Flevoland = 'flevoland',
  Friesland = 'friesland',
  Gelderland = 'gelderland',
  Groningen = 'groningen',
  Limburg = 'limburg',
  NoordBrabant = 'noord_brabant',
  NoordHolland = 'noord_holland',
  Overijsel = 'overijsel',
  Utrecht = 'utrecht',
  Zeeland = 'zeeland',
  ZuidHolland = 'zuid_holland'
}

export enum Enum_Attribuut_Weergave {
  Button = 'button',
  Dropdown = 'dropdown'
}

export enum Enum_Banner_Kleur {
  Blauw = 'blauw',
  Roze = 'roze'
}

export enum Enum_Bestelling_Bestelling_Status {
  Besteld = 'besteld',
  Open = 'open'
}

export enum Enum_Bestelling_Bestelling_Type {
  Artiverkoopartikelen = 'artiverkoopartikelen',
  Gebruiksartikelen = 'gebruiksartikelen'
}

export enum Enum_Componentsharedmetasocial_Socialnetwork {
  Facebook = 'Facebook',
  Twitter = 'Twitter'
}

export enum Enum_Email_Type {
  AanmeldingAdmin = 'aanmelding_admin',
  OrderBevestiging = 'order_bevestiging'
}

export enum Enum_Product_Product_Type {
  Configurabel = 'configurabel',
  Enkel = 'enkel'
}

export enum Enum_Product_Zichtbaar {
  Catalogus = 'catalogus',
  CatalogusZoeken = 'catalogus_zoeken',
  NietIndividueel = 'niet_individueel',
  Zoeken = 'zoeken'
}

export type Email = {
  __typename?: 'Email';
  beschikbareVariabelen?: Maybe<Scalars['String']['output']>;
  body: Scalars['String']['output'];
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  onderwerp: Scalars['String']['output'];
  type?: Maybe<Enum_Email_Type>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type EmailTemplate = {
  __typename?: 'EmailTemplate';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  logo?: Maybe<UploadFileEntityResponse>;
  template: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type Error = {
  __typename?: 'Error';
  code: Scalars['String']['output'];
  message?: Maybe<Scalars['String']['output']>;
};

export type Favorietenlijst = {
  __typename?: 'Favorietenlijst';
  alias?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  gebruiker?: Maybe<UsersPermissionsUserEntityResponse>;
  magento_customer_id?: Maybe<Scalars['Int']['output']>;
  magento_product_ids?: Maybe<Scalars['String']['output']>;
  naam: Scalars['String']['output'];
  producten?: Maybe<ProductRelationResponseCollection>;
  totaal: Scalars['Float']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  uuid?: Maybe<Scalars['String']['output']>;
};


export type FavorietenlijstProductenArgs = {
  filters?: InputMaybe<ProductFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type FavorietenlijstEntity = {
  __typename?: 'FavorietenlijstEntity';
  attributes?: Maybe<Favorietenlijst>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type FavorietenlijstEntityResponse = {
  __typename?: 'FavorietenlijstEntityResponse';
  data?: Maybe<FavorietenlijstEntity>;
};

export type FavorietenlijstEntityResponseCollection = {
  __typename?: 'FavorietenlijstEntityResponseCollection';
  data: Array<FavorietenlijstEntity>;
  meta: ResponseCollectionMeta;
};

export type FavorietenlijstFiltersInput = {
  alias?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<FavorietenlijstFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  gebruiker?: InputMaybe<UsersPermissionsUserFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  magento_customer_id?: InputMaybe<IntFilterInput>;
  magento_product_ids?: InputMaybe<StringFilterInput>;
  naam?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<FavorietenlijstFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<FavorietenlijstFiltersInput>>>;
  producten?: InputMaybe<ProductFiltersInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  uuid?: InputMaybe<StringFilterInput>;
};

export type FavorietenlijstInputObject = {
  naam: Scalars['String']['input'];
  producten?: InputMaybe<Array<Scalars['Int']['input']>>;
};

export type FavorietenlijstRelationResponseCollection = {
  __typename?: 'FavorietenlijstRelationResponseCollection';
  data: Array<FavorietenlijstEntity>;
};

export type FileInfoInput = {
  alternativeText?: InputMaybe<Scalars['String']['input']>;
  caption?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type FloatFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  contains?: InputMaybe<Scalars['Float']['input']>;
  containsi?: InputMaybe<Scalars['Float']['input']>;
  endsWith?: InputMaybe<Scalars['Float']['input']>;
  eq?: InputMaybe<Scalars['Float']['input']>;
  eqi?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  ne?: InputMaybe<Scalars['Float']['input']>;
  nei?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<FloatFilterInput>;
  notContains?: InputMaybe<Scalars['Float']['input']>;
  notContainsi?: InputMaybe<Scalars['Float']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  startsWith?: InputMaybe<Scalars['Float']['input']>;
};

export type GenericMorph = Aanmelding | Adres | Algemeen | Attribuut | AttribuutWaarde | Banner | Bestelling | Categorie | ComponentBestellingProducten | ComponentInhoudAfbeelding | ComponentInhoudBanners | ComponentInhoudButton | ComponentInhoudCategorieNavigatie | ComponentInhoudContent | ComponentInhoudHero | ComponentInhoudInhoud | ComponentInhoudUitgelichteProducten | ComponentInhoudVideo | ComponentMiscButton | ComponentMiscFacturatie | ComponentMiscFooter | ComponentProductAttributen | ComponentSharedMetaSocial | ComponentSharedSeo | ComponentSharedTekstVeld | Document | Email | EmailTemplate | Favorietenlijst | I18NLocale | Klant | Pagina | Product | UploadFile | UploadFolder | UsersPermissionsPermission | UsersPermissionsRole | UsersPermissionsUser;

export type I18NLocale = {
  __typename?: 'I18NLocale';
  code?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type I18NLocaleEntity = {
  __typename?: 'I18NLocaleEntity';
  attributes?: Maybe<I18NLocale>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type I18NLocaleEntityResponse = {
  __typename?: 'I18NLocaleEntityResponse';
  data?: Maybe<I18NLocaleEntity>;
};

export type I18NLocaleEntityResponseCollection = {
  __typename?: 'I18NLocaleEntityResponseCollection';
  data: Array<I18NLocaleEntity>;
  meta: ResponseCollectionMeta;
};

export type I18NLocaleFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<I18NLocaleFiltersInput>>>;
  code?: InputMaybe<StringFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<I18NLocaleFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<I18NLocaleFiltersInput>>>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type IdFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  contains?: InputMaybe<Scalars['ID']['input']>;
  containsi?: InputMaybe<Scalars['ID']['input']>;
  endsWith?: InputMaybe<Scalars['ID']['input']>;
  eq?: InputMaybe<Scalars['ID']['input']>;
  eqi?: InputMaybe<Scalars['ID']['input']>;
  gt?: InputMaybe<Scalars['ID']['input']>;
  gte?: InputMaybe<Scalars['ID']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  lt?: InputMaybe<Scalars['ID']['input']>;
  lte?: InputMaybe<Scalars['ID']['input']>;
  ne?: InputMaybe<Scalars['ID']['input']>;
  nei?: InputMaybe<Scalars['ID']['input']>;
  not?: InputMaybe<IdFilterInput>;
  notContains?: InputMaybe<Scalars['ID']['input']>;
  notContainsi?: InputMaybe<Scalars['ID']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  startsWith?: InputMaybe<Scalars['ID']['input']>;
};

export type IntFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  contains?: InputMaybe<Scalars['Int']['input']>;
  containsi?: InputMaybe<Scalars['Int']['input']>;
  endsWith?: InputMaybe<Scalars['Int']['input']>;
  eq?: InputMaybe<Scalars['Int']['input']>;
  eqi?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  ne?: InputMaybe<Scalars['Int']['input']>;
  nei?: InputMaybe<Scalars['Int']['input']>;
  not?: InputMaybe<IntFilterInput>;
  notContains?: InputMaybe<Scalars['Int']['input']>;
  notContainsi?: InputMaybe<Scalars['Int']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  startsWith?: InputMaybe<Scalars['Int']['input']>;
};

export type JsonFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['JSON']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['JSON']['input']>>>;
  contains?: InputMaybe<Scalars['JSON']['input']>;
  containsi?: InputMaybe<Scalars['JSON']['input']>;
  endsWith?: InputMaybe<Scalars['JSON']['input']>;
  eq?: InputMaybe<Scalars['JSON']['input']>;
  eqi?: InputMaybe<Scalars['JSON']['input']>;
  gt?: InputMaybe<Scalars['JSON']['input']>;
  gte?: InputMaybe<Scalars['JSON']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['JSON']['input']>>>;
  lt?: InputMaybe<Scalars['JSON']['input']>;
  lte?: InputMaybe<Scalars['JSON']['input']>;
  ne?: InputMaybe<Scalars['JSON']['input']>;
  nei?: InputMaybe<Scalars['JSON']['input']>;
  not?: InputMaybe<JsonFilterInput>;
  notContains?: InputMaybe<Scalars['JSON']['input']>;
  notContainsi?: InputMaybe<Scalars['JSON']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['JSON']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['JSON']['input']>>>;
  startsWith?: InputMaybe<Scalars['JSON']['input']>;
};

export type Klant = {
  __typename?: 'Klant';
  adres?: Maybe<AdresRelationResponseCollection>;
  bestellingen?: Maybe<BestellingRelationResponseCollection>;
  categorieen?: Maybe<CategorieRelationResponseCollection>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  factuurEmail?: Maybe<Scalars['String']['output']>;
  factuuradres?: Maybe<AdresEntityResponse>;
  gebruiker?: Maybe<UsersPermissionsUserRelationResponseCollection>;
  magento_customer_ids?: Maybe<Scalars['String']['output']>;
  naam: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type KlantAdresArgs = {
  filters?: InputMaybe<AdresFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type KlantBestellingenArgs = {
  filters?: InputMaybe<BestellingFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type KlantCategorieenArgs = {
  filters?: InputMaybe<CategorieFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type KlantGebruikerArgs = {
  filters?: InputMaybe<UsersPermissionsUserFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type KlantEntity = {
  __typename?: 'KlantEntity';
  attributes?: Maybe<Klant>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type KlantEntityResponse = {
  __typename?: 'KlantEntityResponse';
  data?: Maybe<KlantEntity>;
};

export type KlantEntityResponseCollection = {
  __typename?: 'KlantEntityResponseCollection';
  data: Array<KlantEntity>;
  meta: ResponseCollectionMeta;
};

export type KlantFiltersInput = {
  adres?: InputMaybe<AdresFiltersInput>;
  and?: InputMaybe<Array<InputMaybe<KlantFiltersInput>>>;
  bestellingen?: InputMaybe<BestellingFiltersInput>;
  categorieen?: InputMaybe<CategorieFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  factuurEmail?: InputMaybe<StringFilterInput>;
  factuuradres?: InputMaybe<AdresFiltersInput>;
  gebruiker?: InputMaybe<UsersPermissionsUserFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  magento_customer_ids?: InputMaybe<StringFilterInput>;
  naam?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<KlantFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<KlantFiltersInput>>>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type LongFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['Long']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['Long']['input']>>>;
  contains?: InputMaybe<Scalars['Long']['input']>;
  containsi?: InputMaybe<Scalars['Long']['input']>;
  endsWith?: InputMaybe<Scalars['Long']['input']>;
  eq?: InputMaybe<Scalars['Long']['input']>;
  eqi?: InputMaybe<Scalars['Long']['input']>;
  gt?: InputMaybe<Scalars['Long']['input']>;
  gte?: InputMaybe<Scalars['Long']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Long']['input']>>>;
  lt?: InputMaybe<Scalars['Long']['input']>;
  lte?: InputMaybe<Scalars['Long']['input']>;
  ne?: InputMaybe<Scalars['Long']['input']>;
  nei?: InputMaybe<Scalars['Long']['input']>;
  not?: InputMaybe<LongFilterInput>;
  notContains?: InputMaybe<Scalars['Long']['input']>;
  notContainsi?: InputMaybe<Scalars['Long']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Long']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['Long']['input']>>>;
  startsWith?: InputMaybe<Scalars['Long']['input']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  /** Change user password. Confirm with the current password. */
  changePassword?: Maybe<UsersPermissionsLoginPayload>;
  createAanmelding?: Maybe<AanmeldingEntityResponse>;
  createAdres?: Maybe<AdresEntityResponse>;
  createAttribuut?: Maybe<AttribuutEntityResponse>;
  createAttribuutWaarde?: Maybe<AttribuutWaardeEntityResponse>;
  createFavorietenlijst?: Maybe<FavorietenlijstEntityResponse>;
  createUploadFile?: Maybe<UploadFileEntityResponse>;
  createUploadFolder?: Maybe<UploadFolderEntityResponse>;
  /** Create a new role */
  createUsersPermissionsRole?: Maybe<UsersPermissionsCreateRolePayload>;
  /** Create a new user */
  createUsersPermissionsUser: UsersPermissionsUserEntityResponse;
  deleteAdres?: Maybe<AdresEntityResponse>;
  deleteAttribuut?: Maybe<AttribuutEntityResponse>;
  deleteAttribuutWaarde?: Maybe<AttribuutWaardeEntityResponse>;
  deleteFavorietenlijst?: Maybe<FavorietenlijstEntityResponse>;
  deleteUploadFile?: Maybe<UploadFileEntityResponse>;
  deleteUploadFolder?: Maybe<UploadFolderEntityResponse>;
  /** Delete an existing role */
  deleteUsersPermissionsRole?: Maybe<UsersPermissionsDeleteRolePayload>;
  /** Delete an existing user */
  deleteUsersPermissionsUser: UsersPermissionsUserEntityResponse;
  dubliceerFavorietenlijst?: Maybe<FavorietenlijstEntityResponse>;
  /** Confirm an email users email address */
  emailConfirmation?: Maybe<UsersPermissionsLoginPayload>;
  /** Request a reset password token */
  forgotPassword?: Maybe<UsersPermissionsPasswordPayload>;
  herbestelWinkelwagen?: Maybe<BestellingEntityResponse>;
  login: UsersPermissionsLoginPayload;
  multipleUpload: Array<Maybe<UploadFileEntityResponse>>;
  nieuwFacturatieEmail?: Maybe<UsersPermissionsUserEntityResponse>;
  plaatsBestelling?: Maybe<BestellingEntityResponse>;
  plaatsFavorietenlijstInWinkelwagen?: Maybe<BestellingEntityResponse>;
  plaatsInFavorietenlijst?: Maybe<FavorietenlijstEntityResponse>;
  plaatsProductInWinkelwagen?: Maybe<BestellingEntityResponse>;
  /** Register a user */
  register: UsersPermissionsLoginPayload;
  removeFile?: Maybe<UploadFileEntityResponse>;
  /** Reset user password. Confirm with a code (resetToken from forgotPassword) */
  resetPassword?: Maybe<UsersPermissionsLoginPayload>;
  testOrderbevestiging?: Maybe<BestellingEntityResponse>;
  updateAdres?: Maybe<AdresEntityResponse>;
  updateAttribuut?: Maybe<AttribuutEntityResponse>;
  updateAttribuutWaarde?: Maybe<AttribuutWaardeEntityResponse>;
  updateFavorietenlijst?: Maybe<FavorietenlijstEntityResponse>;
  updateFileInfo: UploadFileEntityResponse;
  updateProductInWinkelwagen?: Maybe<BestellingEntityResponse>;
  updateUploadFile?: Maybe<UploadFileEntityResponse>;
  updateUploadFolder?: Maybe<UploadFolderEntityResponse>;
  /** Update an existing role */
  updateUsersPermissionsRole?: Maybe<UsersPermissionsUpdateRolePayload>;
  /** Update an existing user */
  updateUsersPermissionsUser: UsersPermissionsUserEntityResponse;
  updateWinkelwagen?: Maybe<BestellingEntityResponse>;
  upload: UploadFileEntityResponse;
  verwijderFacturatieEmail?: Maybe<UsersPermissionsUserEntityResponse>;
  verwijderProductUitWinkelwagen?: Maybe<BestellingEntityResponse>;
  verwijderUitFavorietenlijst?: Maybe<FavorietenlijstEntityResponse>;
};


export type MutationChangePasswordArgs = {
  currentPassword: Scalars['String']['input'];
  password: Scalars['String']['input'];
  passwordConfirmation: Scalars['String']['input'];
};


export type MutationCreateAanmeldingArgs = {
  input: AanmeldingInputObject;
};


export type MutationCreateAdresArgs = {
  input: AdresInputObject;
};


export type MutationCreateAttribuutArgs = {
  data: AttribuutInput;
};


export type MutationCreateAttribuutWaardeArgs = {
  data: AttribuutWaardeInput;
};


export type MutationCreateFavorietenlijstArgs = {
  input: FavorietenlijstInputObject;
};


export type MutationCreateUploadFileArgs = {
  data: UploadFileInput;
};


export type MutationCreateUploadFolderArgs = {
  data: UploadFolderInput;
};


export type MutationCreateUsersPermissionsRoleArgs = {
  data: UsersPermissionsRoleInput;
};


export type MutationCreateUsersPermissionsUserArgs = {
  data: UsersPermissionsUserInput;
};


export type MutationDeleteAdresArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDeleteAttribuutArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDeleteAttribuutWaardeArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDeleteFavorietenlijstArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDeleteUploadFileArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDeleteUploadFolderArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDeleteUsersPermissionsRoleArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDeleteUsersPermissionsUserArgs = {
  id: Scalars['ID']['input'];
};


export type MutationDubliceerFavorietenlijstArgs = {
  id: Scalars['ID']['input'];
  input: DubliceerFavorietenlijstInputObject;
};


export type MutationEmailConfirmationArgs = {
  confirmation: Scalars['String']['input'];
};


export type MutationForgotPasswordArgs = {
  email: Scalars['String']['input'];
};


export type MutationHerbestelWinkelwagenArgs = {
  id: Scalars['ID']['input'];
};


export type MutationLoginArgs = {
  input: UsersPermissionsLoginInput;
};


export type MutationMultipleUploadArgs = {
  field?: InputMaybe<Scalars['String']['input']>;
  files: Array<InputMaybe<Scalars['Upload']['input']>>;
  ref?: InputMaybe<Scalars['String']['input']>;
  refId?: InputMaybe<Scalars['ID']['input']>;
};


export type MutationNieuwFacturatieEmailArgs = {
  input: NieuwFacturatieEmailInputObject;
};


export type MutationPlaatsFavorietenlijstInWinkelwagenArgs = {
  id: Scalars['ID']['input'];
};


export type MutationPlaatsInFavorietenlijstArgs = {
  id: Scalars['Int']['input'];
  input: PlaatsInFavorietenlijstInputObject;
};


export type MutationPlaatsProductInWinkelwagenArgs = {
  input: PlaatsProductInWinkelwagenInputObject;
};


export type MutationRegisterArgs = {
  input: UsersPermissionsRegisterInput;
};


export type MutationRemoveFileArgs = {
  id: Scalars['ID']['input'];
};


export type MutationResetPasswordArgs = {
  code: Scalars['String']['input'];
  password: Scalars['String']['input'];
  passwordConfirmation: Scalars['String']['input'];
};


export type MutationTestOrderbevestigingArgs = {
  email?: InputMaybe<Scalars['String']['input']>;
  id: Scalars['Int']['input'];
};


export type MutationUpdateAdresArgs = {
  data: AdresInput;
  id: Scalars['ID']['input'];
};


export type MutationUpdateAttribuutArgs = {
  data: AttribuutInput;
  id: Scalars['ID']['input'];
};


export type MutationUpdateAttribuutWaardeArgs = {
  data: AttribuutWaardeInput;
  id: Scalars['ID']['input'];
};


export type MutationUpdateFavorietenlijstArgs = {
  id: Scalars['ID']['input'];
  input: FavorietenlijstInputObject;
};


export type MutationUpdateFileInfoArgs = {
  id: Scalars['ID']['input'];
  info?: InputMaybe<FileInfoInput>;
};


export type MutationUpdateProductInWinkelwagenArgs = {
  input: UpdateProductInWinkelwagenInputObject;
  productId: Scalars['ID']['input'];
};


export type MutationUpdateUploadFileArgs = {
  data: UploadFileInput;
  id: Scalars['ID']['input'];
};


export type MutationUpdateUploadFolderArgs = {
  data: UploadFolderInput;
  id: Scalars['ID']['input'];
};


export type MutationUpdateUsersPermissionsRoleArgs = {
  data: UsersPermissionsRoleInput;
  id: Scalars['ID']['input'];
};


export type MutationUpdateUsersPermissionsUserArgs = {
  data: UsersPermissionsUserInput;
  id: Scalars['ID']['input'];
};


export type MutationUpdateWinkelwagenArgs = {
  input: BestellingInputObject;
};


export type MutationUploadArgs = {
  field?: InputMaybe<Scalars['String']['input']>;
  file: Scalars['Upload']['input'];
  info?: InputMaybe<FileInfoInput>;
  ref?: InputMaybe<Scalars['String']['input']>;
  refId?: InputMaybe<Scalars['ID']['input']>;
};


export type MutationVerwijderFacturatieEmailArgs = {
  input: NieuwFacturatieEmailInputObject;
};


export type MutationVerwijderProductUitWinkelwagenArgs = {
  productId: Scalars['ID']['input'];
};


export type MutationVerwijderUitFavorietenlijstArgs = {
  id: Scalars['Int']['input'];
  input: PlaatsInFavorietenlijstInputObject;
};

export type NavigationItem = {
  __typename?: 'NavigationItem';
  createdAt?: Maybe<Scalars['String']['output']>;
  createdBy?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['String']['output']>;
  created_by?: Maybe<Scalars['String']['output']>;
  externalPath?: Maybe<Scalars['String']['output']>;
  id: Scalars['Int']['output'];
  items?: Maybe<Array<Maybe<NavigationItem>>>;
  master?: Maybe<Scalars['Int']['output']>;
  menuAttached: Scalars['Boolean']['output'];
  order: Scalars['Int']['output'];
  parent?: Maybe<NavigationItem>;
  path?: Maybe<Scalars['String']['output']>;
  related?: Maybe<NavigationItemRelatedData>;
  title: Scalars['String']['output'];
  type: Scalars['String']['output'];
  uiRouterKey: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['String']['output']>;
  updatedBy?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['String']['output']>;
  updated_by?: Maybe<Scalars['String']['output']>;
};

export type NavigationItemRelated = Categorie | Pagina | Product;

export type NavigationItemRelatedData = {
  __typename?: 'NavigationItemRelatedData';
  attributes?: Maybe<NavigationItemRelated>;
  id: Scalars['Int']['output'];
};

export enum NavigationRenderType {
  Flat = 'FLAT',
  Rfr = 'RFR',
  Tree = 'TREE'
}

export type NieuwFacturatieEmailInputObject = {
  email: Scalars['String']['input'];
};

export type Pagina = {
  __typename?: 'Pagina';
  alias?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  inhoud?: Maybe<Array<Maybe<PaginaInhoudDynamicZone>>>;
  publishedAt?: Maybe<Scalars['DateTime']['output']>;
  seo?: Maybe<ComponentSharedSeo>;
  titel: Scalars['String']['output'];
  toonTitel?: Maybe<Scalars['Boolean']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type PaginaEntity = {
  __typename?: 'PaginaEntity';
  attributes?: Maybe<Pagina>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type PaginaEntityResponse = {
  __typename?: 'PaginaEntityResponse';
  data?: Maybe<PaginaEntity>;
};

export type PaginaEntityResponseCollection = {
  __typename?: 'PaginaEntityResponseCollection';
  data: Array<PaginaEntity>;
  meta: ResponseCollectionMeta;
};

export type PaginaFiltersInput = {
  alias?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<PaginaFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<PaginaFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<PaginaFiltersInput>>>;
  publishedAt?: InputMaybe<DateTimeFilterInput>;
  seo?: InputMaybe<ComponentSharedSeoFiltersInput>;
  titel?: InputMaybe<StringFilterInput>;
  toonTitel?: InputMaybe<BooleanFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type PaginaInhoudDynamicZone = ComponentInhoudAfbeelding | ComponentInhoudBanners | ComponentInhoudButton | ComponentInhoudCategorieNavigatie | ComponentInhoudContent | ComponentInhoudHero | ComponentInhoudInhoud | ComponentInhoudUitgelichteProducten | ComponentInhoudVideo | Error;

export type Pagination = {
  __typename?: 'Pagination';
  page: Scalars['Int']['output'];
  pageCount: Scalars['Int']['output'];
  pageSize: Scalars['Int']['output'];
  total: Scalars['Int']['output'];
};

export type PaginationArg = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  page?: InputMaybe<Scalars['Int']['input']>;
  pageSize?: InputMaybe<Scalars['Int']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
};

export type PlaatsInFavorietenlijstInputObject = {
  product: Scalars['Int']['input'];
};

export type PlaatsProductInWinkelwagenInputObject = {
  aantal?: InputMaybe<Scalars['Int']['input']>;
  product: Scalars['Int']['input'];
};

export type Product = {
  __typename?: 'Product';
  Magento_product_catids?: Maybe<Scalars['String']['output']>;
  afbeelding?: Maybe<UploadFileRelationResponseCollection>;
  alias?: Maybe<Scalars['String']['output']>;
  aliassen?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  attributen?: Maybe<Array<Maybe<Attributen>>>;
  attributenMapping?: Maybe<Array<Maybe<AttribuutMapping>>>;
  attribuut?: Maybe<Array<Maybe<ComponentProductAttributen>>>;
  beschrijving?: Maybe<Scalars['String']['output']>;
  categorie?: Maybe<CategorieRelationResponseCollection>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  documenten?: Maybe<DocumentEntityResponseCollection>;
  documentens?: Maybe<DocumentRelationResponseCollection>;
  favorietenlijsten?: Maybe<FavorietenlijstRelationResponseCollection>;
  gerelateerd?: Maybe<ProductEntityResponseCollection>;
  hoofdpad?: Maybe<Scalars['String']['output']>;
  magento_parent_id?: Maybe<Scalars['Int']['output']>;
  magento_product_id?: Maybe<Scalars['Int']['output']>;
  magento_product_links?: Maybe<Scalars['String']['output']>;
  naam: Scalars['String']['output'];
  prijs: Scalars['Float']['output'];
  prijs_aanbieding?: Maybe<Scalars['Float']['output']>;
  product_type: Enum_Product_Product_Type;
  producten?: Maybe<ProductRelationResponseCollection>;
  productnummer: Scalars['String']['output'];
  publishedAt?: Maybe<Scalars['DateTime']['output']>;
  seo?: Maybe<ComponentSharedSeo>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  zichtbaar?: Maybe<Enum_Product_Zichtbaar>;
};


export type ProductAfbeeldingArgs = {
  filters?: InputMaybe<UploadFileFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type ProductAttribuutArgs = {
  filters?: InputMaybe<ComponentProductAttributenFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type ProductCategorieArgs = {
  filters?: InputMaybe<CategorieFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type ProductDocumentensArgs = {
  filters?: InputMaybe<DocumentFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type ProductFavorietenlijstenArgs = {
  filters?: InputMaybe<FavorietenlijstFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type ProductProductenArgs = {
  filters?: InputMaybe<ProductFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type ProductEntity = {
  __typename?: 'ProductEntity';
  attributes?: Maybe<Product>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type ProductEntityResponse = {
  __typename?: 'ProductEntityResponse';
  data?: Maybe<ProductEntity>;
};

export type ProductEntityResponseCollection = {
  __typename?: 'ProductEntityResponseCollection';
  data: Array<ProductEntity>;
  meta: ResponseCollectionMeta;
};

export type ProductFiltersInput = {
  Magento_product_catids?: InputMaybe<StringFilterInput>;
  alias?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<ProductFiltersInput>>>;
  attribuut?: InputMaybe<ComponentProductAttributenFiltersInput>;
  beschrijving?: InputMaybe<StringFilterInput>;
  categorie?: InputMaybe<CategorieFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  documentens?: InputMaybe<DocumentFiltersInput>;
  favorietenlijsten?: InputMaybe<FavorietenlijstFiltersInput>;
  gerelateerd?: InputMaybe<ProductFiltersInput>;
  hoofdpad?: InputMaybe<StringFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  magento_parent_id?: InputMaybe<IntFilterInput>;
  magento_product_id?: InputMaybe<IntFilterInput>;
  magento_product_links?: InputMaybe<StringFilterInput>;
  naam?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<ProductFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<ProductFiltersInput>>>;
  prijs?: InputMaybe<FloatFilterInput>;
  prijs_aanbieding?: InputMaybe<FloatFilterInput>;
  product_type?: InputMaybe<StringFilterInput>;
  producten?: InputMaybe<ProductFiltersInput>;
  productnummer?: InputMaybe<StringFilterInput>;
  publishedAt?: InputMaybe<DateTimeFilterInput>;
  seo?: InputMaybe<ComponentSharedSeoFiltersInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  zichtbaar?: InputMaybe<StringFilterInput>;
};

export type ProductRelationResponseCollection = {
  __typename?: 'ProductRelationResponseCollection';
  data: Array<ProductEntity>;
};

export type Productregel = {
  __typename?: 'Productregel';
  aantal?: Maybe<Scalars['Int']['output']>;
  naam?: Maybe<Scalars['String']['output']>;
  productnummer?: Maybe<Scalars['String']['output']>;
  totaal?: Maybe<Scalars['Float']['output']>;
  url?: Maybe<Scalars['String']['output']>;
};

export type Productstatistiek = {
  __typename?: 'Productstatistiek';
  aantal?: Maybe<Scalars['Float']['output']>;
  producten?: Maybe<Array<Maybe<Productregel>>>;
  totaal?: Maybe<Scalars['Float']['output']>;
};

export type ProductstatistiekInputObject = {
  /** Afleveradres ID */
  afleverAdres?: InputMaybe<Scalars['ID']['input']>;
  soort?: InputMaybe<BestellingType>;
  /** Datum tot (YYYY-MM-DD) */
  tot?: InputMaybe<Scalars['String']['input']>;
  /** Datum vanaf (YYYY-MM-DD) */
  vanaf?: InputMaybe<Scalars['String']['input']>;
};

export enum PublicationState {
  Live = 'LIVE',
  Preview = 'PREVIEW'
}

export type Query = {
  __typename?: 'Query';
  adres: AdresEntityResponse;
  adressen: AdresEntityResponseCollection;
  algemeen?: Maybe<AlgemeenEntityResponse>;
  attributen?: Maybe<AttribuutEntityResponseCollection>;
  attribuut?: Maybe<AttribuutEntityResponse>;
  attribuutWaarde?: Maybe<AttribuutWaardeEntityResponse>;
  attribuutWaardes?: Maybe<AttribuutWaardeEntityResponseCollection>;
  banner?: Maybe<BannerEntityResponse>;
  banners?: Maybe<BannerEntityResponseCollection>;
  categorie?: Maybe<CategorieEntityResponse>;
  categorieen?: Maybe<CategorieEntityResponseCollection>;
  documenten?: Maybe<DocumentEntityResponseCollection>;
  favorietenlijst?: Maybe<FavorietenlijstEntityResponse>;
  favorietenlijstByAlias?: Maybe<FavorietenlijstEntityResponse>;
  favorietenlijsten: FavorietenlijstEntityResponseCollection;
  findBestelling?: Maybe<BestellingEntityResponse>;
  findBestellingen?: Maybe<BestellingEntityResponseCollection>;
  i18NLocale?: Maybe<I18NLocaleEntityResponse>;
  i18NLocales?: Maybe<I18NLocaleEntityResponseCollection>;
  klanten?: Maybe<KlantEntityResponseCollection>;
  me?: Maybe<UsersPermissionsMe>;
  pagina?: Maybe<PaginaEntityResponse>;
  paginas?: Maybe<PaginaEntityResponseCollection>;
  product?: Maybe<ProductEntityResponse>;
  productByAlias?: Maybe<Scalars['ID']['output']>;
  producten?: Maybe<ProductEntityResponseCollection>;
  productenAdmin?: Maybe<ProductEntityResponseCollection>;
  productenPerCategorie?: Maybe<ProductEntityResponseCollection>;
  productstatistiek?: Maybe<Productstatistiek>;
  renderNavigation: Array<Maybe<NavigationItem>>;
  renderNavigationChild: Array<Maybe<NavigationItem>>;
  triggerEndpoints?: Maybe<AlgemeenEntityResponse>;
  uploadFile?: Maybe<UploadFileEntityResponse>;
  uploadFiles?: Maybe<UploadFileEntityResponseCollection>;
  uploadFolder?: Maybe<UploadFolderEntityResponse>;
  uploadFolders?: Maybe<UploadFolderEntityResponseCollection>;
  usersPermissionsRole?: Maybe<UsersPermissionsRoleEntityResponse>;
  usersPermissionsRoles?: Maybe<UsersPermissionsRoleEntityResponseCollection>;
  usersPermissionsUser?: Maybe<UsersPermissionsUserEntityResponse>;
  usersPermissionsUsers?: Maybe<UsersPermissionsUserEntityResponseCollection>;
  winkelwagen?: Maybe<BestellingEntityResponse>;
  zoekProducten?: Maybe<ProductEntityResponseCollection>;
};


export type QueryAdresArgs = {
  id: Scalars['ID']['input'];
};


export type QueryAdressenArgs = {
  filters?: InputMaybe<AdresFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryAttributenArgs = {
  filters?: InputMaybe<AttribuutFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryAttribuutArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryAttribuutWaardeArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryAttribuutWaardesArgs = {
  filters?: InputMaybe<AttribuutWaardeFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryBannerArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryBannersArgs = {
  filters?: InputMaybe<BannerFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryCategorieArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryCategorieenArgs = {
  filters?: InputMaybe<CategorieFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryDocumentenArgs = {
  filters?: InputMaybe<DocumentFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryFavorietenlijstArgs = {
  id: Scalars['Int']['input'];
};


export type QueryFavorietenlijstByAliasArgs = {
  alias: Scalars['String']['input'];
};


export type QueryFavorietenlijstenArgs = {
  filters?: InputMaybe<FavorietenlijstFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryFindBestellingArgs = {
  bestelnummer: Scalars['Int']['input'];
};


export type QueryFindBestellingenArgs = {
  filters?: InputMaybe<BestellingFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryI18NLocaleArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryI18NLocalesArgs = {
  filters?: InputMaybe<I18NLocaleFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryKlantenArgs = {
  filters?: InputMaybe<KlantFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryPaginaArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryPaginasArgs = {
  filters?: InputMaybe<PaginaFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryProductArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryProductByAliasArgs = {
  alias: Scalars['String']['input'];
};


export type QueryProductenArgs = {
  filters?: InputMaybe<ProductFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryProductenAdminArgs = {
  filters?: InputMaybe<ProductFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  publicationState?: InputMaybe<PublicationState>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryProductenPerCategorieArgs = {
  categorie: Scalars['ID']['input'];
};


export type QueryProductstatistiekArgs = {
  input?: InputMaybe<ProductstatistiekInputObject>;
};


export type QueryRenderNavigationArgs = {
  locale?: InputMaybe<Scalars['I18NLocaleCode']['input']>;
  menuOnly?: InputMaybe<Scalars['Boolean']['input']>;
  navigationIdOrSlug: Scalars['String']['input'];
  path?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<NavigationRenderType>;
};


export type QueryRenderNavigationChildArgs = {
  childUiKey: Scalars['String']['input'];
  id: Scalars['String']['input'];
  menuOnly?: InputMaybe<Scalars['Boolean']['input']>;
  type?: InputMaybe<NavigationRenderType>;
};


export type QueryTriggerEndpointsArgs = {
  buro26: Scalars['Int']['input'];
  type: TriggerEndpointsType;
};


export type QueryUploadFileArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryUploadFilesArgs = {
  filters?: InputMaybe<UploadFileFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryUploadFolderArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryUploadFoldersArgs = {
  filters?: InputMaybe<UploadFolderFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryUsersPermissionsRoleArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryUsersPermissionsRolesArgs = {
  filters?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryUsersPermissionsUserArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


export type QueryUsersPermissionsUsersArgs = {
  filters?: InputMaybe<UsersPermissionsUserFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type QueryZoekProductenArgs = {
  zoekterm: Scalars['String']['input'];
};

export type ResponseCollectionMeta = {
  __typename?: 'ResponseCollectionMeta';
  pagination: Pagination;
};

export type StringFilterInput = {
  and?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  between?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  contains?: InputMaybe<Scalars['String']['input']>;
  containsi?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  eq?: InputMaybe<Scalars['String']['input']>;
  eqi?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  ne?: InputMaybe<Scalars['String']['input']>;
  nei?: InputMaybe<Scalars['String']['input']>;
  not?: InputMaybe<StringFilterInput>;
  notContains?: InputMaybe<Scalars['String']['input']>;
  notContainsi?: InputMaybe<Scalars['String']['input']>;
  notIn?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  notNull?: InputMaybe<Scalars['Boolean']['input']>;
  null?: InputMaybe<Scalars['Boolean']['input']>;
  or?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export enum TriggerEndpointsType {
  Adres = 'ADRES',
  Attribuut = 'ATTRIBUUT',
  Categorie = 'CATEGORIE',
  Favorietenlijst = 'FAVORIETENLIJST'
}

export type UpdateProductInWinkelwagenInputObject = {
  aantal?: InputMaybe<Scalars['Int']['input']>;
};

export type UploadFile = {
  __typename?: 'UploadFile';
  alternativeText?: Maybe<Scalars['String']['output']>;
  caption?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  ext?: Maybe<Scalars['String']['output']>;
  formats?: Maybe<Scalars['JSON']['output']>;
  hash: Scalars['String']['output'];
  height?: Maybe<Scalars['Int']['output']>;
  mime: Scalars['String']['output'];
  name: Scalars['String']['output'];
  previewUrl?: Maybe<Scalars['String']['output']>;
  provider: Scalars['String']['output'];
  provider_metadata?: Maybe<Scalars['JSON']['output']>;
  related?: Maybe<Array<Maybe<GenericMorph>>>;
  size: Scalars['Float']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  url: Scalars['String']['output'];
  width?: Maybe<Scalars['Int']['output']>;
};

export type UploadFileEntity = {
  __typename?: 'UploadFileEntity';
  attributes?: Maybe<UploadFile>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type UploadFileEntityResponse = {
  __typename?: 'UploadFileEntityResponse';
  data?: Maybe<UploadFileEntity>;
};

export type UploadFileEntityResponseCollection = {
  __typename?: 'UploadFileEntityResponseCollection';
  data: Array<UploadFileEntity>;
  meta: ResponseCollectionMeta;
};

export type UploadFileFiltersInput = {
  alternativeText?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<UploadFileFiltersInput>>>;
  caption?: InputMaybe<StringFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  ext?: InputMaybe<StringFilterInput>;
  folder?: InputMaybe<UploadFolderFiltersInput>;
  folderPath?: InputMaybe<StringFilterInput>;
  formats?: InputMaybe<JsonFilterInput>;
  hash?: InputMaybe<StringFilterInput>;
  height?: InputMaybe<IntFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  mime?: InputMaybe<StringFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<UploadFileFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UploadFileFiltersInput>>>;
  previewUrl?: InputMaybe<StringFilterInput>;
  provider?: InputMaybe<StringFilterInput>;
  provider_metadata?: InputMaybe<JsonFilterInput>;
  size?: InputMaybe<FloatFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  url?: InputMaybe<StringFilterInput>;
  width?: InputMaybe<IntFilterInput>;
};

export type UploadFileInput = {
  alternativeText?: InputMaybe<Scalars['String']['input']>;
  caption?: InputMaybe<Scalars['String']['input']>;
  ext?: InputMaybe<Scalars['String']['input']>;
  folder?: InputMaybe<Scalars['ID']['input']>;
  folderPath?: InputMaybe<Scalars['String']['input']>;
  formats?: InputMaybe<Scalars['JSON']['input']>;
  hash?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Int']['input']>;
  mime?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  previewUrl?: InputMaybe<Scalars['String']['input']>;
  provider?: InputMaybe<Scalars['String']['input']>;
  provider_metadata?: InputMaybe<Scalars['JSON']['input']>;
  size?: InputMaybe<Scalars['Float']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
  width?: InputMaybe<Scalars['Int']['input']>;
};

export type UploadFileRelationResponseCollection = {
  __typename?: 'UploadFileRelationResponseCollection';
  data: Array<UploadFileEntity>;
};

export type UploadFolder = {
  __typename?: 'UploadFolder';
  children?: Maybe<UploadFolderRelationResponseCollection>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  files?: Maybe<UploadFileRelationResponseCollection>;
  name: Scalars['String']['output'];
  parent?: Maybe<UploadFolderEntityResponse>;
  path: Scalars['String']['output'];
  pathId: Scalars['Int']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};


export type UploadFolderChildrenArgs = {
  filters?: InputMaybe<UploadFolderFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type UploadFolderFilesArgs = {
  filters?: InputMaybe<UploadFileFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type UploadFolderEntity = {
  __typename?: 'UploadFolderEntity';
  attributes?: Maybe<UploadFolder>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type UploadFolderEntityResponse = {
  __typename?: 'UploadFolderEntityResponse';
  data?: Maybe<UploadFolderEntity>;
};

export type UploadFolderEntityResponseCollection = {
  __typename?: 'UploadFolderEntityResponseCollection';
  data: Array<UploadFolderEntity>;
  meta: ResponseCollectionMeta;
};

export type UploadFolderFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<UploadFolderFiltersInput>>>;
  children?: InputMaybe<UploadFolderFiltersInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  files?: InputMaybe<UploadFileFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<UploadFolderFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UploadFolderFiltersInput>>>;
  parent?: InputMaybe<UploadFolderFiltersInput>;
  path?: InputMaybe<StringFilterInput>;
  pathId?: InputMaybe<IntFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type UploadFolderInput = {
  children?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  files?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  name?: InputMaybe<Scalars['String']['input']>;
  parent?: InputMaybe<Scalars['ID']['input']>;
  path?: InputMaybe<Scalars['String']['input']>;
  pathId?: InputMaybe<Scalars['Int']['input']>;
};

export type UploadFolderRelationResponseCollection = {
  __typename?: 'UploadFolderRelationResponseCollection';
  data: Array<UploadFolderEntity>;
};

export type UsersPermissionsCreateRolePayload = {
  __typename?: 'UsersPermissionsCreateRolePayload';
  ok: Scalars['Boolean']['output'];
};

export type UsersPermissionsDeleteRolePayload = {
  __typename?: 'UsersPermissionsDeleteRolePayload';
  ok: Scalars['Boolean']['output'];
};

export type UsersPermissionsLoginInput = {
  identifier: Scalars['String']['input'];
  password: Scalars['String']['input'];
  provider?: Scalars['String']['input'];
};

export type UsersPermissionsLoginPayload = {
  __typename?: 'UsersPermissionsLoginPayload';
  jwt?: Maybe<Scalars['String']['output']>;
  user: UsersPermissionsMe;
};

export type UsersPermissionsMe = {
  __typename?: 'UsersPermissionsMe';
  achternaam: Scalars['String']['output'];
  blocked?: Maybe<Scalars['Boolean']['output']>;
  confirmed?: Maybe<Scalars['Boolean']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  facturatie?: Maybe<Array<Maybe<ComponentMiscFacturatie>>>;
  id: Scalars['ID']['output'];
  klant?: Maybe<KlantEntityResponse>;
  rol?: Maybe<Scalars['String']['output']>;
  role?: Maybe<UsersPermissionsMeRole>;
  username: Scalars['String']['output'];
  voornaam: Scalars['String']['output'];
};

export type UsersPermissionsMeRole = {
  __typename?: 'UsersPermissionsMeRole';
  description?: Maybe<Scalars['String']['output']>;
  id: Scalars['ID']['output'];
  name: Scalars['String']['output'];
  type?: Maybe<Scalars['String']['output']>;
};

export type UsersPermissionsPasswordPayload = {
  __typename?: 'UsersPermissionsPasswordPayload';
  ok: Scalars['Boolean']['output'];
};

export type UsersPermissionsPermission = {
  __typename?: 'UsersPermissionsPermission';
  action: Scalars['String']['output'];
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  role?: Maybe<UsersPermissionsRoleEntityResponse>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type UsersPermissionsPermissionEntity = {
  __typename?: 'UsersPermissionsPermissionEntity';
  attributes?: Maybe<UsersPermissionsPermission>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type UsersPermissionsPermissionFiltersInput = {
  action?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<UsersPermissionsPermissionFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  not?: InputMaybe<UsersPermissionsPermissionFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UsersPermissionsPermissionFiltersInput>>>;
  role?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
};

export type UsersPermissionsPermissionRelationResponseCollection = {
  __typename?: 'UsersPermissionsPermissionRelationResponseCollection';
  data: Array<UsersPermissionsPermissionEntity>;
};

export type UsersPermissionsRegisterInput = {
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
  username: Scalars['String']['input'];
};

export type UsersPermissionsRole = {
  __typename?: 'UsersPermissionsRole';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  name: Scalars['String']['output'];
  permissions?: Maybe<UsersPermissionsPermissionRelationResponseCollection>;
  type?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  users?: Maybe<UsersPermissionsUserRelationResponseCollection>;
};


export type UsersPermissionsRolePermissionsArgs = {
  filters?: InputMaybe<UsersPermissionsPermissionFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type UsersPermissionsRoleUsersArgs = {
  filters?: InputMaybe<UsersPermissionsUserFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type UsersPermissionsRoleEntity = {
  __typename?: 'UsersPermissionsRoleEntity';
  attributes?: Maybe<UsersPermissionsRole>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type UsersPermissionsRoleEntityResponse = {
  __typename?: 'UsersPermissionsRoleEntityResponse';
  data?: Maybe<UsersPermissionsRoleEntity>;
};

export type UsersPermissionsRoleEntityResponseCollection = {
  __typename?: 'UsersPermissionsRoleEntityResponseCollection';
  data: Array<UsersPermissionsRoleEntity>;
  meta: ResponseCollectionMeta;
};

export type UsersPermissionsRoleFiltersInput = {
  and?: InputMaybe<Array<InputMaybe<UsersPermissionsRoleFiltersInput>>>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  description?: InputMaybe<StringFilterInput>;
  id?: InputMaybe<IdFilterInput>;
  name?: InputMaybe<StringFilterInput>;
  not?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UsersPermissionsRoleFiltersInput>>>;
  permissions?: InputMaybe<UsersPermissionsPermissionFiltersInput>;
  type?: InputMaybe<StringFilterInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  users?: InputMaybe<UsersPermissionsUserFiltersInput>;
};

export type UsersPermissionsRoleInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  permissions?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  type?: InputMaybe<Scalars['String']['input']>;
  users?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
};

export type UsersPermissionsUpdateRolePayload = {
  __typename?: 'UsersPermissionsUpdateRolePayload';
  ok: Scalars['Boolean']['output'];
};

export type UsersPermissionsUser = {
  __typename?: 'UsersPermissionsUser';
  achternaam: Scalars['String']['output'];
  bestellingen?: Maybe<BestellingRelationResponseCollection>;
  blocked?: Maybe<Scalars['Boolean']['output']>;
  confirmed?: Maybe<Scalars['Boolean']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  email: Scalars['String']['output'];
  facturatie?: Maybe<Array<Maybe<ComponentMiscFacturatie>>>;
  favorietenlijsten?: Maybe<FavorietenlijstRelationResponseCollection>;
  klant?: Maybe<KlantEntityResponse>;
  magento_customer_id?: Maybe<Scalars['Int']['output']>;
  provider?: Maybe<Scalars['String']['output']>;
  role?: Maybe<UsersPermissionsRoleEntityResponse>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  username: Scalars['String']['output'];
  voornaam: Scalars['String']['output'];
};


export type UsersPermissionsUserBestellingenArgs = {
  filters?: InputMaybe<BestellingFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type UsersPermissionsUserFacturatieArgs = {
  filters?: InputMaybe<ComponentMiscFacturatieFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};


export type UsersPermissionsUserFavorietenlijstenArgs = {
  filters?: InputMaybe<FavorietenlijstFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

export type UsersPermissionsUserEntity = {
  __typename?: 'UsersPermissionsUserEntity';
  attributes?: Maybe<UsersPermissionsUser>;
  id?: Maybe<Scalars['ID']['output']>;
};

export type UsersPermissionsUserEntityResponse = {
  __typename?: 'UsersPermissionsUserEntityResponse';
  data?: Maybe<UsersPermissionsUserEntity>;
};

export type UsersPermissionsUserEntityResponseCollection = {
  __typename?: 'UsersPermissionsUserEntityResponseCollection';
  data: Array<UsersPermissionsUserEntity>;
  meta: ResponseCollectionMeta;
};

export type UsersPermissionsUserFiltersInput = {
  achternaam?: InputMaybe<StringFilterInput>;
  and?: InputMaybe<Array<InputMaybe<UsersPermissionsUserFiltersInput>>>;
  bestellingen?: InputMaybe<BestellingFiltersInput>;
  blocked?: InputMaybe<BooleanFilterInput>;
  confirmationToken?: InputMaybe<StringFilterInput>;
  confirmed?: InputMaybe<BooleanFilterInput>;
  createdAt?: InputMaybe<DateTimeFilterInput>;
  email?: InputMaybe<StringFilterInput>;
  facturatie?: InputMaybe<ComponentMiscFacturatieFiltersInput>;
  favorietenlijsten?: InputMaybe<FavorietenlijstFiltersInput>;
  id?: InputMaybe<IdFilterInput>;
  klant?: InputMaybe<KlantFiltersInput>;
  magento_customer_id?: InputMaybe<IntFilterInput>;
  not?: InputMaybe<UsersPermissionsUserFiltersInput>;
  or?: InputMaybe<Array<InputMaybe<UsersPermissionsUserFiltersInput>>>;
  password?: InputMaybe<StringFilterInput>;
  provider?: InputMaybe<StringFilterInput>;
  resetPasswordToken?: InputMaybe<StringFilterInput>;
  role?: InputMaybe<UsersPermissionsRoleFiltersInput>;
  updatedAt?: InputMaybe<DateTimeFilterInput>;
  username?: InputMaybe<StringFilterInput>;
  voornaam?: InputMaybe<StringFilterInput>;
};

export type UsersPermissionsUserInput = {
  achternaam?: InputMaybe<Scalars['String']['input']>;
  bestellingen?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  blocked?: InputMaybe<Scalars['Boolean']['input']>;
  confirmationToken?: InputMaybe<Scalars['String']['input']>;
  confirmed?: InputMaybe<Scalars['Boolean']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  facturatie?: InputMaybe<Array<InputMaybe<ComponentMiscFacturatieInput>>>;
  favorietenlijsten?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  klant?: InputMaybe<Scalars['ID']['input']>;
  magento_customer_id?: InputMaybe<Scalars['Int']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  provider?: InputMaybe<Scalars['String']['input']>;
  resetPasswordToken?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['ID']['input']>;
  username?: InputMaybe<Scalars['String']['input']>;
  voornaam?: InputMaybe<Scalars['String']['input']>;
};

export type UsersPermissionsUserRelationResponseCollection = {
  __typename?: 'UsersPermissionsUserRelationResponseCollection';
  data: Array<UsersPermissionsUserEntity>;
};

export type AdresFragmentFragment = { __typename?: 'AdresEntity', id?: string | null, attributes?: { __typename?: 'Adres', voornaam?: string | null, tussenvoegsel?: string | null, achternaam?: string | null, straatnaam: string, huisnummer: string, postcode: string, plaats: string, provincie?: Enum_Adres_Provincie | null, telefoon?: string | null, bedrijf: string, btw_nummer?: string | null, projectcode?: string | null, budget?: any | null, opmerking?: string | null } | null };

export type AdresFindManyQueryVariables = Exact<{
  filters?: InputMaybe<AdresFiltersInput>;
  pagination?: InputMaybe<PaginationArg>;
  sort?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>> | InputMaybe<Scalars['String']['input']>>;
}>;


export type AdresFindManyQuery = { __typename?: 'Query', adressen: { __typename?: 'AdresEntityResponseCollection', data: Array<{ __typename?: 'AdresEntity', id?: string | null, attributes?: { __typename?: 'Adres', bedrijf: string, voornaam?: string | null, tussenvoegsel?: string | null, achternaam?: string | null, straatnaam: string, huisnummer: string, postcode: string, plaats: string, budget?: any | null, opmerking?: string | null, projectcode?: string | null } | null }>, meta: { __typename?: 'ResponseCollectionMeta', pagination: { __typename?: 'Pagination', total: number, page: number, pageCount: number, pageSize: number } } } };

export type AdresFindQueryVariables = Exact<{
  filters?: InputMaybe<AdresFiltersInput>;
}>;


export type AdresFindQuery = { __typename?: 'Query', adressen: { __typename?: 'AdresEntityResponseCollection', data: Array<{ __typename?: 'AdresEntity', id?: string | null, attributes?: { __typename?: 'Adres', voornaam?: string | null, tussenvoegsel?: string | null, achternaam?: string | null, straatnaam: string, huisnummer: string, postcode: string, plaats: string, provincie?: Enum_Adres_Provincie | null, telefoon?: string | null, bedrijf: string, btw_nummer?: string | null, projectcode?: string | null, budget?: any | null, opmerking?: string | null } | null }> } };

export type InhoudAfbeeldingFragmentFragment = { __typename: 'ComponentInhoudAfbeelding', id: string, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } };

export type InhoudBannersFragmentFragment = { __typename: 'ComponentInhoudBanners', id: string, banners?: { __typename?: 'BannerRelationResponseCollection', data: Array<{ __typename?: 'BannerEntity', id?: string | null, attributes?: { __typename?: 'Banner', titel: string, inhoud?: string | null, kleur?: Enum_Banner_Kleur | null, url?: string | null, product_prijs?: number | null, product_prijs_aanbieding?: number | null, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } } | null }> } | null };

export type InhoudCategorieNavigatieFragmentFragment = { __typename: 'ComponentInhoudCategorieNavigatie', id: string, categorie?: { __typename?: 'CategorieEntityResponse', data?: { __typename?: 'CategorieEntity', id?: string | null } | null } | null, cats?: { __typename?: 'CategorieRelationResponseCollection', data: Array<{ __typename?: 'CategorieEntity', id?: string | null }> } | null };

export type InhoudContentFragmentFragment = { __typename: 'ComponentInhoudContent', id: string, inhoud: string, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null }, button?: { __typename?: 'ComponentMiscButton', id: string, label: string, url: string } | null };

export type InhoudHeroFragmentFragment = { __typename: 'ComponentInhoudHero', id: string, titel: string, tekst: string, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null }, button?: { __typename?: 'ComponentMiscButton', id: string, label: string, url: string } | null };

export type InhoudButtonFragmentFragment = { __typename: 'ComponentInhoudButton', id: string, url?: string | null, label?: string | null };

export type InhoudTekstFragmentFragment = { __typename: 'ComponentInhoudInhoud', id: string, tekst: string };

export type InhoudVideoFragmentFragment = { __typename: 'ComponentInhoudVideo', id: string, alt_text?: string | null, youtube_url?: string | null };

export type InhoudUitgelichteProductenFragmentFragment = { __typename: 'ComponentInhoudUitgelichteProducten', id: string, titel: string, buttonTekst: string, categorie?: { __typename?: 'CategorieEntityResponse', data?: { __typename?: 'CategorieEntity', id?: string | null, attributes?: { __typename?: 'Categorie', pad?: string | null } | null } | null } | null };

export type StrapiImageFragmentFragment = { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null };

export type ResponseCollectionMetaFragmentFragment = { __typename?: 'ResponseCollectionMeta', pagination: { __typename?: 'Pagination', total: number, page: number, pageCount: number, pageSize: number } };

export type MiscButtonFragmentFragment = { __typename?: 'ComponentMiscButton', id: string, label: string, url: string };

export type FindNavigationQueryVariables = Exact<{
  navigationId: Scalars['String']['input'];
}>;


export type FindNavigationQuery = { __typename?: 'Query', renderNavigation: Array<{ __typename?: 'NavigationItem', id: number, path?: string | null, title: string, type: string, externalPath?: string | null, items?: Array<{ __typename?: 'NavigationItem', id: number, path?: string | null, title: string, type: string, externalPath?: string | null } | null> | null } | null> };

export type FindNavigationItemByPathQueryVariables = Exact<{
  navigationId: Scalars['String']['input'];
  path: Scalars['String']['input'];
}>;


export type FindNavigationItemByPathQuery = { __typename?: 'Query', renderNavigation: Array<{ __typename?: 'NavigationItem', id: number, path?: string | null, related?: { __typename?: 'NavigationItemRelatedData', id: number, attributes?: { __typename: 'Categorie' } | { __typename: 'Pagina' } | { __typename: 'Product' } | null } | null } | null> };

export type PaginaQueryVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type PaginaQuery = { __typename?: 'Query', pagina?: { __typename?: 'PaginaEntityResponse', data?: { __typename?: 'PaginaEntity', id?: string | null, attributes?: { __typename?: 'Pagina', titel: string, alias?: string | null, toonTitel?: boolean | null, seo?: { __typename?: 'ComponentSharedSeo', id: string, metaTitle?: string | null, metaDescription?: string | null, keywords?: string | null, metaRobots?: string | null, structuredData?: any | null, metaViewport?: string | null, canonicalURL?: string | null, metaImage?: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } | null, metaSocial?: Array<{ __typename?: 'ComponentSharedMetaSocial', id: string, title: string, description: string, socialNetwork: Enum_Componentsharedmetasocial_Socialnetwork, image?: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } | null } | null> | null } | null, inhoud?: Array<{ __typename: 'ComponentInhoudAfbeelding', id: string, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } } | { __typename: 'ComponentInhoudBanners', id: string, banners?: { __typename?: 'BannerRelationResponseCollection', data: Array<{ __typename?: 'BannerEntity', id?: string | null, attributes?: { __typename?: 'Banner', titel: string, inhoud?: string | null, kleur?: Enum_Banner_Kleur | null, url?: string | null, product_prijs?: number | null, product_prijs_aanbieding?: number | null, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } } | null }> } | null } | { __typename: 'ComponentInhoudButton', id: string, url?: string | null, label?: string | null } | { __typename: 'ComponentInhoudCategorieNavigatie', id: string, categorie?: { __typename?: 'CategorieEntityResponse', data?: { __typename?: 'CategorieEntity', id?: string | null } | null } | null, cats?: { __typename?: 'CategorieRelationResponseCollection', data: Array<{ __typename?: 'CategorieEntity', id?: string | null }> } | null } | { __typename: 'ComponentInhoudContent', id: string, inhoud: string, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null }, button?: { __typename?: 'ComponentMiscButton', id: string, label: string, url: string } | null } | { __typename: 'ComponentInhoudHero', id: string, titel: string, tekst: string, afbeelding: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null }, button?: { __typename?: 'ComponentMiscButton', id: string, label: string, url: string } | null } | { __typename: 'ComponentInhoudInhoud', id: string, tekst: string } | { __typename: 'ComponentInhoudUitgelichteProducten', id: string, titel: string, buttonTekst: string, categorie?: { __typename?: 'CategorieEntityResponse', data?: { __typename?: 'CategorieEntity', id?: string | null, attributes?: { __typename?: 'Categorie', pad?: string | null } | null } | null } | null } | { __typename: 'ComponentInhoudVideo', id: string, alt_text?: string | null, youtube_url?: string | null } | { __typename?: 'Error' } | null> | null } | null } | null } | null };

export type PageByAliasQueryVariables = Exact<{
  alias: Scalars['String']['input'];
}>;


export type PageByAliasQuery = { __typename?: 'Query', paginas?: { __typename?: 'PaginaEntityResponseCollection', data: Array<{ __typename?: 'PaginaEntity', id?: string | null, attributes?: { __typename?: 'Pagina', titel: string, alias?: string | null } | null }> } | null };

export type SeoFragmentFragment = { __typename?: 'ComponentSharedSeo', id: string, metaTitle?: string | null, metaDescription?: string | null, keywords?: string | null, metaRobots?: string | null, structuredData?: any | null, metaViewport?: string | null, canonicalURL?: string | null, metaImage?: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } | null, metaSocial?: Array<{ __typename?: 'ComponentSharedMetaSocial', id: string, title: string, description: string, socialNetwork: Enum_Componentsharedmetasocial_Socialnetwork, image?: { __typename?: 'UploadFileEntityResponse', data?: { __typename?: 'UploadFileEntity', id?: string | null, attributes?: { __typename?: 'UploadFile', url: string, alternativeText?: string | null, hash: string, mime: string, name: string, provider: string, size: number, width?: number | null, height?: number | null, formats?: any | null } | null } | null } | null } | null> | null };

export const AdresFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AdresFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"AdresEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"voornaam"}},{"kind":"Field","name":{"kind":"Name","value":"tussenvoegsel"}},{"kind":"Field","name":{"kind":"Name","value":"achternaam"}},{"kind":"Field","name":{"kind":"Name","value":"straatnaam"}},{"kind":"Field","name":{"kind":"Name","value":"huisnummer"}},{"kind":"Field","name":{"kind":"Name","value":"postcode"}},{"kind":"Field","name":{"kind":"Name","value":"plaats"}},{"kind":"Field","name":{"kind":"Name","value":"provincie"}},{"kind":"Field","name":{"kind":"Name","value":"telefoon"}},{"kind":"Field","name":{"kind":"Name","value":"bedrijf"}},{"kind":"Field","name":{"kind":"Name","value":"btw_nummer"}},{"kind":"Field","name":{"kind":"Name","value":"projectcode"}},{"kind":"Field","name":{"kind":"Name","value":"budget"}},{"kind":"Field","name":{"kind":"Name","value":"opmerking"}}]}}]}}]} as unknown as DocumentNode<AdresFragmentFragment, unknown>;
export const StrapiImageFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"StrapiImageFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UploadFileEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeText"}},{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"mime"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"size"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"formats"}}]}}]}}]} as unknown as DocumentNode<StrapiImageFragmentFragment, unknown>;
export const InhoudAfbeeldingFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudAfbeeldingFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudAfbeelding"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"StrapiImageFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UploadFileEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeText"}},{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"mime"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"size"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"formats"}}]}}]}}]} as unknown as DocumentNode<InhoudAfbeeldingFragmentFragment, unknown>;
export const InhoudBannersFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudBannersFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudBanners"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"banners"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"inhoud"}},{"kind":"Field","name":{"kind":"Name","value":"kleur"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"product_prijs"}},{"kind":"Field","name":{"kind":"Name","value":"product_prijs_aanbieding"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"StrapiImageFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UploadFileEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeText"}},{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"mime"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"size"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"formats"}}]}}]}}]} as unknown as DocumentNode<InhoudBannersFragmentFragment, unknown>;
export const InhoudCategorieNavigatieFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudCategorieNavigatieFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudCategorieNavigatie"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"categorie"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"cats"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode<InhoudCategorieNavigatieFragmentFragment, unknown>;
export const MiscButtonFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MiscButtonFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentMiscButton"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"url"}}]}}]} as unknown as DocumentNode<MiscButtonFragmentFragment, unknown>;
export const InhoudContentFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudContentFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudContent"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"inhoud"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"button"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MiscButtonFragment"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"StrapiImageFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UploadFileEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeText"}},{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"mime"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"size"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"formats"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MiscButtonFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentMiscButton"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"url"}}]}}]} as unknown as DocumentNode<InhoudContentFragmentFragment, unknown>;
export const InhoudHeroFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudHeroFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudHero"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"tekst"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"button"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MiscButtonFragment"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"StrapiImageFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UploadFileEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeText"}},{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"mime"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"size"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"formats"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MiscButtonFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentMiscButton"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"url"}}]}}]} as unknown as DocumentNode<InhoudHeroFragmentFragment, unknown>;
export const InhoudButtonFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudButtonFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudButton"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}}]} as unknown as DocumentNode<InhoudButtonFragmentFragment, unknown>;
export const InhoudTekstFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudTekstFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudInhoud"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"tekst"}}]}}]} as unknown as DocumentNode<InhoudTekstFragmentFragment, unknown>;
export const InhoudVideoFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudVideoFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudVideo"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"alt_text"}},{"kind":"Field","name":{"kind":"Name","value":"youtube_url"}}]}}]} as unknown as DocumentNode<InhoudVideoFragmentFragment, unknown>;
export const InhoudUitgelichteProductenFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudUitgelichteProductenFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudUitgelichteProducten"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"buttonTekst"}},{"kind":"Field","name":{"kind":"Name","value":"categorie"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"pad"}}]}}]}}]}}]}}]} as unknown as DocumentNode<InhoudUitgelichteProductenFragmentFragment, unknown>;
export const ResponseCollectionMetaFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ResponseCollectionMetaFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ResponseCollectionMeta"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"pagination"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"total"}},{"kind":"Field","name":{"kind":"Name","value":"page"}},{"kind":"Field","name":{"kind":"Name","value":"pageCount"}},{"kind":"Field","name":{"kind":"Name","value":"pageSize"}}]}}]}}]} as unknown as DocumentNode<ResponseCollectionMetaFragmentFragment, unknown>;
export const SeoFragmentFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"SeoFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentSharedSeo"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"metaTitle"}},{"kind":"Field","name":{"kind":"Name","value":"metaDescription"}},{"kind":"Field","name":{"kind":"Name","value":"metaImage"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"keywords"}},{"kind":"Field","name":{"kind":"Name","value":"metaRobots"}},{"kind":"Field","name":{"kind":"Name","value":"structuredData"}},{"kind":"Field","name":{"kind":"Name","value":"metaViewport"}},{"kind":"Field","name":{"kind":"Name","value":"canonicalURL"}},{"kind":"Field","name":{"kind":"Name","value":"metaSocial"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"socialNetwork"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"StrapiImageFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UploadFileEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeText"}},{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"mime"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"size"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"formats"}}]}}]}}]} as unknown as DocumentNode<SeoFragmentFragment, unknown>;
export const AdresFindManyDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"adresFindMany"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filters"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"AdresFiltersInput"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"pagination"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"PaginationArg"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"sort"}},"type":{"kind":"ListType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"adressen"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"filters"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filters"}}},{"kind":"Argument","name":{"kind":"Name","value":"pagination"},"value":{"kind":"Variable","name":{"kind":"Name","value":"pagination"}}},{"kind":"Argument","name":{"kind":"Name","value":"sort"},"value":{"kind":"Variable","name":{"kind":"Name","value":"sort"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bedrijf"}},{"kind":"Field","name":{"kind":"Name","value":"voornaam"}},{"kind":"Field","name":{"kind":"Name","value":"tussenvoegsel"}},{"kind":"Field","name":{"kind":"Name","value":"achternaam"}},{"kind":"Field","name":{"kind":"Name","value":"straatnaam"}},{"kind":"Field","name":{"kind":"Name","value":"huisnummer"}},{"kind":"Field","name":{"kind":"Name","value":"postcode"}},{"kind":"Field","name":{"kind":"Name","value":"plaats"}},{"kind":"Field","name":{"kind":"Name","value":"budget"}},{"kind":"Field","name":{"kind":"Name","value":"opmerking"}},{"kind":"Field","name":{"kind":"Name","value":"projectcode"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"meta"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"ResponseCollectionMetaFragment"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"ResponseCollectionMetaFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ResponseCollectionMeta"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"pagination"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"total"}},{"kind":"Field","name":{"kind":"Name","value":"page"}},{"kind":"Field","name":{"kind":"Name","value":"pageCount"}},{"kind":"Field","name":{"kind":"Name","value":"pageSize"}}]}}]}}]} as unknown as DocumentNode<AdresFindManyQuery, AdresFindManyQueryVariables>;
export const AdresFindDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"adresFind"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filters"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"AdresFiltersInput"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"adressen"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"filters"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filters"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"AdresFragment"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AdresFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"AdresEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"voornaam"}},{"kind":"Field","name":{"kind":"Name","value":"tussenvoegsel"}},{"kind":"Field","name":{"kind":"Name","value":"achternaam"}},{"kind":"Field","name":{"kind":"Name","value":"straatnaam"}},{"kind":"Field","name":{"kind":"Name","value":"huisnummer"}},{"kind":"Field","name":{"kind":"Name","value":"postcode"}},{"kind":"Field","name":{"kind":"Name","value":"plaats"}},{"kind":"Field","name":{"kind":"Name","value":"provincie"}},{"kind":"Field","name":{"kind":"Name","value":"telefoon"}},{"kind":"Field","name":{"kind":"Name","value":"bedrijf"}},{"kind":"Field","name":{"kind":"Name","value":"btw_nummer"}},{"kind":"Field","name":{"kind":"Name","value":"projectcode"}},{"kind":"Field","name":{"kind":"Name","value":"budget"}},{"kind":"Field","name":{"kind":"Name","value":"opmerking"}}]}}]}}]} as unknown as DocumentNode<AdresFindQuery, AdresFindQueryVariables>;
export const FindNavigationDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"findNavigation"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"navigationId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"renderNavigation"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"navigationIdOrSlug"},"value":{"kind":"Variable","name":{"kind":"Name","value":"navigationId"}}},{"kind":"Argument","name":{"kind":"Name","value":"type"},"value":{"kind":"EnumValue","value":"TREE"}},{"kind":"Argument","name":{"kind":"Name","value":"menuOnly"},"value":{"kind":"BooleanValue","value":true}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"path"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"externalPath"}},{"kind":"Field","name":{"kind":"Name","value":"items"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"path"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"externalPath"}}]}}]}}]}}]} as unknown as DocumentNode<FindNavigationQuery, FindNavigationQueryVariables>;
export const FindNavigationItemByPathDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"findNavigationItemByPath"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"navigationId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"path"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"renderNavigation"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"navigationIdOrSlug"},"value":{"kind":"Variable","name":{"kind":"Name","value":"navigationId"}}},{"kind":"Argument","name":{"kind":"Name","value":"path"},"value":{"kind":"Variable","name":{"kind":"Name","value":"path"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"path"}},{"kind":"Field","name":{"kind":"Name","value":"related"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}}]}}]}}]}}]}}]} as unknown as DocumentNode<FindNavigationItemByPathQuery, FindNavigationItemByPathQueryVariables>;
export const PaginaDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"pagina"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"pagina"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"alias"}},{"kind":"Field","name":{"kind":"Name","value":"toonTitel"}},{"kind":"Field","name":{"kind":"Name","value":"seo"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"SeoFragment"}}]}},{"kind":"Field","name":{"kind":"Name","value":"inhoud"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudAfbeeldingFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudBannersFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudCategorieNavigatieFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudContentFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudHeroFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudTekstFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudVideoFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudUitgelichteProductenFragment"}},{"kind":"FragmentSpread","name":{"kind":"Name","value":"InhoudButtonFragment"}}]}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"StrapiImageFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"UploadFileEntity"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"alternativeText"}},{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"mime"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"provider"}},{"kind":"Field","name":{"kind":"Name","value":"size"}},{"kind":"Field","name":{"kind":"Name","value":"width"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"formats"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"MiscButtonFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentMiscButton"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"url"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"SeoFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentSharedSeo"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"metaTitle"}},{"kind":"Field","name":{"kind":"Name","value":"metaDescription"}},{"kind":"Field","name":{"kind":"Name","value":"metaImage"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"keywords"}},{"kind":"Field","name":{"kind":"Name","value":"metaRobots"}},{"kind":"Field","name":{"kind":"Name","value":"structuredData"}},{"kind":"Field","name":{"kind":"Name","value":"metaViewport"}},{"kind":"Field","name":{"kind":"Name","value":"canonicalURL"}},{"kind":"Field","name":{"kind":"Name","value":"metaSocial"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"socialNetwork"}},{"kind":"Field","name":{"kind":"Name","value":"image"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudAfbeeldingFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudAfbeelding"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudBannersFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudBanners"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"banners"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"inhoud"}},{"kind":"Field","name":{"kind":"Name","value":"kleur"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"product_prijs"}},{"kind":"Field","name":{"kind":"Name","value":"product_prijs_aanbieding"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudCategorieNavigatieFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudCategorieNavigatie"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"categorie"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"cats"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudContentFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudContent"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"inhoud"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"button"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MiscButtonFragment"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudHeroFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudHero"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"tekst"}},{"kind":"Field","name":{"kind":"Name","value":"afbeelding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"StrapiImageFragment"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"button"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"MiscButtonFragment"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudTekstFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudInhoud"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"tekst"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudVideoFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudVideo"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"alt_text"}},{"kind":"Field","name":{"kind":"Name","value":"youtube_url"}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudUitgelichteProductenFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudUitgelichteProducten"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"buttonTekst"}},{"kind":"Field","name":{"kind":"Name","value":"categorie"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"pad"}}]}}]}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"InhoudButtonFragment"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"ComponentInhoudButton"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"__typename"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}}]} as unknown as DocumentNode<PaginaQuery, PaginaQueryVariables>;
export const PageByAliasDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"pageByAlias"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"alias"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"paginas"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"filters"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"alias"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"eq"},"value":{"kind":"Variable","name":{"kind":"Name","value":"alias"}}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"attributes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"titel"}},{"kind":"Field","name":{"kind":"Name","value":"alias"}}]}}]}}]}}]}}]} as unknown as DocumentNode<PageByAliasQuery, PageByAliasQueryVariables>;