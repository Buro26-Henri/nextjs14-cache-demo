import { gql } from '@urql/core'


export const STRAPI_IMAGE_FRAGMENT_STRING = `
    fragment StrapiImageFragment on UploadFileEntity {
        id
        attributes {
            url
            alternativeText
            hash
            mime
            name
            provider
            size
            width
            height
            formats
        }
    }
`

export const STRAPI_IMAGE_FRAGMENT = gql`
    fragment StrapiImageFragment on UploadFileEntity {
        id
        attributes {
            url
            alternativeText
            hash
            mime
            name
            provider
            size
            width
            height
            formats
        }
    }
`

export const RESPONSE_COLLECTION_META_FRAGMENT = gql`
    fragment ResponseCollectionMetaFragment on ResponseCollectionMeta  {
        pagination {
            total
            page
            pageCount
            pageSize
        }
    }
`

export const MISC_BUTTON_FRAGMENT = gql`
    fragment MiscButtonFragment on ComponentMiscButton {
        id
        label
        url
    }
`