import { STRAPI_IMAGE_FRAGMENT } from '@/lib/strapi/queries/misc'
import { gql } from '@urql/core'


export const SEO_FRAGMENT = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    fragment SeoFragment on ComponentSharedSeo {
        id
        metaTitle
        metaDescription
        metaImage {
            data {
                ...StrapiImageFragment
            }
        }
        keywords
        metaRobots
        structuredData
        metaViewport
        canonicalURL
        metaSocial {
            id
            title
            description
            socialNetwork
            image {
                data {
                    ...StrapiImageFragment
                }
            }
        }
    }
`