import {RESPONSE_COLLECTION_META_FRAGMENT} from "@/lib/strapi/queries/misc";
import { gql } from '@urql/core'

export const ADRES_FRAGMENT = gql`
    fragment AdresFragment on AdresEntity {
        id
        attributes {
            voornaam
            tussenvoegsel
            achternaam
            straatnaam
            huisnummer
            postcode
            plaats
            provincie
            telefoon
            bedrijf
            btw_nummer
            projectcode
            budget
            opmerking
        }
    }
`

export const ADRES_FIND_MANY_QUERY = gql`
    ${RESPONSE_COLLECTION_META_FRAGMENT}
    query adresFindMany($filters: AdresFiltersInput, $pagination: PaginationArg, $sort: [String]) {
        adressen(
            filters: $filters
            pagination: $pagination
            sort: $sort
        ) {
            data {
                id
                attributes {
                    bedrijf
                    voornaam
                    tussenvoegsel
                    achternaam
                    straatnaam
                    huisnummer
                    postcode
                    plaats
                    budget
                    opmerking
                    projectcode
                }
            }
            meta {
                ...ResponseCollectionMetaFragment
            }
        }
    }
`

export const ADRES_FIND_QUERY = gql`
    ${ADRES_FRAGMENT}
    query adresFind($filters: AdresFiltersInput) {
        adressen(filters: $filters) {
            data {
                ...AdresFragment
            }
        }
    }
`

