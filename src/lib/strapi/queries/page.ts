import {STRAPI_IMAGE_FRAGMENT} from "@/lib/strapi/queries/misc";
import {SEO_FRAGMENT} from "@/lib/strapi/queries/seo";
import {
    INHOUD_AFBEELDING_FRAGMENT,
    INHOUD_BANNERS_FRAGMENT,
    INHOUD_BUTTON_FRAGMENT,
    INHOUD_CATEGORIE_NAVIGATIE_FRAGMENT,
    INHOUD_CONTENT_FRAGMENT,
    INHOUD_HERO_FRAGMENT,
    INHOUD_TEKST_FRAGMENT,
    INHOUD_UITGELICHTE_PRODUCTEN_FRAGMENT,
    INHOUD_VIDEO_FRAGMENT
} from "@/lib/strapi/queries/components";
import { gql } from '@urql/core'


export const PAGE = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    ${SEO_FRAGMENT}
    ${INHOUD_AFBEELDING_FRAGMENT}
    ${INHOUD_BANNERS_FRAGMENT}
    ${INHOUD_BUTTON_FRAGMENT}
    ${INHOUD_CONTENT_FRAGMENT}
    ${INHOUD_CATEGORIE_NAVIGATIE_FRAGMENT}
    ${INHOUD_HERO_FRAGMENT}
    ${INHOUD_TEKST_FRAGMENT}
    ${INHOUD_VIDEO_FRAGMENT}
    ${INHOUD_UITGELICHTE_PRODUCTEN_FRAGMENT}
    query pagina($id: ID!) {
        pagina(id: $id) {
            data {
                id
                attributes {
                    titel
                    alias
                    toonTitel
                    seo {
                        ...SeoFragment
                    }
                    inhoud {
                        ...InhoudAfbeeldingFragment
                        ...InhoudBannersFragment
                        ...InhoudCategorieNavigatieFragment
                        ...InhoudContentFragment
                        ...InhoudHeroFragment
                        ...InhoudTekstFragment
                        ...InhoudVideoFragment
                        ...InhoudUitgelichteProductenFragment
                        ...InhoudButtonFragment
                    }
                }
            }
        }
    }
`

export const PAGE_BY_ALIAS = gql`
    query pageByAlias($alias: String!) {
        paginas(filters: { alias: { eq: $alias } }) {
            data {
                id
                attributes {
                    titel
                    alias
                }
            }
        }
    }
`

