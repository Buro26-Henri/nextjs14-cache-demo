import { MISC_BUTTON_FRAGMENT, STRAPI_IMAGE_FRAGMENT } from '@/lib/strapi/queries/misc'
import { gql } from '@urql/core'

export const INHOUD_AFBEELDING_FRAGMENT = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    fragment InhoudAfbeeldingFragment on ComponentInhoudAfbeelding{
        __typename
        id
        afbeelding {
            data {
                ...StrapiImageFragment
            }
        }
    }
`

export const INHOUD_BANNERS_FRAGMENT = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    fragment InhoudBannersFragment on ComponentInhoudBanners {
        __typename
        id
        banners {
            data {
                id
                attributes {
                    titel
                    inhoud
                    kleur
                    url
                    product_prijs
                    product_prijs_aanbieding
                    afbeelding {
                        data {
                            ...StrapiImageFragment
                        }
                    }
                }
            }
        }
    }
`

export const INHOUD_CATEGORIE_NAVIGATIE_FRAGMENT = gql`
    fragment InhoudCategorieNavigatieFragment on ComponentInhoudCategorieNavigatie {
        __typename
        id
        categorie {
            data {
                id
            }
        }
        cats {
            data {
                id
            }
        }
    }
`


export const INHOUD_CONTENT_FRAGMENT = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    ${MISC_BUTTON_FRAGMENT}
    fragment InhoudContentFragment on ComponentInhoudContent {
        __typename
        id
        inhoud
        afbeelding {
            data {
                ...StrapiImageFragment
            }
        }
        button {
            ...MiscButtonFragment
        }
    }
`

export const INHOUD_HERO_FRAGMENT = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    ${MISC_BUTTON_FRAGMENT}
    fragment InhoudHeroFragment on ComponentInhoudHero {
        __typename
        id
        titel
        tekst
        afbeelding {
            data {
                ...StrapiImageFragment
            }
        }
        button{
            ...MiscButtonFragment
        }
    }
`

export const INHOUD_BUTTON_FRAGMENT = gql`
    fragment InhoudButtonFragment on ComponentInhoudButton {
        __typename
        id
        url
        label
    }
`


export const INHOUD_TEKST_FRAGMENT = gql`
    fragment InhoudTekstFragment on ComponentInhoudInhoud {
        __typename
        id
        tekst
    }
`

export const INHOUD_VIDEO_FRAGMENT = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    fragment InhoudVideoFragment on ComponentInhoudVideo {
        __typename
        id
        alt_text
        youtube_url
    }
`

export const INHOUD_UITGELICHTE_PRODUCTEN_FRAGMENT = gql`
    ${STRAPI_IMAGE_FRAGMENT}
    fragment InhoudUitgelichteProductenFragment on ComponentInhoudUitgelichteProducten {
        __typename
        id
        titel
        buttonTekst
        categorie {
            data {
                id
                attributes {
                    pad
                }
            }
        }
    }
`
