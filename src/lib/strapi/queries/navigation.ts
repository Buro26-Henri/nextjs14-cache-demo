import { gql } from '@urql/core'


export const NAVIGATION = gql`
    query findNavigation($navigationId: String!) {
        renderNavigation(
            navigationIdOrSlug: $navigationId
            type: TREE
            menuOnly: true
        ) {
            id
            path
            title
            type
            externalPath
            items {
                id
                path
                title
                type
                externalPath
            }
        }
    }
`

export const NAVIGATION_ITEM_BY_PATH = gql`
    query findNavigationItemByPath($navigationId: String!, $path: String!) {
        renderNavigation(
            navigationIdOrSlug: $navigationId
            path: $path
        ) {
            id
            path
            related {
                id
                attributes {
                    __typename
                }
            }
        }
    }
`