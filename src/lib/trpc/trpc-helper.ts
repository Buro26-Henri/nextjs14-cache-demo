import {appRouter} from "@/server/routers/_app";
import {GetServerSidePropsContext, GetStaticPropsContext} from 'next'
import {createServerSideHelpers} from "@trpc/react-query/server";
import {createContext} from "@/server/context";
import {transformer} from "@/lib/trpc/transformer";


export const ssrHelper = async (ctx: GetServerSidePropsContext | GetStaticPropsContext) => {
    return createServerSideHelpers({
        router: appRouter, // @ts-ignore
        ctx: await createContext(ctx),
        transformer,
    })
}

