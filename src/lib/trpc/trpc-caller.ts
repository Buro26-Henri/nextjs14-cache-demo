import 'server-only'
import { appRouter } from '@/server/routers/_app'
import { createContext } from '@/server/context'


export const createCaller = async (ctx?: any) => {
    return appRouter.createCaller(await createContext(ctx))
}