import {NextRequest, NextResponse} from "next/server";


/**
 * Create a middleware
 *
 * @see https://nextjs.org/docs/pages/building-your-application/routing/middleware
 *
 * @param middleware
 */
export const createMiddleware = (middleware: (req: NextRequest) => Promise<NextResponse | null>) => middleware