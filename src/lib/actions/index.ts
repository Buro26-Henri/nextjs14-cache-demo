import { createSafeActionClient } from 'next-safe-action'
import { createCaller } from '@/lib/trpc/trpc-caller.ts'

export const action = createSafeActionClient({
    async middleware(parsedInput, data) {
        const caller = await createCaller()

        return {
            caller
        }
    }
})