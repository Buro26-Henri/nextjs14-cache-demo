import {useParams, usePathname, useRouter as useNextRouter} from 'next/navigation'
import {useEffect} from "react";
import type { NavigateOptions } from 'next/dist/shared/lib/app-router-context.shared-runtime'

type UseRouterOptions = {
    onAfterTransition?: (path: string) => void
}

export const useRouter = ({onAfterTransition}: UseRouterOptions = {}) => {
    const router = useNextRouter()
    const {lang} = useParams<{ lang: string }>()
    const path = usePathname()

    const parseRoute = (route: string, params?: any) => {
        return route
    }

    const push = (href: string, options?: NavigateOptions) => {
        return router.push(href, options)
    }

    const redirect = (route: string, params?: any) => {
        return push(parseRoute(route, params))
    }

    useEffect(() => {
        // Bind react router navigation event to all a tags
        const onClick = (e: any) => {
            const target = e.target as HTMLElement

            let foundTarget = target

            if (
                target.tagName.toLowerCase() !== 'a' &&
                target.tagName.toLowerCase() !== 'button'
            ) {
                const closestAnchor = target.closest('a')
                if (closestAnchor) {
                    foundTarget = closestAnchor
                }
            }
            const lcTagName = foundTarget.tagName.toLowerCase()

            if (lcTagName === 'a' || lcTagName === 'button') {
                const href = foundTarget.getAttribute('href')
                if (href && href.startsWith('/')) {
                    e.preventDefault()

                    router.push(href)
                }
            }
        }

        window.addEventListener('click', onClick)

        return () => window.removeEventListener('click', onClick)
    }, [router, path])

    useEffect(() => {
        window.scrollTo(0, 0)
        onAfterTransition && onAfterTransition(path)
    }, [onAfterTransition, path])

    return {
        ...router,
        locale: (lang || 'nl') as 'nl' | 'en',
        parseRoute,
        redirect,
        push,
    }
}