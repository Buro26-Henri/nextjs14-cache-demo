import {useInView} from 'react-cool-inview';

export type UseInfiniteLoadingProps = {
    loadMore: () => void,
    hasMore: boolean,
}

export const useInfiniteLoading = ({ loadMore, hasMore }: UseInfiniteLoadingProps) => {
    const { observe } = useInView({
        onEnter: () => {
            hasMore && loadMore()
        },
    });

    return {
        loadMoreRef: observe
    }
}