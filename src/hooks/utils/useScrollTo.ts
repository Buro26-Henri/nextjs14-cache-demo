import {useRef} from "react";


export const useScrollTo = () => {
    const scrollToRef = useRef<any>(null)

    const scrollToElement = () => {
        if (scrollToRef.current === null) return

        scrollToRef.current.scrollIntoView({
            behavior: 'smooth',
        })
    }

    return { scrollToRef, scrollToElement }
}