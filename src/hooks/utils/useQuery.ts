import { useRouter } from 'next/router'


export const useQuery = () => {
    // @ts-ignore
    return new URLSearchParams(useRouter().query)
}