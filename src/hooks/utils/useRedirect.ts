import {useRouter} from '@/hooks'


export const useRedirect = (route: string, params?: any) => {
    const {push: redirect, parseRoute} = useRouter()

    const go = () => {
        const path = parseRoute(route, params)

        return redirect(path)
    }

    return {
        go,
    }
}