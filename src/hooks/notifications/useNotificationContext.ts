import {createContext, useCallback, useMemo, useState} from "react";
import {Variant} from "react-bootstrap/types";

export interface NotificationContextType {
    notifications: Notification[]
    addNotification: (notification: Notification) => void
    removeNotification: (notification: Notification) => void
}

export interface Notification {
    id: string
    message: string
    severity: Variant
    autoHideDuration: number
}

export const NotificationContext = createContext<NotificationContextType>({
    notifications: [],
    addNotification: () => {
    },
    removeNotification: () => {
    },
})
export const useNotificationContext = () => {
    const [notifications, setNotifications] = useState<Notification[]>([])

    const removeNotification = useCallback((notification: Notification) => {
        setNotifications((prev) => prev.filter((n) => n.id !== notification.id))
    }, [])

    const addNotification = useCallback((notification: Notification) => {
        setNotifications(prev => [...prev, notification])

        const timer = setTimeout(() => {
            removeNotification(notification)
        }, notification.autoHideDuration)

        return () => clearTimeout(timer)
    }, [removeNotification])

    return {
        NotificationContext,
        values: {
            notifications: useMemo(() => notifications, [notifications]),
            addNotification,
            removeNotification,
        },
    }
}