import {useContext} from "react";
import {NotificationContext} from "./useNotificationContext";

export const useNotification = () => {
    return useContext(NotificationContext)
}