import {FC, ReactNode} from "react";
import cn from "classnames";
import styles from './styles.module.css'

export interface PaperProps {
    children: ReactNode
    className?: string
    theme?: 'blue' | 'white' | 'override'
    elevation?: 'none' | 'medium'
    borderRadius?: 'small' | 'medium' | 'large'
}


const Paper: FC<PaperProps> = ({children, className, theme = 'blue', elevation = 'none', borderRadius = 'medium'}) => {

    return (
        <div
            className={cn({
                [styles.paper]: true,
                [styles.blue]: theme === 'blue',
                [styles.white]: theme === 'white',
                [styles.elevationMedium]: elevation === 'medium',
                [styles.borderRadiusSmall]: borderRadius === 'small',
                [styles.borderRadiusMedium]: borderRadius === 'medium',
                [styles.borderRadiusLarge]: borderRadius === 'large',
            }, className)}
        >
            {children}
        </div>
    )
}

export default Paper