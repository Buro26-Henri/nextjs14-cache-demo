"use client"

import {forwardRef, HTMLProps, useState, ReactNode} from 'react';
import cn from 'classnames';
import Form from 'react-bootstrap/Form';
import styles from './styles.module.css';

export interface TextFieldProps extends HTMLProps<HTMLInputElement> {
    error?: string
    isSearchBox?: boolean
    formGroupClassName?: string
    iconRight?: ReactNode
}

const TextField = forwardRef<HTMLInputElement, TextFieldProps>((
    {label, type, error, required, formGroupClassName, className, iconRight, isSearchBox = false, ...props},
    ref
) => {
    const [isFocused, setIsFocused] = useState(false)

    return (
        <div
            className={cn({
                'd-none': type === 'hidden' && !error,
            }, styles.formGroup, formGroupClassName)}
        >
            {label && 
                <Form.Label
                    className={cn(styles.label, {
                        [styles.labelError]: error,
                        [styles.labelFocus]: isFocused,
                    })}
                    htmlFor={props?.id || props?.name}
                >
                    {label}
                    {required && <span className="required"> *</span>}
                </Form.Label> 
            }
            {/** @ts-ignore */}
            <Form.Control
                className={cn(className, {
                    [styles.textFieldError]: error,
                    [styles.textArea]: type == 'textarea',
                    [styles.textField]: type !== 'textarea' && !isSearchBox,
                })}
                id={props?.id || props?.name}
                type={type}
                placeholder={props?.placeholder}
                ref={ref}
                onFocus={() => setIsFocused(true)}
                onBlur={() => setIsFocused(false)}
                isInvalid={!!error} // @ts-ignore
                as={type === 'textarea' ? 'textarea' : undefined}
                {...props}
            />
            {iconRight}

            {error && <div className={styles.error}>{error}</div>}
        </div>
    )
})

export default TextField

TextField.displayName = 'TextField'
