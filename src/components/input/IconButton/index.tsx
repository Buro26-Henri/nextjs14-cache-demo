'use client'

import React, { AnchorHTMLAttributes, DetailedHTMLProps } from 'react'
import cn from 'classnames'
import { IconButtonVariants } from '@/types/components'
import styles from './styles.module.css'
import {
    AccountIcon,
    AddToCartIcon,
    AddressIcon,
    CartIcon,
    CheckIcon,
    CloseIcon,
    HeartIcon,
    OrderIcon,
    WarningIcon
} from '@/components/data-display/Icons'
import Link from 'next/link'

export interface IconButtonComponentProps
    extends DetailedHTMLProps<AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement> {
    disabled?: boolean
    icon: IconButtonVariants
}

const IconButtonComponent: React.FC<IconButtonComponentProps> = ({ icon, className, ...props }) => {
    const getIcon = () => {
        switch (icon) {
            case IconButtonVariants.Account:
                return <AccountIcon className={styles.account}></AccountIcon>
            case IconButtonVariants.AddToCart:
                return <AddToCartIcon className={styles.addToCartIcon}></AddToCartIcon>
            case IconButtonVariants.Cart:
                return <CartIcon className={styles.cartIcon}></CartIcon>
            case IconButtonVariants.Check:
                return <CheckIcon className={styles.checkIcon}></CheckIcon>
            case IconButtonVariants.Close:
                return <CloseIcon className={styles.closeIcon}></CloseIcon>
            case IconButtonVariants.HeartBlack:
                return <HeartIcon className={styles.heartIconBlack}></HeartIcon>
            case IconButtonVariants.Heart:
                return <HeartIcon className={styles.heartIcon}></HeartIcon>
            case IconButtonVariants.Location:
                return <AddressIcon className={styles.addressIcon}></AddressIcon>
            case IconButtonVariants.Order:
                return <OrderIcon className={styles.orderIcon}></OrderIcon>
            case IconButtonVariants.Warning:
                return <WarningIcon className={styles.warningIcon}></WarningIcon>
        }
    }

    return (
        <>
            {props?.href && ( // @ts-ignore
                <Link className={cn(styles.iconButton, className)} {...props}>
                    {getIcon()}
                </Link>
            )}
            {!props?.href && ( // @ts-ignore
                <button className={cn(styles.iconButton, className)} {...props}>
                    {getIcon()}
                </button>
            )}
        </>
    )
}

export default IconButtonComponent
