'use client';

import React from 'react';
import Link from 'next/link';
import cn from 'classnames';
import {Button} from 'react-bootstrap';
import {ButtonVariants} from '@/types/components';
import styles from './styles.module.css';

export interface ButtonComponentProps extends React.HTMLAttributes<HTMLButtonElement> {
    variant?: ButtonVariants
    url?: string
    type?: 'button' | 'submit' | 'reset'
    disabled?: boolean
}

const ButtonComponent: React.FC<ButtonComponentProps> = (
    {
        variant = ButtonVariants.DEFAULT,
        url,
        className,
        ...props
    }
) => {
    const buttonStyle = cn(styles.btn, styles[variant], className)
    
    return (
        <>
            {url && ( // @ts-ignore
                <Link
                    href={url}
                    className={cn(buttonStyle, styles.link)}
                    {...props}
                >
                    {props.children}
                </Link>
            )}
            {!url && (
                <Button
                    variant={variant}
                    className={buttonStyle}
                    {...props}
                >
                    {props.children}
                </Button>
            )}
        </>
    )
}

export default ButtonComponent
