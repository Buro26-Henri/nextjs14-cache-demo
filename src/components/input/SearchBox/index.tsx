'use client'

import {forwardRef, useEffect} from 'react'
import TextField, {TextFieldProps} from "@/components/input/TextField"
import {useDebounceValue} from "usehooks-ts";
import {SearchIcon} from '@/components/data-display/Icons';
import styles from './styles.module.css'
import cn from 'classnames';

export interface SearchBoxProps extends TextFieldProps {
    onChangeDebounced?: (value: string) => void
    debounceTimeout?: number
    className?: string
    formGroupClassName?: string
}

const SearchBox = forwardRef<HTMLInputElement, SearchBoxProps>((props, ref) => {
    const {onChangeDebounced, debounceTimeout = 300, onChange, formGroupClassName, className, ...otherProps} = props
    const [debouncedValue, setInputValue] = useDebounceValue((otherProps?.value ?? otherProps?.defaultValue ?? '') as string, debounceTimeout)

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue(event.target.value)
        if (onChange) {
            onChange(event)
        }
    }

    useEffect(() => {
        if (onChangeDebounced) {
            onChangeDebounced(debouncedValue)
        }
    }, [debouncedValue, onChangeDebounced])

    return (
        <TextField // @ts-ignore
            ref={ref}
            onChange={handleChange}
            formGroupClassName={cn(styles.searchBoxContainer, formGroupClassName)}
            className={cn(styles.searchBox, className)}
            iconRight={<SearchIcon className={styles.iconRight}/>}
            {...otherProps}
        />
    )
})

SearchBox.displayName = 'SearchBox'

export default SearchBox
