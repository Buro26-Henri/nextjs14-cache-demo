'use client'

import { forwardRef, HTMLProps } from 'react'
import cn from 'classnames'
import Form from 'react-bootstrap/Form'
import styles from './styles.module.css'

export interface CheckboxProps extends HTMLProps<HTMLInputElement> {
    error?: string
    formGroupClassName?: string
}

const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>((
    { label, type, error, required, formGroupClassName, className, as, ...props },
    ref
) => {

    return (
        <Form.Check
            id={props?.id || props?.name}
            className={cn({
                'd-none': type === 'hidden' && !error
            }, styles.formGroup, formGroupClassName)}
        >
            <Form.Check.Input
                type={'checkbox'}
                // @ts-ignore
                ref={ref}
                {...props}
                className={cn(styles.checkbox, className, {
                    [styles.error]: error,
                    [styles.disabled]: props.disabled
                })}
            />
            {label && (
                <Form.Check.Label
                    className={cn(styles.label, {
                        [styles.labelError]: error
                    })}
                >
                    {label}
                    {required && <span className="required"> *</span>}
                </Form.Check.Label>
            )}
            {error && <Form.Control.Feedback type="invalid">{error}</Form.Control.Feedback>}
        </Form.Check>
    )
})

export default Checkbox

Checkbox.displayName = 'Checkbox'
