"use client"

import React, { forwardRef, useState } from 'react'
import cn from 'classnames'
import Form from 'react-bootstrap/Form'
import styles from './styles.module.css'

import ReactDatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import "./datepicker.css"
import nl from 'date-fns/locale/nl';
import { CalenderIcon } from '@/components/data-display/Icons'
import { useTranslation } from 'next-i18next'

export interface DatePickerProps {
    name: string
    label?: string
    type?: string
    error?: string
    required?: boolean
    className?: string
    selected: Date|undefined
    onSelect?: any
    disabled?: boolean
}

const DatePicker: React.FC<DatePickerProps> = ({ name, label, type, disabled, error, required, className, selected, onSelect, ...props }) => {
    const { t } = useTranslation()
    const [isFocused, setIsFocused] = useState(false)


    return (
        <div
            className={cn(styles.datepickerWrapper,{
                'd-none': type === 'hidden' && !error,
            })}
        >
            {label && 
                <Form.Label
                    className={cn(styles.label, {
                        [styles.labelError]: error,
                        [styles.labelFocus]: isFocused,
                    })}
                    htmlFor={name}
                >
                    {label}
                    {required && <span className="required">*</span>}
                </Form.Label> 
            }

            <ReactDatePicker // @ts-ignore
                locale={nl}
                selected={selected}
                onSelect={onSelect} //when day is clicked
                onChange={onSelect}
                className={styles.datepicker}
                dateFormat="dd-MM-yyyy"
                isClearable
                disabled={disabled}
                showIcon={true}
                placeholderText={t('common:datepicker.placeholder')}
                icon={<CalenderIcon />}
            />

            {error && <div className="error">{error}</div>}
        </div>
    )
}

export default DatePicker
