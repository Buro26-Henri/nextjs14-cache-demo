"use client"

import {forwardRef, HTMLProps, useState, ReactNode} from 'react';
import cn from 'classnames';
import Form from 'react-bootstrap/Form';
import styles from './styles.module.css';

export interface SelectProps extends HTMLProps<HTMLSelectElement> {
    error?: string
    formGroupClassName?: string
    iconRight?: ReactNode
    children: ReactNode
    inputSize?: 'sm' | 'lg' | undefined
}

const Select = forwardRef<HTMLInputElement, SelectProps>((
    {label, type, error, required, formGroupClassName, className, iconRight,inputSize, ...props},
    ref,
) => {
    const [isFocused, setIsFocused] = useState(false)

    return (
        <div
            className={cn({
                'd-none': type === 'hidden' && !error,
            }, formGroupClassName)}
        >
            {label && 
                <Form.Label
                    className={cn(styles.label, {
                        [styles.labelError]: error,
                        [styles.labelFocus]: isFocused,
                    })}
                    htmlFor={props?.id || props?.name}
                >
                    {label}
                    {required && <span className="required">*</span>}
                </Form.Label> 
            }
            {/** @ts-ignore */}
            <Form.Select
                className={cn(styles.textField, {
                    [styles.textFieldError]: error,
                }, className)}
                id={props?.id || props?.name}
                placeholder={props?.placeholder}
                ref={ref}
                isInvalid={!!error}
                onFocus={() => setIsFocused(true)}
                onBlur={() => setIsFocused(false)} // @ts-ignore
                size={inputSize}
                {...props}
            >
                {props.children}
            </Form.Select>
            {iconRight}

            {error && <div className="error">{error}</div>}
        </div>
    )
})

export default Select

Select.displayName = 'Select'

export const SelectOption = forwardRef<HTMLOptionElement, HTMLProps<HTMLOptionElement>>((props, ref) => {
    return (
        <option
            ref={ref}
            {...props}
        />
    )
})

SelectOption.displayName = 'SelectOption'