'use client'

import React, { useContext } from 'react'
import styles from './MultiSelect.module.css'
import { CloseIcon } from '@/components/data-display/Icons'
import useClickOutside from '@restart/ui/useClickOutside'
import cn from 'classnames'
import Checkbox from '@/components/input/Checkbox'

export interface MultiSelectProps<T> {
    label: string
    values: string[]
    options: T[]
    extractOptionKey: (value: T) => string
    optionLabel: (value: T) => string
    optionValue: (value: T) => string
    onChange?: (values: string[]) => Promise<void> | void
    disabled?: boolean
    className?: string
    labelClassName?: string
    optionsBelowSlot?: React.ReactNode
}

const MultiSelect = <T, >(
    {
        label,
        values,
        options,
        extractOptionKey,
        optionValue,
        optionLabel,
        onChange,
        disabled,
        className,
        labelClassName,
        optionsBelowSlot
    }: MultiSelectProps<T>
) => {
    const { isOpen, setIsOpen, openCloseDisabled } = useContext(MultiSelectContext)
    const ref = React.useRef<HTMLDivElement>(null)
    useClickOutside(ref, () => setIsOpen(false), {
        disabled: !isOpen || disabled || openCloseDisabled
    })

    return (
        <div className={className}>
            <label className={cn(styles.label, labelClassName)}>
                {label}
            </label>

            <div
                className={cn(styles.selectBox, {
                    [styles.selectBoxOpen]: isOpen
                })}
                ref={ref}
                onClick={() => setIsOpen(true)}
            >
                <div className={styles.selectedValues}>
                    {options.map((option) => (
                        <React.Fragment key={extractOptionKey(option)}>
                            {values.includes(optionValue(option)) && (
                                <div
                                    className={cn(styles.chip, {
                                        [styles.chipDisabled]: disabled
                                    })}
                                >
                                    <span>{optionLabel(option)}</span>

                                    <CloseIcon
                                        className={cn(styles.chipClose, {
                                            [styles.chipCloseDisabled]: disabled
                                        })}
                                        onClick={async e => {
                                            e.preventDefault()
                                            e.stopPropagation()

                                            if (disabled || !onChange) return

                                            await onChange(values.filter(value => value !== optionValue(option)))
                                        }}
                                    />
                                </div>
                            )}
                        </React.Fragment>
                    ))}
                </div>

                {isOpen && (
                    <div className={styles.availableOptions}>
                        {options.map((option) => (
                            <React.Fragment key={extractOptionKey(option)}>
                                <div className={cn(styles.option, {
                                    [styles.optionDisabled]: disabled
                                })}>
                                    <Checkbox
                                        className={styles.optionInput}
                                        formGroupClassName={styles.optionFormGroup}
                                        checked={values.includes(optionValue(option))}
                                        disabled={disabled}
                                        onChange={async () => {
                                            if (disabled || !onChange) return

                                            await onChange(
                                                values.includes(optionValue(option))
                                                    ? values.filter(value => value !== optionValue(option))
                                                    : [...values, optionValue(option)]
                                            )
                                        }}
                                    />

                                    <span>{optionLabel(option)}</span>
                                </div>
                            </React.Fragment>
                        ))}

                        {optionsBelowSlot}
                    </div>
                )}
            </div>
        </div>
    )
}

const MultiSelectContainer = <T, >(props: MultiSelectProps<T>) => {
    const [isOpen, setIsOpen] = React.useState(false)
    const [openCloseDisabled, setOpenCloseDisabled] = React.useState(false)

    return (
        <MultiSelectContext.Provider value={{
            isOpen,
            setIsOpen,
            setOpenCloseDisabled,
            openCloseDisabled
        }}>
            <MultiSelect {...props} />
        </MultiSelectContext.Provider>
    )
}

export default MultiSelectContainer

export const MultiSelectContext = React.createContext<{
    isOpen: boolean
    setIsOpen: (isOpen: boolean) => void
    setOpenCloseDisabled: (openCloseDisabled: boolean) => void
    openCloseDisabled: boolean
}>({} as any)

