import React from 'react';
import Script from "next/script";

export interface StructuredDataProps {
    children: string|object
}

const StructuredData: React.FC<StructuredDataProps> = ({children}) => {


    return (
        <Script
            id="structured-data"
            type="application/ld+json"
        >
            {JSON.stringify(children)}
        </Script>
    )
}

export default StructuredData