"use client"

import React, {useEffect, useState} from 'react';
import cn from "classnames";

export interface ScrollToTopProps {

}

const ScrollToTop: React.FC<ScrollToTopProps> = ({}) => {
    const [showScrollToTop, setShowScrollToTop] = useState(false)

    const handleTop = () => {
        scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        })
    }

    useEffect(() => {
        const onScroll = () => setShowScrollToTop(window.scrollY > 150)

        window.addEventListener('scroll', onScroll, { passive: true })
        
        return () => window.removeEventListener('scroll', onScroll);
    }, [])

    return (
        <div
            className={cn('back-to-top', {
                show: showScrollToTop,
                hide: !showScrollToTop
            })}
            onClick={handleTop} 
        >
            <p>Scroll omhoog</p>
        </div>
    )
}

export default ScrollToTop