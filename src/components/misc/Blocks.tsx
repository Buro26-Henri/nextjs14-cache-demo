"use client"

import React from 'react';
import { BlocksRenderer, type BlocksContent } from '@strapi/blocks-react-renderer';
import Link from "next/link";
import Image from "next/image";
import process from "process";

export interface BlocksProps {
    children: BlocksContent|any
}

const Blocks: React.FC<BlocksProps> = ({children}) => {

    return (
        <BlocksRenderer
            content={children}
            blocks={{
                link: ({ children, url, ...props }) => (
                    <Link
                        href={url}
                        target={url?.startsWith('http') ? '_blank' : '_self'}
                        {...props}
                    >
                        {children}
                    </Link>
                ),
                image: ({ image }) => (
                    <Image
                        src={`${process.env.NEXT_PUBLIC_API_URL}${image.url}`}
                        alt={image.alternativeText as string}
                        className={'content__image'}
                        width={image.width}
                        height={image.height}
                    />
                )
            }}
        />
    )
}

export default Blocks