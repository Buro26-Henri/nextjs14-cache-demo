'use client'

import React, { useEffect, useState } from 'react'
import { getImageProps } from 'next/image'
import { StrapiImageFragmentFragment } from '@/lib/strapi'
import styles from './styles.module.css'
import cn from 'classnames'

export interface StrapiImageProps extends StrapiImageFragmentFragment {
    className?: string
    lazy?: boolean
    containerClassName?: string
}

const StrapiImage: React.FC<StrapiImageProps> = ({ lazy = true, className, containerClassName, attributes }) => {
    const ref = React.useRef<HTMLImageElement>(null)
    const [isLoaded, setIsLoaded] = useState(!lazy)
    const baseUrl = process.env.NEXT_PUBLIC_API_URL || ''
    const largeUri = attributes?.formats?.large?.url ?? attributes?.url ?? ''
    const mediumUri = attributes?.formats?.medium?.url ?? largeUri
    const smallUri = attributes?.formats?.small?.url ?? mediumUri
    const thumbnailUri = attributes?.formats?.thumbnail?.url ?? smallUri

    const largeWidth = attributes?.formats?.large?.width ?? attributes?.width ?? 0
    const largeHeight = attributes?.formats?.large?.height ?? attributes?.height ?? 0

    useEffect(() => {
        if (ref.current?.complete) {
            setIsLoaded(true)
        }

        const onLoad = () => {
            setIsLoaded(true)
        }

        ref?.current?.addEventListener('load', onLoad)

        return () => {
            ref?.current?.removeEventListener('load', onLoad)
        }
    }, [ref])

    if (!attributes || !attributes.url) {
        return <picture/>
    }

    const {props: {srcSet, ...rest}} = getImageProps({
        src: `${baseUrl}${attributes?.url}`,
        alt: attributes?.alternativeText ?? '',
        loading: lazy ? 'lazy' : 'eager',
        width: largeWidth,
        height: largeHeight,
        className: cn({
            [styles.image]: true,
            [styles.imageLoading]: !isLoaded
        }, className),
    })

    return (
        <picture
            className={cn({
                [styles.imageContainer]: true,
                [styles.imageContainerLoading]: !isLoaded,
                [containerClassName ?? '']: !!containerClassName
            })}
            style={{
                backgroundImage: lazy ? `url(${baseUrl}${thumbnailUri})` : undefined
            }}
        >
            <source
                srcSet={`${baseUrl}${largeUri}`}
                media="(min-width: 1200px)"
            />

            <source
                srcSet={`${baseUrl}${mediumUri}`}
                media="(min-width: 768px)"
            />

            <source
                srcSet={`${baseUrl}${smallUri}`}
                media="(max-width: 767px)"
            />

            <img
                {...rest}
                ref={ref}
            />
        </picture>
    )
}

export default StrapiImage