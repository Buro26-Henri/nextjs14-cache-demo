import React from 'react';
import Head from "next/head";
import {
    ComponentSharedMetaSocial,
    ComponentSharedSeo,
    Enum_Componentsharedmetasocial_Socialnetwork
} from "@/types/strapi";


const Seo: React.FC<ComponentSharedSeo & { locale: string, appName: string }> = (
    {
        metaTitle,
        metaDescription,
        metaImage,
        keywords,
        canonicalURL,
        structuredData,
        metaSocial,
        locale,
        appName,
    }
) => {

    return (
        <>
            {structuredData && (
                <script
                    type="application/ld+json"
                >
                    {structuredData}
                </script>
            )}


        </>
    )
}

export default Seo

const MetaSocialNetwork: React.FC<ComponentSharedMetaSocial> = ({title, socialNetwork, image, description}) => (
    <>
        {socialNetwork === Enum_Componentsharedmetasocial_Socialnetwork.Twitter &&
            <>
                <meta
                    key="twitter:card"
                    name="twitter:card"
                    content="summary_large_image"
                />
                <meta
                    key="twitter:title"
                    property="twitter:title"
                    content={title}
                />
                <meta
                    key="twitter:description"
                    property="twitter:description"
                    content={description}
                />
                <meta
                    key="twitter:image"
                    property="twitter:image"
                    content={image?.data?.attributes?.url ? `${process.env.NEXT_PUBLIC_API_URL}${image?.data?.attributes?.url}` : `${process.env.NEXT_PUBLIC_BASE_URL}/images/logo.jpg`}
                />
            </>
        }
    </>
)