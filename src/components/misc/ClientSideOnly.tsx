"use client"

import {useIsClientSide} from '@/hooks'
import {HasChildren} from '@/types/components'
import React from 'react'

export interface ClientSideOnly extends HasChildren {
}

const ClientSideOnly: React.FC<ClientSideOnly> = ({children}) => {
    const isClientSide = useIsClientSide()

    if (!isClientSide) return null

    return (
        <>{children}</>
    )
}

export default ClientSideOnly