import React from 'react';
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import remarkGfm from "remark-gfm"; // @ts-ignore
import {ReactMarkdownOptions} from "react-markdown/lib/react-markdown";
import Link from "next/link";
import Image from "next/image";
import * as process from "process";

export interface MarkdownProps extends ReactMarkdownOptions {

}

const Markdown: React.FC<MarkdownProps | any> = ({children, ...props}) => {

    return (
        <div className={'text__block'}>
            <ReactMarkdown
                rehypePlugins={[rehypeRaw, ...(props?.rehypePlugins || [])]}
                remarkPlugins={[remarkGfm, ...(props?.remarkPlugins || [])]}
                components={{
                    a: ({node, ...props}) => ( // @ts-ignore
                        <Link
                            {...props}
                            href={props.href || '' as string}
                            target={props?.href?.startsWith('http') ? '_blank' : '_self'}
                        />
                    ),
                    img: ({node, ...props}) => (
                        <Image
                            src={`${process.env.NEXT_PUBLIC_API_URL}${props.src}`}
                            alt={props.alt as string}
                            className={'content__image'}
                            width={1920}
                            height={1080}
                        />
                    ),
                    ...(props?.components || {}),
                }}
                {...props}
            >
                {children}
            </ReactMarkdown>
        </div>
    )
}

export default Markdown
