'use client'

import React, { useRef } from 'react'
import styles from './Modal.module.css'
import IconButton from '@/components/input/IconButton'
import { IconButtonVariants } from '@/types/components'
import { createPortal } from 'react-dom'
import { CSSTransition } from 'react-transition-group'
import cn from 'classnames'

export interface ModalProps {
    children: React.ReactNode
    open: boolean
    onClose?: () => void
    modalClassName?: string
}

const Modal: React.FC<ModalProps> = ({ children, open, onClose, modalClassName }) => {
    const backdropRef = useRef(null)
    const modalRef = useRef(null)

    if (typeof document === 'undefined') {
        return null
    }

    return (
        <>
            {createPortal((
                <CSSTransition
                    in={open}
                    nodeRef={backdropRef}
                    timeout={300}
                    unmountOnExit
                    classNames={{
                        enter: styles.backdropEnter,
                        enterActive: styles.backdropEnterActive,
                        exit: styles.backdropExit,
                        exitActive: styles.backdropExitActive
                    }}
                >
                    <div
                        className={styles.backdrop}
                        onClick={onClose}
                        ref={backdropRef}
                    />
                </CSSTransition>
            ), document?.querySelector('body') as NonNullable<Element>)}
            {createPortal((
                <CSSTransition
                    in={open}
                    nodeRef={modalRef}
                    timeout={300}
                    unmountOnExit
                    classNames={{
                        enter: styles.modalEnter,
                        enterActive: styles.modalEnterActive,
                        exit: styles.modalExit,
                        exitActive: styles.modalExitActive
                    }}
                >
                    <div className={cn(styles.modal, modalClassName)} ref={modalRef}>
                        <div className={styles.header}>
                            <IconButton
                                className={styles.iconClose}
                                icon={IconButtonVariants.Close}
                                onClick={onClose}
                            />
                        </div>
                        <div className={styles.content}>
                            {children}
                        </div>
                    </div>
                </CSSTransition>
            ), document?.querySelector('body') as NonNullable<Element>)}
        </>
    )
}

export default Modal