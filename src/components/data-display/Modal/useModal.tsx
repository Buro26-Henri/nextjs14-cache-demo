import { useState } from 'react'
import Modal from './Modal'

export const useModal = () => {
    const [isOpen, setIsOpen] = useState(false)
    const open = () => setIsOpen(true)
    const close = () => setIsOpen(false)

    return {
        isOpen,
        open,
        close,
        Modal,
        props: {
            open: isOpen,
        }
    }
}