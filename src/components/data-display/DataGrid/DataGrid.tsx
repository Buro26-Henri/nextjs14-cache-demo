'use client'

import React, {ReactNode} from 'react';
import {useTranslation} from "next-i18next";
import {ListCardLoading} from "@/components/data-display/DataGrid/ListCard";

export interface BaseGridProps<T> {
    data: T[]
    renderEmpty?: () => React.ReactNode
}

export interface GridProps<T> extends BaseGridProps<T> {
    renderItem: (item: T) => React.ReactNode
    renderLoading?: () => React.ReactNode
    loadMoreRefItem?: () => ReactNode
    isFetching?: boolean
    isFetchingMore?: boolean
    hasMore?: boolean
    containerClassName?: string
}


const DataGrid = <T, >(
    {
        data,
        renderItem,
        isFetching,
        isFetchingMore,
        loadMoreRefItem,
        hasMore,
        renderLoading,
        containerClassName,
        ...props
    }: GridProps<T>
) => {
    const doRenderLoading = renderLoading ? renderLoading : () => <DataGridLoading/>

    return (
        <DataGridContainer
            data={data}
            className={containerClassName}
            {...props}
        >
            {!isFetching && data.map(item => renderItem(item))}

            {(isFetching || isFetchingMore) && (
                doRenderLoading()
            )}

            {!isFetching && hasMore && loadMoreRefItem && (
                (loadMoreRefItem())
            )}
        </DataGridContainer>
    )
}

export default DataGrid

export interface DataGridContainerProps<T> extends BaseGridProps<T> {
    className?: string
    children: ReactNode
}

const DataGridContainer = <T, >({data, renderEmpty, children, className}: DataGridContainerProps<T>) => {
    const hasItems = data.length > 0
    const {t} = useTranslation()
    const doRenderEmpty = renderEmpty ? renderEmpty : () => (
        <p>{t('common:no-items-found')}</p>
    )

    return (
        <section className={className}>
            {hasItems && children}
            {!hasItems && doRenderEmpty()}
        </section>
    )
}

export const DataGridLoading = () => {
    return (
        <>
            <ListCardLoading/>
            <ListCardLoading/>
            <ListCardLoading/>
            <ListCardLoading/>
        </>
    )
}