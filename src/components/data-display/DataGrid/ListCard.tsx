import { FC, ReactNode } from 'react'
import Link from 'next/link'
import styles from './ListCard.module.css'
import cn from 'classnames'
import Skeleton from 'react-loading-skeleton'

export interface ListCardProps {
    className?: string
    url?: string
    children: ReactNode
}

const ListCard: FC<ListCardProps> = ({ url, className, children }) => {

    return (
        <article
            className={cn(styles.card, className)}
        >
            {url && (
                <Link
                    href={url}
                    className={cn(styles.link)}
                >
                    {children}
                </Link>
            )}
            {!url && (
                <div className={cn(styles.link)}>
                    {children}
                </div>
            )}
        </article>
    )
}

export default ListCard

export const ListCardLoading = () => {
    return (
        <article
            className={cn(styles.card, styles.cardLoading)}
        >
            <div className={'w-100 d-flex justify-content-between'}>
                <div className={'d-flex flex-column w-100'}>
                    <Skeleton width={346} height={17} style={{ maxWidth: '100%' }} />
                    <Skeleton width={261} height={17} style={{ maxWidth: '100%' }} />
                </div>

                <div className={'ml-auto d-none d-md-flex'}>
                    <Skeleton width={60} height={36} />
                </div>
            </div>


        </article>
    )
}