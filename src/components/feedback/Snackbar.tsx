import React from 'react'
import {HasOptionalChildren, IconButtonVariants} from '@/types/components'
import {Variant} from 'react-bootstrap/types' // Import Bootstrap's toast component
import IconButtonComponent from '../input/IconButton'
import styles from './Snackbar.module.css'
import cn from 'classnames'
import {SuccessIcon, WarningIcon} from '../data-display/Icons'

export interface SnackbarProps extends HasOptionalChildren {
    autoHideDuration: number
    open: boolean
    color?: string
    close: () => void
    severity: Variant
}

const Snackbar: React.FC<SnackbarProps> = ({autoHideDuration, open, close, children, severity}) => {
    const success = severity === 'success'
    const danger = severity === 'danger'

    return (
        <div
            className={cn(styles.notification, {
                [styles.success]: success,
                [styles.danger]: danger,
            })}
        >
            <div className={styles.notificationBody}>
                <div className={styles.content}>
                    {success && <SuccessIcon className={styles.successIcon}></SuccessIcon>}
                    {danger && <WarningIcon className={styles.warningIcon}></WarningIcon>}
                    {children}
                </div>

                <IconButtonComponent
                    onClick={close}
                    icon={IconButtonVariants.Close}
                    className={styles.btnClose}
                />
            </div>
        </div>
    )
}

export default Snackbar
