'use client'

import React, { useEffect, useRef } from 'react'
import { useNotification } from '@/hooks'
import Snackbar from '../feedback/Snackbar'
import styles from './Notifications.module.css'
import { Col, Container, Row } from 'react-bootstrap'

export interface NotificationsProps {}

const Notifications: React.FC<NotificationsProps> = ({}) => {
    const { notifications, removeNotification } = useNotification()
    const notificationsRef = useRef<HTMLElement>(null)

    useEffect(() => {
        if (notificationsRef.current) {
            notificationsRef.current.scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            })
        }
    }, [notifications])

    if (!notifications.length) return null

    return (
        <Container>
            <section ref={notificationsRef} className={styles.notificationContainer}>
                {notifications.map((notification) => (
                    <Snackbar
                        {...notification}
                        key={notification.id + Math.random()}
                        autoHideDuration={notification.autoHideDuration}
                        open={true}
                        close={() => {
                            removeNotification(notification)
                        }}
                    >
                        {notification.message}
                    </Snackbar>
                ))}
            </section>
        </Container>
    )
}
export default Notifications
