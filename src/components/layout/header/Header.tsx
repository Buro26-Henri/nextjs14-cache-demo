import React from 'react'
import Navigation from '@/components/layout/header/Navigation'
import styles from './Header.module.css'

export interface HeaderProps {

}

const Header: React.FC<HeaderProps> = ({}) => {

    return (
        <header className={styles.container}>
            <Navigation />
        </header>
    )
}

export default Header
