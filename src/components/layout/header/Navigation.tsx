import React from 'react'
import Link from 'next/link'
import styles from './Navigation.module.css'

export interface NavigationProps {

}

const Navigation: React.FC<NavigationProps> = ({}) => {

    return (
        <nav className={styles.container}>
            <Link href={'/'}>Home</Link>
            <Link href={'/fundamentals'}>Fundamentals</Link>
            <Link href={'/todos/none'}>Cache</Link>
        </nav>
    )
}

export default Navigation
