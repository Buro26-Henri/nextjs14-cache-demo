import React from 'react';
import {HasChildren} from "@/types/components";
import {ErrorBoundary as ReactErrorBoundary} from "react-error-boundary";

export interface ErrorBoundaryProps extends HasChildren {

}

const ErrorBoundary: React.FC<ErrorBoundaryProps> = ({children}) => {

    return (
        <ReactErrorBoundary
            fallbackRender={({error, resetErrorBoundary}) => {

                return (
                    <div className={'error-boundary'}>
                        <h1>{JSON.stringify(error)}</h1>
                    </div>
                )
            }}
        >
            {children}
        </ReactErrorBoundary>
    )
}

export default ErrorBoundary