"use client"

import React from 'react';
import {Poppins} from "next/font/google";
import ReactDOM from "react-dom";

export interface FontProviderProps {
    children: React.ReactNode
}

const poppins = Poppins({
    weight: ['400', '500', '600', '700'],
    style: ['normal', 'italic'],
    subsets: ['latin'],
})

ReactDOM.preconnect('https://d1azc1qln24ryf.cloudfront.net')
ReactDOM.preload('https://d1azc1qln24ryf.cloudfront.net/45713/productionburo26nl/style-cf.css?gj4v6g', {
    as: 'style',
})

const FontProvider: React.FC<FontProviderProps> = ({children}) => {

    return (
        <>
            <head>
                <link rel="stylesheet"
                      href="https://d1azc1qln24ryf.cloudfront.net/45713/productionburo26nl/style-cf.css?gj4v6g"/>
                <style jsx global>
                    {`
                        :root {
                            --heading-font: ${poppins.style.fontFamily};
                            --main-font: ${poppins.style.fontFamily};
                        }
                    `}
                </style>
            </head>
            {children}
        </>
    )
}

export default FontProvider