"use client"

import React from 'react';
import {useNotificationContext} from "@/hooks";

export interface NotificationProviderProps {
    children: React.ReactNode
}

const NotificationProvider: React.FC<NotificationProviderProps> = ({children}) => {
    const {NotificationContext, values} = useNotificationContext()

    return (
        <NotificationContext.Provider value={values}>
            {children}
        </NotificationContext.Provider>
    )
}

export default NotificationProvider