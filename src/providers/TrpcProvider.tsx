"use client"

import React, {useState} from 'react';
import {createTRPCReact} from "@trpc/react-query";
import type {AppRouter} from '@/server/routers/_app'
import {httpBatchLink} from "@trpc/client";
import process from "process";
import {QueryClientProvider} from "@tanstack/react-query";
import {transformer} from "@/lib/trpc/transformer";
import {queryClient as reactQueryClient} from "@/lib/trpc/query-client";
import {ReactQueryDevtools} from "@tanstack/react-query-devtools";

export interface TrpcProviderProps {
    children: React.ReactNode
}

const trpc = createTRPCReact<AppRouter>()

function getBaseUrl() {
    if (typeof window !== 'undefined')
        // browser should use relative path
        return ''

    if (process.env.VERCEL_URL)
        // reference for vercel.com
        return `https://${process.env.VERCEL_URL}`

    if (process.env.RENDER_INTERNAL_HOSTNAME)
        // reference for render.com
        return `http://${process.env.RENDER_INTERNAL_HOSTNAME}:${process.env.PORT}`

    // assume localhost
    return process.env.NEXT_PUBLIC_BASE_URL || 'http://localhost:3000'
}

const TrpcProvider: React.FC<TrpcProviderProps> = ({children}) => {
    const [queryClient] = useState(
        () => reactQueryClient,
    )

    const [trpcClient] = useState(() =>
        trpc.createClient({
            links: [
                httpBatchLink({
                    url: `${getBaseUrl()}/api/trpc`,
                }),
            ],
            transformer,
        })
    )

    return (
        <trpc.Provider client={trpcClient} queryClient={queryClient}>
            <QueryClientProvider client={queryClient}>
                {children}

                <ReactQueryDevtools initialIsOpen={false}/>
            </QueryClientProvider>
        </trpc.Provider>
    );
}

export default TrpcProvider