'use client'

import React from 'react';
import {SkeletonTheme} from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

export interface SkeletonThemeProviderProps {
    children: React.ReactNode
}

const SkeletonThemeProvider: React.FC<SkeletonThemeProviderProps> = ({children}) => {

    return (
        <SkeletonTheme
            baseColor={'#E4EDF5'}
            highlightColor={'#ECF3FA'}
        >
            {children}
        </SkeletonTheme>
    )
}

export default SkeletonThemeProvider