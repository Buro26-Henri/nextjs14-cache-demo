import {router} from '../trpc';
import list from '@/server/routers/list.ts'

export const appRouter = router({
    list,
})


export type AppRouter = typeof appRouter
