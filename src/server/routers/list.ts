import { procedure, router } from '@/server/trpc.ts'
import { db } from '@/database'
import * as z from 'zod'
import { revalidateTag, unstable_cache as nextCache } from 'next/cache'
import {
    type InsertList,
    type InsertTodo,
    list,
    todo,
} from '@/database/schema/todos'
import { eq } from 'drizzle-orm'
import { findManyLists, findManyListsCached, findOneList, findOneListCached } from '@/database/queries/todos'
import { CacheType } from '@/types/cache.ts'

export default router({

    findMany: procedure
        .input(z.object({
            cache: z.nativeEnum(CacheType),
        }))
        .query(async ({ input: {cache} }) => {
            if (cache === CacheType.NONE) {
                return findManyLists()
            }

            if (cache === CacheType.NEXT) {
                const func = nextCache(
                    findManyLists,
                    ['list'],
                    {
                        tags: ['list', 'todo'],
                    },
                )

                return func()
            }

            if (cache === CacheType.REACT) {
                return findManyListsCached()
            }

            if (cache === CacheType.NEXT_REACT) {
                const func = nextCache(
                    async () => {
                        return findManyListsCached()
                    },
                    ['list'],
                    {
                        tags: ['list', 'todo'],
                    },
                )

                return func()
            }

            throw new Error('Invalid cache type')
        }),

    findOne: procedure
        .input(z.object({
            id: z.string(),
            cache: z.nativeEnum(CacheType),
        }))
        .query(async ({ input }) => {
            if (input.cache === CacheType.NONE) {
                return findOneList(Number(input.id))
            }

            if (input.cache === CacheType.NEXT) {
                const func = nextCache(
                    findOneList,
                    ['list', input.id],
                    {
                        tags: ['list', 'todo'],
                    },
                )

                return func(Number(input.id))
            }

            if (input.cache === CacheType.REACT) {
                return findOneListCached(Number(input.id))
            }

            if (input.cache === CacheType.NEXT_REACT) {
                const func = nextCache(async (id: number) => {
                        return findOneListCached(id)
                    },
                    ['list', input.id],
                    {
                        tags: ['list', 'todo'],
                    },
                )

                return func(Number(input.id))
            }

            throw new Error('Invalid cache type')
        }),

    create: procedure
        .input(z.custom<InsertList>())
        .mutation(async ({ input }) => {
            const result = await db.insert(list).values({
                ...input,
                createdAt: (new Date()).getTime(),
            })

            // Only when next cache is used
            revalidateTag('list')

            return result
        }),

    delete: procedure
        .input(z.object({
            id: z.string(),
        }))
        .mutation(async ({ input }) => {
            const result = await db.delete(list)
                .where(
                    eq(list.id, Number(input.id)),
                )

            // Only when next cache is used
            revalidateTag('list')

            return result
        }),

    todos: router({
        add: procedure
            .input(z.custom<InsertTodo>())
            .mutation(async ({ input }) => {
                const result = await db.insert(todo).values({
                    ...input,
                    createdAt: (new Date()).getTime(),
                })

                // Only when next cache is used
                revalidateTag('list')

                return result
            }),

        update: procedure
            .input(z.custom<Partial<InsertTodo>>())
            .mutation(async ({ input }) => {
                const result = await db.update(todo)
                    .set(input)
                    .where(eq(todo.id, input.id!))
                    .returning({
                        listId: todo.listId,
                    })

                // Only when next cache is used
                revalidateTag('list')

                return result
            }),

        delete: procedure
            .input(z.object({
                id: z.string(),
            }))
            .mutation(async ({ input }) => {
                const result = await db.delete(todo)
                    .where(eq(todo.id, Number(input.id)))
                    .returning({
                        listId: todo.listId,
                    })

                // Only when next cache is used
                revalidateTag('list')

                return result
            }),
    }),

})