import { createConfig } from 'buro26-graphql-codegen-sdk'

export default createConfig({
    schema: 'http://localhost:1337/graphql',
    documents: 'src/lib/strapi/queries/**/!(*.generated).{ts,tsx}',
    typesImportPath: '@/lib/strapi/generated/types',
    schemaImportPath: '@/lib/strapi/generated/schema',
    sdl: {
        path: './schema.graphql',
    },
    types: {
        path: 'src/lib/strapi/generated/types.ts',
    },
    client: {
        path: 'src/lib/strapi/generated/client-factory.ts',
        config: {
            logger: true,
        },
    },
    zod: {
        path: 'src/lib/strapi/generated/schema.ts',
        config: {
            exportFormDataSchema: true,
        },
    },
})
